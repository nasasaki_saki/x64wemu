//
// Created by tia on 2021/04/17.
//

#ifndef X64WEMU_SRC_X64STEP_H
#define X64WEMU_SRC_X64STEP_H

// #include "x64fetchgen_test.h"
#include "x64machine.h"
#include "x64instruction.h"

struct ErrorInfo step_(struct X64Machine *m, _Bool enable_disassemble, _Bool disable_execute, _Bool debug);

#endif //X64WEMU_SRC_X64STEP_H
