//
// Created by tia on 2021/03/20.
//

#include "x64fetch.h"
#include "x64machine.h"
#include "x64instruction.h"

#include <string.h>
#include <stdint.h>
#include <stdbool.h>

uint8_t fetch_prefix(struct X64Machine *const machine, struct OpePrefix *prefix) {
    uint8_t offset = 0;
    memset(prefix, 0, sizeof(struct OpePrefix));

    while (offset < 4){
        uint8_t byte = machine->memory[machine->ip.r + offset];
        if (byte == PREFIX_LOCK
            || byte == PREFIX_REP_NE_NZ
            || byte == PREFIX_REP_E_Z) {
            prefix->grp1 = byte;
            byte = machine->memory[machine->ip.r + ++offset];
        }

        else if (byte == PREFIX_CS_OVERRIDE
            || byte == PREFIX_SS_OVERRIDE
            || byte == PREFIX_DS_OVERRIDE
            || byte == PREFIX_ES_OVERRIDE
            || byte == PREFIX_FS_OVERRIDE
            || byte == PREFIX_GS_OVERRIDE) {
            prefix->grp2 = byte;
            byte = machine->memory[machine->ip.r + ++offset];
        }

        else if (byte == PREFIX_OPERAND_SIZE_OVERRIDE) {
            prefix->grp3 = byte;
            byte = machine->memory[machine->ip.r + ++offset];
        }

        else if (byte == PREFIX_ADDR_SIZE_OVERRIDE) {
            prefix->grp4 = byte;
            byte = machine->memory[machine->ip.r + ++offset];
        }

        else {
            break;
        }
    }

    return offset;
}

_Bool fetch_rex_prefix(struct X64Machine *const machine, uint64_t rip_offset, struct OpeRexPrefix *const dist) {
    memset(dist, 0, sizeof(struct OpeRexPrefix));
    uint8_t byte = machine->memory[machine->ip.r + rip_offset];
    if ((byte >> 4) == 0b0100) {
        *(uint8_t *)dist = byte;
        return true;
    } else {
        return false;
    }
}

uint8_t fetch_vex_prefix(struct X64Machine *const machine, uint64_t rip_offset, union OpeVexPrefix *const vex_prefix) {
    memset(vex_prefix, 0, sizeof(union OpeVexPrefix));
    uint8_t byte = machine->memory[machine->ip.r + rip_offset];

    if (byte == 0xc4) {  // 3-byte
        vex_prefix->byte_3 = *(struct Ope3byteVexPrefix *)&machine->memory[machine->ip.r + rip_offset];
        return 3;
    } else if (byte == 0xc5) {  // 2-byte
        vex_prefix->byte_2 = *(struct Ope2byteVexPrefix *)&machine->memory[machine->ip.r + rip_offset];
        return 2;
    } else {
        return 0;
    }
}

_Bool fetch_evex_prefix(struct X64Machine *const machine, uint64_t rip_offset, struct OpeEvexPrefix *const evex_prefix) {
    memset(evex_prefix, 0, sizeof(struct OpeEvexPrefix));
    uint8_t byte = machine->memory[machine->ip.r + rip_offset];

    if (byte == 0x62) {
        *evex_prefix = *(struct OpeEvexPrefix *)&machine->memory[machine->ip.r + rip_offset];
        return true;
    } else {
        return false;
    }
}

uint8_t fetch_all_prefix(struct X64Machine *const machine, struct Instruction *const operation) {
    memset(operation, 0, sizeof(struct Instruction));
    uint8_t size = 0;
    uint8_t prefix_size;
    prefix_size = fetch_prefix(machine, &operation->prefix);
    if (prefix_size) {
        operation->has_prefix = true;
        size += prefix_size;
    }
    if (fetch_rex_prefix(machine, size, &operation->rex_prefix)) {
        operation->has_rex_prefix = true;
        size ++;
    } else {
        prefix_size = fetch_vex_prefix(machine, size, &operation->vex_prefix);
        if (prefix_size) {
            operation->vex_size = prefix_size;
            size += prefix_size;
        } else if (fetch_evex_prefix(machine, size, &operation->evex_prefix)) {
            operation->has_evex_prefix = true;
            size += 4;
        }
    }
    return size;
}

uint8_t fetch_mod_rm(struct X64Machine *machine, uint8_t offset, struct Instruction *dist) {
    uint8_t *ip = &machine->memory[machine->ip.r + offset];
    uint8_t size = 0;

    *(uint8_t*)&dist->mod_rm = ip[size ++];

    _Bool has_sib = false;
    if (dist->mod_rm.rm == 0b100u && dist->mod_rm.mod != 0b11u) {
        *(uint8_t *)&dist->sib = ip[size++];
        has_sib = true;
    }

    if (dist->mod_rm.mod == 0b00u) {
        if (dist->mod_rm.rm == 0b101u || (has_sib && dist->sib.base == 0b101u)) {
            dist->displacement.dword = *(uint32_t *)(ip + size);
            size += 4;
        }
    } else if (dist->mod_rm.mod == 0b01u) {
        dist->displacement.byte = *(uint8_t *)(ip + size);
        size += 1;
    } else if (dist->mod_rm.mod == 0b10u) {
        dist->displacement.dword = *(uint32_t *)(ip + size);
        size += 4;
    }

    return size;
}
