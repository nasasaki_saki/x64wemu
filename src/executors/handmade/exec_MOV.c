#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>

// flags: None.

// cat: r/mN, rN

// TAG: implemented here
#define TEMPLATE_MOV_RMn_Rn(machine, ins, error, size_constant) do { \
    struct ModRMPointer dist_info =  get_addr_from_modRM_64bit(machine, ins);       \
    UINTn_T(size_constant) *dist_addr;                                                            \
    if (dist_info.kind == MODRM_MEMORY) {                                           \
        dist_addr = &MEMORY_ACCESS_WRITE(UINTn_T(size_constant), machine, dist_info.memory_addr);   \
    } else if (dist_info.kind == MODRM_REGISTER) {                                  \
        dist_addr = &dist_info.register_p->REGISTER_SIZE_KIND(size_constant);                                           \
    } else {                                                                        \
        dist_addr = NULL;                                                               \
        error.code = ERROR_PANIC;                                                       \
        return error;                                                                   \
    }                                                                               \
    union X64Register *src_register = get_register(machine, ins->mod_rm.reg, REX_R(ins)); \
    *dist_addr = src_register->REGISTER_SIZE_KIND(size_constant);                                               \
} while(false)

#define TEMPLATE_MOV_RMn_Rn_DISASSEMBLE(machine, ins, reg_size) do { \
    char dist_buf[64];                           \
    char *register_str =get_addr_str_from_modRM_64bit(machine, ins, dist_buf); \
    if (register_str != NULL) {                  \
        format_register_str(register_str, reg_size, dist_buf);       \
    }                                            \
    char src_buf[64];                            \
    format_register_str(get_register_str(machine, ins->mod_rm.reg, REX_R(ins)), reg_size, src_buf); \
    printf("mov %s, %s\n", dist_buf, src_buf);   \
} while(false)

struct ErrorInfo exec_MOV_REX__88_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m8,r8
    // Opcode           : REX + 88 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r8 to r/m8.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_MOV_RMn_Rn(machine, ins, error, 8);

    return error;
}

struct ErrorInfo exec_MOV_REX__88_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m8,r8
    // Opcode           : REX + 88 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r8 to r/m8.

    TEMPLATE_MOV_RMn_Rn_DISASSEMBLE(machine, ins, 8);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__89_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m64,r64
    // Opcode           : REX.W + 89 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r64 to r/m64.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_MOV_RMn_Rn(machine, ins, error, 64);

    return error;
}

struct ErrorInfo exec_MOV_REXW__89_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m64,r64
    // Opcode           : REX.W + 89 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r64 to r/m64.

    TEMPLATE_MOV_RMn_Rn_DISASSEMBLE(machine, ins, 64);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_MOV_89_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m16,r16
    // Opcode           : 89 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r16 to r/m16.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_MOV_RMn_Rn(machine, ins, error, 16);

    return error;
}

struct ErrorInfo exec_MOV_89_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m16,r16
    // Opcode           : 89 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r16 to r/m16.

    TEMPLATE_MOV_RMn_Rn_DISASSEMBLE(machine, ins, 16);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_MOV_89_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m32,r32
    // Opcode           : 89 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r32 to r/m32.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_MOV_RMn_Rn(machine, ins, error, 32);

    return error;
}

struct ErrorInfo exec_MOV_89_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m32,r32
    // Opcode           : 89 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r32 to r/m32.

    TEMPLATE_MOV_RMn_Rn_DISASSEMBLE(machine, ins, 32);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

// cat:  r/m8, r8 (no rex prefix)

struct ErrorInfo exec_MOV_88_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m8,r8
    // Opcode           : 88 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r8 to r/m8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_88_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m8,r8
    // Opcode           : 88 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r8 to r/m8.

    // TODO: implement

    puts("not implemented: MOV r/m8,r8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

// cat: rN, r/mN

// TAG: implemented here
#define TEMPLATE_MOV_R_RMn(machine, ins, error, size_constant) do {                         \
    union X64Register *dist_register = get_register(machine, ins->mod_rm.reg, REX_R(ins));  \
    struct ModRMPointer src_info = get_addr_from_modRM_64bit(machine, ins);                 \
    NORMALIZE_MODRM_POINTER_READ(machine, src_info, src_addr, error, size_constant)                  \
    dist_register->REGISTER_SIZE_KIND(size_constant) = *src_addr;                           \
} while(false)

#define TEMPLATE_MOV_Rn_RMn_DISASSEMBLE(machine, ins, reg_size) do { \
    char dist_buf[64];                                                \
    format_register_str(get_register_str(machine, ins->mod_rm.reg, REX_R(ins)), reg_size, dist_buf); \
    char src_buf[64];                                               \
    char *register_str = get_addr_str_from_modRM_64bit(machine, ins, src_buf);                     \
    if (register_str != NULL) {                                      \
        format_register_str(register_str, reg_size, src_buf);       \
    }                                                                \
    printf("mov %s, %s\n", dist_buf, src_buf);                      \
} while(false)

struct ErrorInfo exec_MOV_REX__8A_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r8,r/m8
    // Opcode           : REX + 8A /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r/m8 to r8.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOV_R_RMn(machine, ins, error, 8);
    return error;
}

struct ErrorInfo exec_MOV_REX__8A_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r8,r/m8
    // Opcode           : REX + 8A /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r/m8 to r8.

    TEMPLATE_MOV_Rn_RMn_DISASSEMBLE(machine, ins, 8);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__8B_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r64,r/m64
    // Opcode           : REX.W + 8B /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move r/m64 to r64.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOV_R_RMn(machine, ins, error, 64);
    return error;
}

struct ErrorInfo exec_MOV_REXW__8B_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r64,r/m64
    // Opcode           : REX.W + 8B /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move r/m64 to r64.

    TEMPLATE_MOV_Rn_RMn_DISASSEMBLE(machine, ins, 64);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_MOV_8B_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r16,r/m16
    // Opcode           : 8B /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move r/m16 to r16.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOV_R_RMn(machine, ins, error, 16);
    return error;
}

struct ErrorInfo exec_MOV_8B_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r16,r/m16
    // Opcode           : 8B /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move r/m16 to r16.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOV_Rn_RMn_DISASSEMBLE(machine, ins, 16);
    return error;
}

struct ErrorInfo exec_MOV_8B_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r32,r/m32
    // Opcode           : 8B /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move r/m32 to r32.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOV_R_RMn(machine, ins, error, 32);
    return error;
}

struct ErrorInfo exec_MOV_8B_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r32,r/m32
    // Opcode           : 8B /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move r/m32 to r32.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOV_Rn_RMn_DISASSEMBLE(machine, ins, 32);
    return error;
}

// r8, r/m8

struct ErrorInfo exec_MOV_8A_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r8,r/m8
    // Opcode           : 8A /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move r/m8 to r8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_8A_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r8,r/m8
    // Opcode           : 8A /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move r/m8 to r8.

    // TODO: implement

    puts("not implemented: MOV r8,r/m8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

// r/mN, immN

struct ErrorInfo exec_MOV_REX__C6_0_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m8,imm8
    // Opcode           : REX + C6 /0 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8/16/32/70
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move imm8 to r/m8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REX__C6_0_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m8,imm8
    // Opcode           : REX + C6 /0 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8/16/32/70
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move imm8 to r/m8.

    // TODO: implement

    puts("not implemented: MOV r/m8,imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

// r/m64, imm32

struct ErrorInfo exec_MOV_REXW__C7_0_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m64,imm32
    // Opcode           : REX.W + C7 /0 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8/16/32/73
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move imm32 sign extended to 64-bits to r/m64.

    struct ErrorInfo error = {NO_ERROR, 0};

    // TAG: implemented here

    struct ModRMPointer dist_info = get_addr_from_modRM_64bit(machine, ins);
    uint64_t *dist_addr;
    if (dist_info.kind == MODRM_MEMORY) {
        dist_addr = &MEMORY_ACCESS_WRITE(uint64_t, machine, dist_info.memory_addr);
    } else if (dist_info.kind == MODRM_REGISTER) {
        dist_addr = &dist_info.register_p->r;
    } else {
        error.code = ERROR_PANIC;
        return error;
    }
    *dist_addr = (int64_t)(int32_t)ins->immediate.dword;

    return error;
}

struct ErrorInfo exec_MOV_REXW__C7_0_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m64,imm32
    // Opcode           : REX.W + C7 /0 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8/16/32/73
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move imm32 sign extended to 64-bits to r/m64.

    char dist_buf[64];
    char *register_str = get_addr_str_from_modRM_64bit(machine, ins, dist_buf);
    if (register_str != NULL) {
        format_register_str(register_str, 64, dist_buf);
    }
    uint64_t src = (int64_t)(int32_t)ins->immediate.dword;
    printf("mov %s, 0x%" PRIx64 "\n", dist_buf, src);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_MOV_C7_0_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m16,imm16
    // Opcode           : C7 /0 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8/16/32/71
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move imm16 to r/m16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_C7_0_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m16,imm16
    // Opcode           : C7 /0 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8/16/32/71
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move imm16 to r/m16.

    // TODO: implement

    puts("not implemented: MOV r/m16,imm16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_C7_0_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m32,imm32
    // Opcode           : C7 /0 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8/16/32/72
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move imm32 to r/m32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_C7_0_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m32,imm32
    // Opcode           : C7 /0 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8/16/32/72
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move imm32 to r/m32.

    // TODO: implement

    puts("not implemented: MOV r/m32,imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

// cat: r/m8, imm8 (no rex prefix)

struct ErrorInfo exec_MOV_C6_0_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m8,imm8
    // Opcode           : C6 /0 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8/16/32/69
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move imm8 to r/m8.

    struct ErrorInfo error = {NO_ERROR, 0};

    struct ModRMPointer dist_info = get_addr_from_modRM_64bit_byte(machine, ins);
    NORMALIZE_MODRM_POINTER_BYTE_WRITE(machine, dist_info, dist_addr, error)
    *dist_addr = ins->immediate.byte;

    return error;
}

struct ErrorInfo exec_MOV_C6_0_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m8,imm8
    // Opcode           : C6 /0 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8/16/32/69
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move imm8 to r/m8.

    char dist_buf[64];
    get_addr_str_from_modRM_64bit_byte(machine, ins, dist_buf);
    printf("mov %s, 0x%" PRIx8 "\n", dist_buf, ins->immediate.byte);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

// cat: not implemented

struct ErrorInfo exec_MOV_REXW__A0(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV AL,moffs8
    // Opcode           : REX.W + A0
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : Moffs
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move byte at (offset) to AL.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__A0_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV AL,moffs8
    // Opcode           : REX.W + A0
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : Moffs
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move byte at (offset) to AL.

    // TODO: implement

    puts("not implemented: MOV AL,moffs8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__A1(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV RAX,moffs64
    // Opcode           : REX.W + A1
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : Moffs
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move quadword at (offset) to RAX.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__A1_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV RAX,moffs64
    // Opcode           : REX.W + A1
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : Moffs
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move quadword at (offset) to RAX.

    // TODO: implement

    puts("not implemented: MOV RAX,moffs64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__A2(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV moffs8,AL
    // Opcode           : REX.W + A2
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : Moffs (w)
    // Operand 2        : AL/AX/EAX/RAX
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move AL to (offset).

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__A2_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV moffs8,AL
    // Opcode           : REX.W + A2
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : Moffs (w)
    // Operand 2        : AL/AX/EAX/RAX
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move AL to (offset).

    // TODO: implement

    puts("not implemented: MOV moffs8,AL");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__A3(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV moffs64,RAX
    // Opcode           : REX.W + A3
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : Moffs (w)
    // Operand 2        : AL/AX/EAX/RAX
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move RAX to (offset).

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__A3_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV moffs64,RAX
    // Opcode           : REX.W + A3
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : Moffs (w)
    // Operand 2        : AL/AX/EAX/RAX
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move RAX to (offset).

    // TODO: implement

    puts("not implemented: MOV moffs64,RAX");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REX__B0_rb_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r8,imm8
    // Opcode           : REX + B0 +rb ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : imm8/16/32/65
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move imm8 to r8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REX__B0_rb_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r8,imm8
    // Opcode           : REX + B0 +rb ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : imm8/16/32/65
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move imm8 to r8.

    // TODO: implement

    puts("not implemented: MOV r8,imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__B8_rd_io(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r64,imm64
    // Opcode           : REX.W + B8 +rd io
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : imm8/16/32/68
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move imm64 to r64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__B8_rd_io_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r64,imm64
    // Opcode           : REX.W + B8 +rd io
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : imm8/16/32/68
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move imm64 to r64.

    // TODO: implement

    puts("not implemented: MOV r64,imm64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_A0(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV AL,moffs8
    // Opcode           : A0
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : Moffs
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move byte at (seg:offset) to AL.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_A0_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV AL,moffs8
    // Opcode           : A0
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : Moffs
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move byte at (seg:offset) to AL.

    // TODO: implement

    puts("not implemented: MOV AL,moffs8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_A1_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV AX,moffs16
    // Opcode           : A1
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : Moffs
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move word at (seg:offset) to AX.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_A1_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV AX,moffs16
    // Opcode           : A1
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : Moffs
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move word at (seg:offset) to AX.

    // TODO: implement

    puts("not implemented: MOV AX,moffs16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_A1(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV EAX,moffs32
    // Opcode           : A1
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : Moffs
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move doubleword at (seg:offset) to EAX.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_A1_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV EAX,moffs32
    // Opcode           : A1
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : Moffs
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move doubleword at (seg:offset) to EAX.

    // TODO: implement

    puts("not implemented: MOV EAX,moffs32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_A2(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV moffs8,AL
    // Opcode           : A2
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : Moffs (w)
    // Operand 2        : AL/AX/EAX/RAX
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move AL to (seg:offset).

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_A2_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV moffs8,AL
    // Opcode           : A2
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : Moffs (w)
    // Operand 2        : AL/AX/EAX/RAX
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move AL to (seg:offset).

    // TODO: implement

    puts("not implemented: MOV moffs8,AL");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_A3_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV moffs16,AX
    // Opcode           : A3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : Moffs (w)
    // Operand 2        : AL/AX/EAX/RAX
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move AX to (seg:offset).

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_A3_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV moffs16,AX
    // Opcode           : A3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : Moffs (w)
    // Operand 2        : AL/AX/EAX/RAX
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move AX to (seg:offset).

    // TODO: implement

    puts("not implemented: MOV moffs16,AX");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_A3(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV moffs32,EAX
    // Opcode           : A3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : Moffs (w)
    // Operand 2        : AL/AX/EAX/RAX
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move EAX to (seg:offset).

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_A3_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV moffs32,EAX
    // Opcode           : A3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : Moffs (w)
    // Operand 2        : AL/AX/EAX/RAX
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move EAX to (seg:offset).

    // TODO: implement

    puts("not implemented: MOV moffs32,EAX");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_B0_rb_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r8,imm8
    // Opcode           : B0 +rb ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : imm8/16/32/64
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move imm8 to r8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_B0_rb_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r8,imm8
    // Opcode           : B0 +rb ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : imm8/16/32/64
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move imm8 to r8.

    // TODO: implement

    puts("not implemented: MOV r8,imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_B8_rw_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r16,imm16
    // Opcode           : B8 +rw iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : imm8/16/32/66
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move imm16 to r16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_B8_rw_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r16,imm16
    // Opcode           : B8 +rw iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : imm8/16/32/66
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move imm16 to r16.

    // TODO: implement

    puts("not implemented: MOV r16,imm16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_B8_rd_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r32,imm32
    // Opcode           : B8 +rd id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : imm8/16/32/67
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move imm32 to r32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_B8_rd_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r32,imm32
    // Opcode           : B8 +rd id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : imm8/16/32/67
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move imm32 to r32.

    // TODO: implement

    puts("not implemented: MOV r32,imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}
