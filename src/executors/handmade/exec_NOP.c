#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

struct ErrorInfo exec_NOP_NP_0F_1F_0_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : NOP r/m16
    // Opcode           : NP 0F 1F /0
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Multi-byte no-operation instruction.

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_NOP_NP_0F_1F_0_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : NOP r/m16
    // Opcode           : NP 0F 1F /0
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Multi-byte no-operation instruction.

    puts("NOP r/m16");

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_NOP_NP_0F_1F_0(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : NOP r/m32
    // Opcode           : NP 0F 1F /0
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Multi-byte no-operation instruction.

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_NOP_NP_0F_1F_0_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : NOP r/m32
    // Opcode           : NP 0F 1F /0
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Multi-byte no-operation instruction.

    puts("NOP r/m32");

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}