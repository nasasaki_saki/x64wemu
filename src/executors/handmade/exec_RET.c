#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

struct ErrorInfo exec_RET_C3(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RET
    // Opcode           : C3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Near return to calling procedure.

    uint64_t next_ip = MEMORY_ACCESS(uint64_t, machine, machine->sp.r);
    machine->sp.r += 8;
    machine->ip.r = next_ip;

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_RET_C3_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RET
    // Opcode           : C3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Near return to calling procedure.

    puts("ret");

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_RET_CB(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RET
    // Opcode           : CB
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Far return to calling procedure.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RET_CB_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RET
    // Opcode           : CB
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Far return to calling procedure.

    // TODO: implement

    puts("not implemented: RET");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RET_C2_iw(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RET imm16
    // Opcode           : C2 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : imm16
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Near return to calling procedure and pop imm16 bytes from stack.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RET_C2_iw_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RET imm16
    // Opcode           : C2 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : imm16
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Near return to calling procedure and pop imm16 bytes from stack.

    // TODO: implement

    puts("not implemented: RET imm16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RET_CA_iw(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RET imm16
    // Opcode           : CA iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : imm16
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Far return to calling procedure and pop imm16 bytes from stack.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RET_CA_iw_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RET imm16
    // Opcode           : CA iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : imm16
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Far return to calling procedure and pop imm16 bytes from stack.

    // TODO: implement

    puts("not implemented: RET imm16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}