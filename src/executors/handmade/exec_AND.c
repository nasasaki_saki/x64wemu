#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>

// flags: The OF and CF flags are cleared;
// the SF, ZF, and PF flags are set according to the result.
// The state of the AF flag is undefined.
#define AND_SET_FLAGS(rflags, result, size_constant) do { \
    (rflags).overflow = 0;                                \
    (rflags).carry = 0;                                   \
    (rflags).sign = SIGN((result));                       \
    (rflags).zero = ZERO((result));                       \
    (rflags).parity = parity((result));                   \
} while(false)

// cat: rmN, rN

struct ErrorInfo exec_AND_REXW__21_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m64, r64
    // Opcode           : REX.W + 21 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m64 AND r32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_REXW__21_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m64, r64
    // Opcode           : REX.W + 21 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m64 AND r32.

    // TODO: implement

    puts("not implemented: AND r/m64, r64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_21_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m16, r16
    // Opcode           : 21 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m16 AND r16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_21_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m16, r16
    // Opcode           : 21 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m16 AND r16.

    // TODO: implement

    puts("not implemented: AND r/m16, r16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_21_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m32, r32
    // Opcode           : 21 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m32 AND r32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_21_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m32, r32
    // Opcode           : 21 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m32 AND r32.

    // TODO: implement

    puts("not implemented: AND r/m32, r32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

// cat: r/m8, r8 (no rex prefix)

struct ErrorInfo exec_AND_20_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m8, r8
    // Opcode           : 20 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m8 AND r8.

    struct ErrorInfo error = {NO_ERROR, 0};

    struct ModRMPointer dist_info = get_addr_from_modRM_64bit_byte(machine, ins);
    NORMALIZE_MODRM_POINTER_BYTE_WRITE(machine, dist_info, dist_addr, error)
    struct RegisterWithBytePosition src_register = get_byte_register(machine, ins->mod_rm.reg);
    NORMALIZE_BYTE_REGISTER_WITH_POSITION_WITH_DECL(machine, src_addr, src_register);
    *dist_addr &= *src_addr;
    AND_SET_FLAGS(machine->rflags, *dist_addr, 8);

    return error;
}

struct ErrorInfo exec_AND_20_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m8, r8
    // Opcode           : 20 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m8 AND r8.

    char dist_buf[64];
    get_addr_str_from_modRM_64bit_byte(machine, ins, dist_buf);
    char *src_register = get_byte_register_str(machine, ins->mod_rm.reg);
    printf("and %s, %s", dist_buf, src_register);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

// cat: rmN, imm8 (sign extended)

// TAG: implemented here
#define TEMPLATE_AND_RMn_IMM8(machine, ins, error, size_constant) do {                                  \
    struct ModRMPointer dist_info = get_addr_from_modRM_64bit(machine, ins);                            \
    INTn_T(size_constant) src = (INTn_T(size_constant))(int8_t)ins->immediate.byte;                     \
    UINTn_T(size_constant) *dist_addr;                                                                  \
    if (dist_info.kind == MODRM_MEMORY) {                                                               \
        dist_addr = &MEMORY_ACCESS_WRITE(UINTn_T(size_constant), machine, dist_info.memory_addr);       \
    } else if (dist_info.kind == MODRM_REGISTER) {                                                      \
        dist_addr = &dist_info.register_p->REGISTER_SIZE_KIND(size_constant);                           \
    } else {                                                                                            \
        error.code = ERROR_PANIC;                                                                       \
        return error;                                                                                   \
    }                                                                                                   \
    *dist_addr &= src;                                                                                  \
    AND_SET_FLAGS(machine->rflags, *dist_addr, size_constant);                                          \
    } while (false)

#define TEMPLATE_AND_RMn_IMM8_DISASSEMBLE(machine, ins, size_constant) do { \
    char dist_buf[64];                                                        \
    char *addr_str =get_addr_str_from_modRM_64bit(machine, ins, dist_buf);\
    if (addr_str != NULL) {                                               \
        format_register_str(addr_str, size_constant, dist_buf);                          \
    }                                                                       \
    INTn_T(size_constant) src = (INTn_T(size_constant))(int8_t)ins->immediate.byte;                \
    printf("and %s, 0x%0" BIT_TO_BYTE_STR(size_constant) PRIx ## size_constant "\n", dist_buf, src);                                \
} while (false)

struct ErrorInfo exec_AND_REXW__83_4_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m64, imm8
    // Opcode           : REX.W + 83 /4 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m64 AND imm8 (sign-extended).

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_AND_RMn_IMM8(machine, ins, error, 64);

    return error;
}

struct ErrorInfo exec_AND_REXW__83_4_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m64, imm8
    // Opcode           : REX.W + 83 /4 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m64 AND imm8 (sign-extended).

    TEMPLATE_AND_RMn_IMM8_DISASSEMBLE(machine, ins, 64);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_AND_83_4_ib_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m16, imm8
    // Opcode           : 83 /4 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m16 AND imm8 (sign-extended).

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_AND_RMn_IMM8(machine, ins, error, 16);

    return error;
}

struct ErrorInfo exec_AND_83_4_ib_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m16, imm8
    // Opcode           : 83 /4 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m16 AND imm8 (sign-extended).

    TEMPLATE_AND_RMn_IMM8_DISASSEMBLE(machine, ins, 16);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_AND_83_4_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m32, imm8
    // Opcode           : 83 /4 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m32 AND imm8 (sign-extended).

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_AND_RMn_IMM8(machine, ins, error, 32);

    return error;
}

struct ErrorInfo exec_AND_83_4_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m32, imm8
    // Opcode           : 83 /4 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m32 AND imm8 (sign-extended).

    TEMPLATE_AND_RMn_IMM8_DISASSEMBLE(machine, ins, 32);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

// cat: r/m8, imm8 (no rex prefix)

struct ErrorInfo exec_AND_80_4_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m8, imm8
    // Opcode           : 80 /4 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m8 AND imm8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_80_4_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m8, imm8
    // Opcode           : 80 /4 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m8 AND imm8.

    // TODO: implement

    puts("not implemented: AND r/m8, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

// not implemented

struct ErrorInfo exec_AND_REXW__25_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND RAX, imm32
    // Opcode           : REX.W + 25 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : RAX AND imm32 sign-extended to 64-bits.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_REXW__25_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND RAX, imm32
    // Opcode           : REX.W + 25 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : RAX AND imm32 sign-extended to 64-bits.

    // TODO: implement

    puts("not implemented: AND RAX, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_REX__80_4_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m8 , imm8
    // Opcode           : REX + 80 /4 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m8 AND imm8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_REX__80_4_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m8 , imm8
    // Opcode           : REX + 80 /4 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m8 AND imm8.

    // TODO: implement

    puts("not implemented: AND r/m8 , imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_REXW__81_4_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m64, imm32
    // Opcode           : REX.W + 81 /4 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m64 AND imm32 sign extended to 64-bits.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_REXW__81_4_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m64, imm32
    // Opcode           : REX.W + 81 /4 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m64 AND imm32 sign extended to 64-bits.

    // TODO: implement

    puts("not implemented: AND r/m64, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_REX__20_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m8 , r8
    // Opcode           : REX + 20 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m64 AND r8 (sign-extended).

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_REX__20_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m8 , r8
    // Opcode           : REX + 20 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m64 AND r8 (sign-extended).

    // TODO: implement

    puts("not implemented: AND r/m8 , r8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_REX__22_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r8 , r/m8
    // Opcode           : REX + 22 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m64 AND r8 (sign-extended).

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_REX__22_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r8 , r/m8
    // Opcode           : REX + 22 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m64 AND r8 (sign-extended).

    // TODO: implement

    puts("not implemented: AND r8 , r/m8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_REXW__23_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r64, r/m64
    // Opcode           : REX.W + 23 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r64 AND r/m64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_REXW__23_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r64, r/m64
    // Opcode           : REX.W + 23 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r64 AND r/m64.

    // TODO: implement

    puts("not implemented: AND r64, r/m64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_24_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND AL, imm8
    // Opcode           : 24 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AL AND imm8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_24_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND AL, imm8
    // Opcode           : 24 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AL AND imm8.

    // TODO: implement

    puts("not implemented: AND AL, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_25_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND AX, imm16
    // Opcode           : 25 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AX AND imm16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_25_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND AX, imm16
    // Opcode           : 25 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AX AND imm16.

    // TODO: implement

    puts("not implemented: AND AX, imm16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_25_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND EAX, imm32
    // Opcode           : 25 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : EAX AND imm32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_25_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND EAX, imm32
    // Opcode           : 25 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : EAX AND imm32.

    // TODO: implement

    puts("not implemented: AND EAX, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_81_4_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m16, imm16
    // Opcode           : 81 /4 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m16 AND imm16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_81_4_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m16, imm16
    // Opcode           : 81 /4 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m16 AND imm16.

    // TODO: implement

    puts("not implemented: AND r/m16, imm16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_81_4_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m32, imm32
    // Opcode           : 81 /4 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m32 AND imm32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_81_4_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r/m32, imm32
    // Opcode           : 81 /4 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m32 AND imm32.

    // TODO: implement

    puts("not implemented: AND r/m32, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_22_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r8, r/m8
    // Opcode           : 22 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r8 AND r/m8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_22_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r8, r/m8
    // Opcode           : 22 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r8 AND r/m8.

    // TODO: implement

    puts("not implemented: AND r8, r/m8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_23_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r16, r/m16
    // Opcode           : 23 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r16 AND r/m16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_23_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r16, r/m16
    // Opcode           : 23 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r16 AND r/m16.

    // TODO: implement

    puts("not implemented: AND r16, r/m16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_23_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r32, r/m32
    // Opcode           : 23 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r32 AND r/m32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_AND_23_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : AND r32, r/m32
    // Opcode           : 23 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r32 AND r/m32.

    // TODO: implement

    puts("not implemented: AND r32, r/m32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}