#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>

// flags: The OF, SF, ZF, AF, PF, and CF flags are set according to the result.
// TODO: implement OF, AF, CF
#define ADD_SET_FLAGS(rflags, before_a, before_b, result, size_constant) do { \
    rflags.overflow;                                                          \
    rflags.sign = SIGN(result);                                               \
    rflags.zero = ZERO(result);                                               \
    rflags.adjust;                                                                          \
    rflags.parity = parity((uint64_t)result);                                           \
    rflags.carry;                                                                              \
} while (false)

// r/mN, immN

// TAG: implemented here
#define TEMPLATE_ADD_RMn_IMMn(machine, ins, error, dist_size_constant, src_size_constant) do { \
    struct ModRMPointer dist_info = get_addr_from_modRM_64bit(machine, ins); \
    UINTn_T(dist_size_constant) *dist_addr;                                                   \
    if (dist_info.kind == MODRM_MEMORY) {                                  \
        dist_addr = &MEMORY_ACCESS_WRITE(UINTn_T(dist_size_constant), machine, dist_info.memory_addr); \
    } else if (dist_info.kind == MODRM_REGISTER) {                         \
        dist_addr = &dist_info.register_p->REGISTER_SIZE_KIND(dist_size_constant);                                                   \
    } else {                                                               \
        error.code = ERROR_PANIC;                                              \
        return error;                                                          \
    }                                                                      \
    UINTn_T(dist_size_constant) before = *dist_addr;                                          \
    UINTn_T(dist_size_constant) src = (INTn_T(dist_size_constant))(INTn_T(src_size_constant))ins->immediate.dword;                 \
    *dist_addr += src;                                                     \
    ADD_SET_FLAGS(machine->rflags, before, src, *dist_addr, dist_size_constant);           \
} while (false)

#define TEMPLATE_ADD_RMn_IMMn_DISASSEMBLE(machine, ins, dist_size_constant, src_size_constant) do { \
    char dist_buf[64];                                                                                  \
    char *register_str = get_addr_str_from_modRM_64bit(machine, ins, dist_buf);                         \
    if (register_str != NULL) {                                                                         \
        format_register_str(register_str, dist_size_constant, dist_buf);                                                    \
    }                                                                                               \
    UINTn_T(dist_size_constant) src = (INTn_T(dist_size_constant))(INTn_T(src_size_constant))ins->immediate.dword;                 \
    printf("add %s, 0x%" PRIx ## dist_size_constant "\n", dist_buf, src);                                                   \
} while (false)

struct ErrorInfo exec_ADD_REX__80_0_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m8 , imm8
    // Opcode           : REX + 80 /0 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add sign-extended imm8 to r/m8.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_ADD_RMn_IMMn(machine, ins, error, 8, 8);
    return error;
}

struct ErrorInfo exec_ADD_REX__80_0_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m8 , imm8
    // Opcode           : REX + 80 /0 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add sign-extended imm8 to r/m8.

    TEMPLATE_ADD_RMn_IMMn_DISASSEMBLE(machine, ins, 8, 8);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_ADD_REXW__81_0_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m64, imm32
    // Opcode           : REX.W + 81 /0 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add imm32 sign-extended to 64-bits to r/m64.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_ADD_RMn_IMMn(machine, ins, error, 64, 32);
    return error;
}

struct ErrorInfo exec_ADD_REXW__81_0_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m64, imm32
    // Opcode           : REX.W + 81 /0 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add imm32 sign-extended to 64-bits to r/m64.

    TEMPLATE_ADD_RMn_IMMn_DISASSEMBLE(machine, ins, 64, 32);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_ADD_REXW__83_0_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m64, imm8
    // Opcode           : REX.W + 83 /0 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add sign-extended imm8 to r/m64.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_ADD_RMn_IMMn(machine, ins, error, 64, 8);
    return error;
}

struct ErrorInfo exec_ADD_REXW__83_0_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m64, imm8
    // Opcode           : REX.W + 83 /0 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add sign-extended imm8 to r/m64.

    TEMPLATE_ADD_RMn_IMMn_DISASSEMBLE(machine, ins, 64, 8);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_ADD_81_0_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m16, imm16
    // Opcode           : 81 /0 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add imm16 to r/m16.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_ADD_RMn_IMMn(machine, ins, error, 16, 16);
    return error;
}

struct ErrorInfo exec_ADD_81_0_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m16, imm16
    // Opcode           : 81 /0 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add imm16 to r/m16.

    TEMPLATE_ADD_RMn_IMMn_DISASSEMBLE(machine, ins, 16, 16);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_ADD_81_0_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m32, imm32
    // Opcode           : 81 /0 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add imm32 to r/m32.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_ADD_RMn_IMMn(machine, ins, error, 32, 32);
    return error;
}

struct ErrorInfo exec_ADD_81_0_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m32, imm32
    // Opcode           : 81 /0 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add imm32 to r/m32.

    TEMPLATE_ADD_RMn_IMMn_DISASSEMBLE(machine, ins, 32, 32);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_ADD_83_0_ib_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m16, imm8
    // Opcode           : 83 /0 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add sign-extended imm8 to r/m16.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_ADD_RMn_IMMn(machine, ins, error, 16, 8);
    return error;
}

struct ErrorInfo exec_ADD_83_0_ib_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m16, imm8
    // Opcode           : 83 /0 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add sign-extended imm8 to r/m16.

    TEMPLATE_ADD_RMn_IMMn_DISASSEMBLE(machine, ins, 16, 8);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_ADD_83_0_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m32, imm8
    // Opcode           : 83 /0 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add sign-extended imm8 to r/m32.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_ADD_RMn_IMMn(machine, ins, error, 32, 8);
    return error;
}

struct ErrorInfo exec_ADD_83_0_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m32, imm8
    // Opcode           : 83 /0 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add sign-extended imm8 to r/m32.

    TEMPLATE_ADD_RMn_IMMn_DISASSEMBLE(machine, ins, 32, 8);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

// rmN, immN (no rex prefix)

struct ErrorInfo exec_ADD_80_0_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m8, imm8
    // Opcode           : 80 /0 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add imm8 to r/m8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_80_0_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m8, imm8
    // Opcode           : 80 /0 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Add imm8 to r/m8.

    // TODO: implement

    puts("not implemented: ADD r/m8, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

// not implemented

struct ErrorInfo exec_ADD_REXW__05_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD RAX, imm32
    // Opcode           : REX.W + 05 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add imm32 sign-extended to 64-bits to RAX.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_REXW__05_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD RAX, imm32
    // Opcode           : REX.W + 05 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add imm32 sign-extended to 64-bits to RAX.

    // TODO: implement

    puts("not implemented: ADD RAX, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_REX__00_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m8 , r8
    // Opcode           : REX + 00 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r8 to r/m8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_REX__00_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m8 , r8
    // Opcode           : REX + 00 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r8 to r/m8.

    // TODO: implement

    puts("not implemented: ADD r/m8 , r8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_REXW__01_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m64, r64
    // Opcode           : REX.W + 01 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r64 to r/m64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_REXW__01_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m64, r64
    // Opcode           : REX.W + 01 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r64 to r/m64.

    // TODO: implement

    puts("not implemented: ADD r/m64, r64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_REX__02_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r8 , r/m8
    // Opcode           : REX + 02 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r/m8 to r8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_REX__02_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r8 , r/m8
    // Opcode           : REX + 02 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r/m8 to r8.

    // TODO: implement

    puts("not implemented: ADD r8 , r/m8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_REXW__03_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r64, r/m64
    // Opcode           : REX.W + 03 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r/m64 to r64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_REXW__03_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r64, r/m64
    // Opcode           : REX.W + 03 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r/m64 to r64.

    // TODO: implement

    puts("not implemented: ADD r64, r/m64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_04_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD AL, imm8
    // Opcode           : 04 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add imm8 to AL.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_04_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD AL, imm8
    // Opcode           : 04 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add imm8 to AL.

    // TODO: implement

    puts("not implemented: ADD AL, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_05_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD AX, imm16
    // Opcode           : 05 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add imm16 to AX.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_05_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD AX, imm16
    // Opcode           : 05 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add imm16 to AX.

    // TODO: implement

    puts("not implemented: ADD AX, imm16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_05_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD EAX, imm32
    // Opcode           : 05 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add imm32 to EAX.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_05_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD EAX, imm32
    // Opcode           : 05 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add imm32 to EAX.

    // TODO: implement

    puts("not implemented: ADD EAX, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_00_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m8, r8
    // Opcode           : 00 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r8 to r/m8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_00_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m8, r8
    // Opcode           : 00 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r8 to r/m8.

    // TODO: implement

    puts("not implemented: ADD r/m8, r8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_01_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m16, r16
    // Opcode           : 01 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r16 to r/m16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_01_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m16, r16
    // Opcode           : 01 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r16 to r/m16.

    // TODO: implement

    puts("not implemented: ADD r/m16, r16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_01_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m32, r32
    // Opcode           : 01 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r32 to r/m32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_01_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r/m32, r32
    // Opcode           : 01 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r32 to r/m32.

    // TODO: implement

    puts("not implemented: ADD r/m32, r32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_02_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r8, r/m8
    // Opcode           : 02 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r/m8 to r8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_02_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r8, r/m8
    // Opcode           : 02 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r/m8 to r8.

    // TODO: implement

    puts("not implemented: ADD r8, r/m8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_03_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r16, r/m16
    // Opcode           : 03 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r/m16 to r16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_03_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r16, r/m16
    // Opcode           : 03 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r/m16 to r16.

    // TODO: implement

    puts("not implemented: ADD r16, r/m16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_03_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r32, r/m32
    // Opcode           : 03 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r/m32 to r32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_ADD_03_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ADD r32, r/m32
    // Opcode           : 03 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Add r/m32 to r32.

    // TODO: implement

    puts("not implemented: ADD r32, r/m32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}