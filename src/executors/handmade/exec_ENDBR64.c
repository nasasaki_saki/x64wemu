#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

// flags: None.

struct ErrorInfo exec_ENDBR64_F3_0F_1E_FA(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ENDBR64
    // Opcode           : F3 0F 1E FA
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : CET_IBT
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Terminate indirect branch in 64 bit mode

    // TODO: implement
    // TAG: implemented here

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_ENDBR64_F3_0F_1E_FA_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : ENDBR64
    // Opcode           : F3 0F 1E FA
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : CET_IBT
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Terminate indirect branch in 64 bit mode

    puts("endbr64");

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}
