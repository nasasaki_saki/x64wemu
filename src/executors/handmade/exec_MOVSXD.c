#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

struct ErrorInfo exec_MOVSXD_REXW__63_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOVSXD r64, r/m32
    // Opcode           : REX.W + 63 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move doubleword to quadword with sign- extension.

    struct ErrorInfo error = {NO_ERROR, 0};

    union X64Register *dist_reg = get_register(machine, ins->mod_rm.reg, REX_R(ins));
    struct ModRMPointer src_info = get_addr_from_modRM_64bit(machine, ins);
    uint32_t *src_addr;
    if (src_info.kind == MODRM_REGISTER) {
        src_addr = &src_info.register_p->e;
    } else if (src_info.kind == MODRM_MEMORY) {
        src_addr = &MEMORY_ACCESS(uint32_t, machine, src_info.memory_addr);
    } ELSE_ERROR_RET(error, ERROR_PANIC)

    dist_reg->r = (int64_t)(int32_t)(*src_addr);

    return error;
}

struct ErrorInfo exec_MOVSXD_REXW__63_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOVSXD r64, r/m32
    // Opcode           : REX.W + 63 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move doubleword to quadword with sign- extension.

    char dist_buf[64];
    char src_buf[64];
    char *dist_reg_kind = get_register_str(machine, ins->mod_rm.reg, REX_R(ins));
    format_register_str(dist_reg_kind, 64, dist_buf);
    char *src_reg_kind = get_addr_str_from_modRM_64bit(machine, ins, src_buf);
    if (src_reg_kind != NULL) {
        format_register_str(src_reg_kind, 64, src_buf);
    }

    printf("movsxd %s, %s\n", dist_buf, src_buf);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}