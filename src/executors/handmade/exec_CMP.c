#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>

// copy from SUB.
// flags: The OF, SF, ZF, AF, PF, and CF flags are set according to the result.
// TODO: implement OF, AF, CF
#define CMP_SET_FLAGS(rflags, before_a, before_b, result, size_constant) do { \
    rflags.overflow;                                                          \
    rflags.sign = SIGN(result);                                               \
    rflags.zero = ZERO(result);                                               \
    rflags.adjust;                                                            \
    rflags.parity = parity((uint64_t)result);                                 \
    rflags.carry;                                                             \
} while (false)

// cat: *a*, immN

struct ErrorInfo exec_CMP_REXW__3D_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP RAX, imm32
    // Opcode           : REX.W + 3D id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm32 sign-extended to 64-bits with RAX.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_REXW__3D_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP RAX, imm32
    // Opcode           : REX.W + 3D id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm32 sign-extended to 64-bits with RAX.

    // TODO: implement

    puts("not implemented: CMP RAX, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_3C_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP AL, imm8
    // Opcode           : 3C ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm8 with AL.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_3C_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP AL, imm8
    // Opcode           : 3C ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm8 with AL.

    // TODO: implement

    puts("not implemented: CMP AL, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_3D_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP AX, imm16
    // Opcode           : 3D iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm16 with AX.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_3D_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP AX, imm16
    // Opcode           : 3D iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm16 with AX.

    // TODO: implement

    puts("not implemented: CMP AX, imm16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_3D_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP EAX, imm32
    // Opcode           : 3D id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm32 with EAX.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_3D_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP EAX, imm32
    // Opcode           : 3D id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm32 with EAX.

    // TODO: implement

    puts("not implemented: CMP EAX, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

// cat: r/mN, immN

// TAG: implemented here
#define TEMPLATE_CMP_RMn_IMMn(machine, ins, error, dist_size_constant, src_size_constant) do { \
    struct ModRMPointer dist_info = get_addr_from_modRM_64bit(machine, ins); \
    UINTn_T(dist_size_constant) *dist_addr;                                                   \
    if (dist_info.kind == MODRM_MEMORY) {                                  \
        dist_addr = &MEMORY_ACCESS(UINTn_T(dist_size_constant), machine, dist_info.memory_addr); \
    } else if (dist_info.kind == MODRM_REGISTER) {                         \
        dist_addr = &dist_info.register_p->REGISTER_SIZE_KIND(dist_size_constant);                                                   \
    } else {                                                               \
        error.code = ERROR_PANIC;                                              \
        return error;                                                          \
    }                                                                      \
    UINTn_T(dist_size_constant) src = (INTn_T(dist_size_constant))(INTn_T(src_size_constant))ins->immediate.dword;                 \
    UINTn_T(dist_size_constant) result = *dist_addr - src;                                          \
    CMP_SET_FLAGS(machine->rflags, *dist_addr, src, result, dist_size_constant);           \
} while (false)

#define TEMPLATE_CMP_RMn_IMMn_DISASSEMBLE(machine, ins, dist_size_constant, src_size_constant) do { \
    char dist_buf[64];                                                                                  \
    char *register_str = get_addr_str_from_modRM_64bit(machine, ins, dist_buf);                         \
    if (register_str != NULL) {                                                                         \
        format_register_str(register_str, dist_size_constant, dist_buf);                                                    \
    }                                                                                               \
    UINTn_T(dist_size_constant) src = (INTn_T(dist_size_constant))(INTn_T(src_size_constant))ins->immediate.dword;                 \
    printf("cmp %s, 0x%" PRIx ## dist_size_constant "\n", dist_buf, src);                                                   \
} while (false)

struct ErrorInfo exec_CMP_REX__80_7_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m8 , imm8
    // Opcode           : REX + 80 /7 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm8 with r/m8.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_CMP_RMn_IMMn(machine, ins, error, 8, 8);
    return error;
}

struct ErrorInfo exec_CMP_REX__80_7_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m8 , imm8
    // Opcode           : REX + 80 /7 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm8 with r/m8.

    TEMPLATE_CMP_RMn_IMMn_DISASSEMBLE(machine, ins, 8, 8);
    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_REXW__81_7_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m64, imm32
    // Opcode           : REX.W + 81 /7 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm32 sign-extended to 64-bits with r/m64.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_CMP_RMn_IMMn(machine, ins, error, 64, 32);
    return error;
}

struct ErrorInfo exec_CMP_REXW__81_7_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m64, imm32
    // Opcode           : REX.W + 81 /7 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm32 sign-extended to 64-bits with r/m64.

    TEMPLATE_CMP_RMn_IMMn_DISASSEMBLE(machine, ins, 64, 32);
    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_REXW__83_7_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m64, imm8
    // Opcode           : REX.W + 83 /7 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm8 with r/m64.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_CMP_RMn_IMMn(machine, ins, error, 64, 8);
    return error;
}

struct ErrorInfo exec_CMP_REXW__83_7_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m64, imm8
    // Opcode           : REX.W + 83 /7 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm8 with r/m64.

    TEMPLATE_CMP_RMn_IMMn_DISASSEMBLE(machine, ins, 64, 8);
    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_81_7_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m16, imm16
    // Opcode           : 81 /7 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm16 with r/m16.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_CMP_RMn_IMMn(machine, ins, error, 16, 16);
    return error;
}

struct ErrorInfo exec_CMP_81_7_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m16, imm16
    // Opcode           : 81 /7 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm16 with r/m16.

    TEMPLATE_CMP_RMn_IMMn_DISASSEMBLE(machine, ins, 16, 16);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_CMP_81_7_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m32, imm32
    // Opcode           : 81 /7 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm32 with r/m32.

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    TEMPLATE_CMP_RMn_IMMn(machine, ins, error, 32, 32);
    return error;
}

struct ErrorInfo exec_CMP_81_7_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m32, imm32
    // Opcode           : 81 /7 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm32 with r/m32.

    TEMPLATE_CMP_RMn_IMMn_DISASSEMBLE(machine, ins, 32, 32);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_CMP_83_7_ib_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m16, imm8
    // Opcode           : 83 /7 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm8 with r/m16.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_CMP_RMn_IMMn(machine, ins, error, 16, 8);
    return error;
}

struct ErrorInfo exec_CMP_83_7_ib_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m16, imm8
    // Opcode           : 83 /7 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm8 with r/m16.

    TEMPLATE_CMP_RMn_IMMn_DISASSEMBLE(machine, ins, 16, 8);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_CMP_83_7_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m32, imm8
    // Opcode           : 83 /7 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm8 with r/m32.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_CMP_RMn_IMMn(machine, ins, error, 32, 8);
    return error;
}

struct ErrorInfo exec_CMP_83_7_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m32, imm8
    // Opcode           : 83 /7 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare imm8 with r/m32.

    TEMPLATE_CMP_RMn_IMMn_DISASSEMBLE(machine, ins, 32, 8);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

// cat: r/m8, imm8 (no rex prefix)

struct ErrorInfo exec_CMP_80_7_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m8, imm8
    // Opcode           : 80 /7 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Compare imm8 with r/m8.

    struct ErrorInfo error = {NO_ERROR, 0};

    struct ModRMPointer dist_info = get_addr_from_modRM_64bit_byte(machine, ins);
    NORMALIZE_MODRM_POINTER_BYTE_READ(machine, dist_info, dist_addr, error)
    uint8_t src = ins->immediate.byte;
    uint8_t result = *dist_addr - src;
    CMP_SET_FLAGS(machine->rflags, *dist_addr, src, result, 8);

    return error;
}

struct ErrorInfo exec_CMP_80_7_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m8, imm8
    // Opcode           : 80 /7 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Compare imm8 with r/m8.

    char dist_buf[64];
    get_addr_str_from_modRM_64bit_byte(machine, ins, dist_buf);
    printf("cmp %s, 0x%" PRIx8 "\n", dist_buf, ins->immediate.byte);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

// cat: r/mN, rN

struct ErrorInfo exec_CMP_REX__38_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m8 , r8
    // Opcode           : REX + 38 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r8 with r/m8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_REX__38_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m8 , r8
    // Opcode           : REX + 38 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r8 with r/m8.

    // TODO: implement

    puts("not implemented: CMP r/m8 , r8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_REXW__39_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m64,r64
    // Opcode           : REX.W + 39 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r64 with r/m64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_REXW__39_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m64,r64
    // Opcode           : REX.W + 39 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r64 with r/m64.

    // TODO: implement

    puts("not implemented: CMP r/m64,r64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_38_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m8, r8
    // Opcode           : 38 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r8 with r/m8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_38_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m8, r8
    // Opcode           : 38 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r8 with r/m8.

    // TODO: implement

    puts("not implemented: CMP r/m8, r8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_39_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m16, r16
    // Opcode           : 39 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r16 with r/m16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_39_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m16, r16
    // Opcode           : 39 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r16 with r/m16.

    // TODO: implement

    puts("not implemented: CMP r/m16, r16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_39_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m32, r32
    // Opcode           : 39 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r32 with r/m32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_39_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r/m32, r32
    // Opcode           : 39 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r32 with r/m32.

    // TODO: implement

    puts("not implemented: CMP r/m32, r32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

// cat: rN, r/mN

struct ErrorInfo exec_CMP_REX__3A_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r8 , r/m8
    // Opcode           : REX + 3A /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r/m8 with r8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_REX__3A_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r8 , r/m8
    // Opcode           : REX + 3A /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r/m8 with r8.

    // TODO: implement

    puts("not implemented: CMP r8 , r/m8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_REXW__3B_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r64, r/m64
    // Opcode           : REX.W + 3B /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r/m64 with r64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_REXW__3B_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r64, r/m64
    // Opcode           : REX.W + 3B /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r/m64 with r64.

    // TODO: implement

    puts("not implemented: CMP r64, r/m64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_3A_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r8, r/m8
    // Opcode           : 3A /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r/m8 with r8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_3A_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r8, r/m8
    // Opcode           : 3A /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r/m8 with r8.

    // TODO: implement

    puts("not implemented: CMP r8, r/m8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_3B_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r16, r/m16
    // Opcode           : 3B /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r/m16 with r16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_3B_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r16, r/m16
    // Opcode           : 3B /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r/m16 with r16.

    // TODO: implement

    puts("not implemented: CMP r16, r/m16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_3B_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r32, r/m32
    // Opcode           : 3B /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r/m32 with r32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CMP_3B_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CMP r32, r/m32
    // Opcode           : 3B /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Compare r/m32 with r32.

    // TODO: implement

    puts("not implemented: CMP r32, r/m32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}