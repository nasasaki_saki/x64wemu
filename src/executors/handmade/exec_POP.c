#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

// flags: None.

// TAG: implemented here
#define TEMPLATE_POP_Rn(machine, ins, error, size_constant) do { \
    union X64Register *dist = get_register(machine, OPCODE_REG(ins->opcode[0]), REX_B(ins)); \
    dist->REGISTER_SIZE_KIND(size_constant) = MEMORY_ACCESS(UINTn_T(size_constant), machine, machine->sp.r);                          \
    machine->sp.r += (size_constant / 8);                                      \
} while (false)

#define TEMPLATE_POP_Rn_DISASSEMBLE(machine, ins, size_constant) do { \
    char *dist = get_register_str(machine, ins->opcode[0] & 0x7, REX_B(ins)); \
    char dist_buf[64];                                                    \
    format_register_str(dist, size_constant, dist_buf);                              \
    printf("pop %s\n", dist_buf);                                           \
} while (false)

struct ErrorInfo exec_POP_0F_A1_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP FS
    // Opcode           : 0F A1
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into FS; increment stack pointer by 16 bits.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_0F_A1_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP FS
    // Opcode           : 0F A1
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into FS; increment stack pointer by 16 bits.

    // TODO: implement

    puts("not implemented: POP FS");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_0F_A1(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP FS
    // Opcode           : 0F A1
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into FS; increment stack pointer by 64 bits.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_0F_A1_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP FS
    // Opcode           : 0F A1
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into FS; increment stack pointer by 64 bits.

    // TODO: implement

    puts("not implemented: POP FS");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_0F_A9_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP GS
    // Opcode           : 0F A9
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into GS; increment stack pointer by 16 bits.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_0F_A9_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP GS
    // Opcode           : 0F A9
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into GS; increment stack pointer by 16 bits.

    // TODO: implement

    puts("not implemented: POP GS");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_0F_A9(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP GS
    // Opcode           : 0F A9
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into GS; increment stack pointer by 64 bits.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_0F_A9_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP GS
    // Opcode           : 0F A9
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into GS; increment stack pointer by 64 bits.

    // TODO: implement

    puts("not implemented: POP GS");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_8F_0_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP r/m16
    // Opcode           : 8F /0
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into m16; increment stack pointer.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_8F_0_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP r/m16
    // Opcode           : 8F /0
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into m16; increment stack pointer.

    // TODO: implement

    puts("not implemented: POP r/m16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_8F_0(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP r/m64
    // Opcode           : 8F /0
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into m64; increment stack pointer. Cannot encode 32-bit operand size.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_8F_0_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP r/m64
    // Opcode           : 8F /0
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into m64; increment stack pointer. Cannot encode 32-bit operand size.

    // TODO: implement

    puts("not implemented: POP r/m64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_58_rw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP r16
    // Opcode           : 58 +rw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into r16; increment stack pointer.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_POP_Rn(machine, ins, error, 16);
    return error;
}

struct ErrorInfo exec_POP_58_rw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP r16
    // Opcode           : 58 +rw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into r16; increment stack pointer.

    TEMPLATE_POP_Rn_DISASSEMBLE(machine, ins, 16);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_POP_58_rd(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP r64
    // Opcode           : 58 +rd
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into r64; increment stack pointer. Cannot encode 32-bit operand size.

    TEMPLATE_POP_Rn(machine, ins, error, 64);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_POP_58_rd_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP r64
    // Opcode           : 58 +rd
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : opcode +rd (w)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into r64; increment stack pointer. Cannot encode 32-bit operand size.

    TEMPLATE_POP_Rn_DISASSEMBLE(machine, ins, 64);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_POP_1F(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP DS
    // Opcode           : 1F
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into DS; increment stack pointer.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_1F_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP DS
    // Opcode           : 1F
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into DS; increment stack pointer.

    // TODO: implement

    puts("not implemented: POP DS");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_07(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP ES
    // Opcode           : 07
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into ES; increment stack pointer.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_07_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP ES
    // Opcode           : 07
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into ES; increment stack pointer.

    // TODO: implement

    puts("not implemented: POP ES");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_17(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP SS
    // Opcode           : 17
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into SS; increment stack pointer.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_POP_17_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : POP SS
    // Opcode           : 17
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : NA
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Pop top of stack into SS; increment stack pointer.

    // TODO: implement

    puts("not implemented: POP SS");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}