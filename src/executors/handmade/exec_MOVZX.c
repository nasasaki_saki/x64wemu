#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>

#define TEMPLATE_MOVZX(machine, ins, error, dist_size_constant, src_size_constant) do { \
    union X64Register *dist = get_register(machine, ins->mod_rm.reg, REX_R(ins));       \
    struct ModRMPointer src_info = get_addr_from_modRM_64bit(machine, ins);             \
    NORMALIZE_MODRM_POINTER_READ(machine, src_info, src_addr, error, src_size_constant)                          \
    dist->REGISTER_SIZE_KIND(dist_size_constant) = (UINTn_T(dist_size_constant))*src_addr;                                                      \
} while (false)

#define TEMPLATE_MOVZX_DISASSEMBLE(machine, ins, dist_size_constant, src_size_constant) do { \
    char dist_buf[64];                                                \
    format_register_str(get_register_str(machine, ins->mod_rm.reg, REX_R(ins)), dist_size_constant, dist_buf); \
    char src_buf[64];                                               \
    char *register_str = get_addr_str_from_modRM_64bit(machine, ins, src_buf);                     \
    if (register_str != NULL) {                                      \
        format_register_str(register_str, src_size_constant, src_buf);       \
    }                                                                \
    printf("movzx %s, %s\n", dist_buf, src_buf);                      \
} while(false)

// cat: rN, r/mN

struct ErrorInfo exec_MOVZX_REXW__0F_B6_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOVZX r64, r/m8
    // Opcode           : REX.W + 0F B6 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move byte to quadword, zero-extension.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOVZX(machine, ins, error, 64, 8);
    return error;
}

struct ErrorInfo exec_MOVZX_REXW__0F_B6_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOVZX r64, r/m8
    // Opcode           : REX.W + 0F B6 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move byte to quadword, zero-extension.

    TEMPLATE_MOVZX_DISASSEMBLE(machine, ins, 64, 8);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_MOVZX_REXW__0F_B7_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOVZX r64, r/m16
    // Opcode           : REX.W + 0F B7 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move word to quadword, zero-extension.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOVZX(machine, ins, error, 64, 16);
    return error;
}

struct ErrorInfo exec_MOVZX_REXW__0F_B7_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOVZX r64, r/m16
    // Opcode           : REX.W + 0F B7 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move word to quadword, zero-extension.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOVZX_DISASSEMBLE(machine, ins, 64, 16);
    return error;
}

struct ErrorInfo exec_MOVZX_0F_B6_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOVZX r16, r/m8
    // Opcode           : 0F B6 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move byte to word with zero-extension.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOVZX(machine, ins, error, 16, 8);
    return error;
}

struct ErrorInfo exec_MOVZX_0F_B6_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOVZX r16, r/m8
    // Opcode           : 0F B6 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move byte to word with zero-extension.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOVZX_DISASSEMBLE(machine, ins, 16, 8);
    return error;
}

struct ErrorInfo exec_MOVZX_0F_B6_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOVZX r32, r/m8
    // Opcode           : 0F B6 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move byte to doubleword, zero-extension.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOVZX(machine, ins, error, 32, 8);
    return error;
}

struct ErrorInfo exec_MOVZX_0F_B6_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOVZX r32, r/m8
    // Opcode           : 0F B6 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move byte to doubleword, zero-extension.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOVZX_DISASSEMBLE(machine, ins, 32, 8);
    return error;
}

struct ErrorInfo exec_MOVZX_0F_B7_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOVZX r32, r/m16
    // Opcode           : 0F B7 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move word to doubleword, zero-extension.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOVZX(machine, ins, error, 32, 16);
    return error;
}

struct ErrorInfo exec_MOVZX_0F_B7_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOVZX r32, r/m16
    // Opcode           : 0F B7 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Move word to doubleword, zero-extension.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_MOVZX_DISASSEMBLE(machine, ins, 32, 16);
    return error;
}