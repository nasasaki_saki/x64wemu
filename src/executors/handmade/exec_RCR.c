#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

// flags: If the masked count is 0, the flags are not affected.
// If the masked count is 1, then the OF flag is affected,
// otherwise (masked count is greater than 1) the OF flag is undefined.
//     The CF flag is affected when the masked count is nonzero.
//     The SF, ZF, AF, and PF flags are always unaffected.
// TODO: implement

// TAG: implemented here
// TODO: flags

#define TEMPLATE_RCR_RMn_1(machine, ins, error, size_constant) do {                                 \
    struct ModRMPointer dist_info = get_addr_from_modRM_64bit(machine, ins);                        \
    UINTn_T(size_constant) *dist_addr;                                                              \
    if (dist_info.kind == MODRM_REGISTER) {                                                         \
        dist_addr = &dist_info.register_p->REGISTER_SIZE_KIND(size_constant);                       \
    } else if (dist_info.kind == MODRM_MEMORY) {                                                    \
        dist_addr = &MEMORY_ACCESS_WRITE(UINTn_T(size_constant), machine, dist_info.memory_addr);   \
    } else {                                                                                        \
        error.code = ERROR_PANIC;                                                                   \
        return error;                                                                               \
    }                                                                                               \
    UINTn_T(size_constant) dist_value = *dist_addr;                                                 \
    UINTn_T(size_constant) next_cf = dist_value & 0x0001;                                           \
    dist_value >>= 1;                                                                               \
    dist_value |= (UINTn_T(size_constant))machine->rflags.carry << (size_constant ## u - 1u);                               \
    machine->rflags.carry = next_cf;                                                                \
    *dist_addr = dist_value;                                                                        \
} while (false)

#define TEMPLATE_RCR_RMn_1_DISASSEMBLE(machine, ins, size_constant) do {        \
    char dist_buf[64];                                                          \
    char *register_str =get_addr_str_from_modRM_64bit(machine, ins, dist_buf);  \
    if (register_str != NULL) {                                                 \
        format_register_str(register_str, size_constant, dist_buf);             \
    }                                                                           \
    printf("rcr %s, 1\n", dist_buf);                                            \
} while (false)

struct ErrorInfo exec_RCR_REX__D0_3(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m8, 1
    // Opcode           : REX + D0 /3
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : 1
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 9 bits (CF, r/m8) right once.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_REX__D0_3_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m8, 1
    // Opcode           : REX + D0 /3
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : 1
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 9 bits (CF, r/m8) right once.

    // TODO: implement

    puts("not implemented: RCR r/m8, 1");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_REX__D2_3(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m8, CL
    // Opcode           : REX + D2 /3
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : CL
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 9 bits (CF, r/m8) right CL times.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_REX__D2_3_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m8, CL
    // Opcode           : REX + D2 /3
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : CL
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 9 bits (CF, r/m8) right CL times.

    // TODO: implement

    puts("not implemented: RCR r/m8, CL");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_REX__C0_3_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m8, imm8
    // Opcode           : REX + C0 /3 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 9 bits (CF, r/m8) right imm8 times.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_REX__C0_3_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m8, imm8
    // Opcode           : REX + C0 /3 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 9 bits (CF, r/m8) right imm8 times.

    // TODO: implement

    puts("not implemented: RCR r/m8, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_REXW__D1_3(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m64, 1
    // Opcode           : REX.W + D1 /3
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : 1
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 65 bits (CF, r/m64) right once. Uses a 6 bit count.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_RCR_RMn_1(machine, ins, error, 64);

    return error;
}

struct ErrorInfo exec_RCR_REXW__D1_3_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m64, 1
    // Opcode           : REX.W + D1 /3
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : 1
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 65 bits (CF, r/m64) right once. Uses a 6 bit count.

    TEMPLATE_RCR_RMn_1_DISASSEMBLE(machine, ins, 64);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_RCR_REXW__D3_3(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m64, CL
    // Opcode           : REX.W + D3 /3
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : CL
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 65 bits (CF, r/m64) right CL times. Uses a 6 bit count.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_REXW__D3_3_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m64, CL
    // Opcode           : REX.W + D3 /3
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : CL
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 65 bits (CF, r/m64) right CL times. Uses a 6 bit count.

    // TODO: implement

    puts("not implemented: RCR r/m64, CL");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_REXW__C1_3_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m64, imm8
    // Opcode           : REX.W + C1 /3 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 65 bits (CF, r/m64) right imm8 times. Uses a 6 bit count.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_REXW__C1_3_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m64, imm8
    // Opcode           : REX.W + C1 /3 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 65 bits (CF, r/m64) right imm8 times. Uses a 6 bit count.

    // TODO: implement

    puts("not implemented: RCR r/m64, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_D0_3(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m8, 1
    // Opcode           : D0 /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : 1
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 9 bits (CF, r/m8) right once.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_D0_3_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m8, 1
    // Opcode           : D0 /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : 1
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 9 bits (CF, r/m8) right once.

    // TODO: implement

    puts("not implemented: RCR r/m8, 1");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_D2_3(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m8, CL
    // Opcode           : D2 /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : CL
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 9 bits (CF, r/m8) right CL times.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_D2_3_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m8, CL
    // Opcode           : D2 /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : CL
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 9 bits (CF, r/m8) right CL times.

    // TODO: implement

    puts("not implemented: RCR r/m8, CL");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_C0_3_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m8, imm8
    // Opcode           : C0 /3 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 9 bits (CF, r/m8) right imm8 times.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_C0_3_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m8, imm8
    // Opcode           : C0 /3 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 9 bits (CF, r/m8) right imm8 times.

    // TODO: implement

    puts("not implemented: RCR r/m8, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_D1_3_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m16, 1
    // Opcode           : D1 /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : 1
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 17 bits (CF, r/m16) right once.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_RCR_RMn_1(machine, ins, error, 16);

    return error;
}

struct ErrorInfo exec_RCR_D1_3_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m16, 1
    // Opcode           : D1 /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : 1
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 17 bits (CF, r/m16) right once.

    // TODO: implement

    TEMPLATE_RCR_RMn_1_DISASSEMBLE(machine, ins, 16);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_RCR_D1_3(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m32, 1
    // Opcode           : D1 /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : 1
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 33 bits (CF, r/m32) right once. Uses a 6 bit count.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_RCR_RMn_1(machine, ins, error, 32);

    return error;
}

struct ErrorInfo exec_RCR_D1_3_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m32, 1
    // Opcode           : D1 /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : 1
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 33 bits (CF, r/m32) right once. Uses a 6 bit count.

    TEMPLATE_RCR_RMn_1_DISASSEMBLE(machine, ins, 32);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_RCR_D3_3_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m16, CL
    // Opcode           : D3 /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : CL
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 17 bits (CF, r/m16) right CL times.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_D3_3_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m16, CL
    // Opcode           : D3 /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : CL
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 17 bits (CF, r/m16) right CL times.

    // TODO: implement

    puts("not implemented: RCR r/m16, CL");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_D3_3(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m32, CL
    // Opcode           : D3 /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : CL
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 33 bits (CF, r/m32) right CL times.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_D3_3_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m32, CL
    // Opcode           : D3 /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : CL
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 33 bits (CF, r/m32) right CL times.

    // TODO: implement

    puts("not implemented: RCR r/m32, CL");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_C1_3_ib_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m16, imm8
    // Opcode           : C1 /3 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 17 bits (CF, r/m16) right imm8 times.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_C1_3_ib_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m16, imm8
    // Opcode           : C1 /3 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 17 bits (CF, r/m16) right imm8 times.

    // TODO: implement

    puts("not implemented: RCR r/m16, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_C1_3_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m32, imm8
    // Opcode           : C1 /3 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 33 bits (CF, r/m32) right imm8 times.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_RCR_C1_3_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : RCR r/m32, imm8
    // Opcode           : C1 /3 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : imm8
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Rotate 33 bits (CF, r/m32) right imm8 times.

    // TODO: implement

    puts("not implemented: RCR r/m32, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}