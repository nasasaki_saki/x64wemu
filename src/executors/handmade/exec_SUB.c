#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>

// flags: The OF, SF, ZF, AF, PF, and CF flags are set according to the result.
// TODO: implement OF, AF, CF
#define SUB_SET_FLAGS(rflags, before_a, before_b, result, size_constant) do { \
    rflags.overflow;                                                          \
    rflags.sign = SIGN(result);                                               \
    rflags.zero = ZERO(result);                                               \
    rflags.adjust;                                                                          \
    rflags.parity = parity((uint64_t)result);                                           \
    rflags.carry;                                                                              \
} while (false)

// *a*, immN

struct ErrorInfo exec_SUB_REXW__2D_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB RAX, imm32
    // Opcode           : REX.W + 2D id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract imm32 sign-extended to 64-bits from RAX.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_REXW__2D_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB RAX, imm32
    // Opcode           : REX.W + 2D id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract imm32 sign-extended to 64-bits from RAX.

    // TODO: implement

    puts("not implemented: SUB RAX, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

// r/mn, imm

// TAG: implemented here
#define TEMPLATE_SUB_RMn_IMMn(machine, ins, error, dist_size_constant, src_size_constant) do { \
    struct ModRMPointer dist_info = get_addr_from_modRM_64bit(machine, ins); \
    UINTn_T(dist_size_constant) *dist_addr;                                                   \
    if (dist_info.kind == MODRM_MEMORY) {                                  \
        dist_addr = &MEMORY_ACCESS_WRITE(UINTn_T(dist_size_constant), machine, dist_info.memory_addr); \
    } else if (dist_info.kind == MODRM_REGISTER) {                         \
        dist_addr = &dist_info.register_p->REGISTER_SIZE_KIND(dist_size_constant);                                                   \
    } else {                                                               \
        error.code = ERROR_PANIC;                                              \
        return error;                                                          \
    }                                                                      \
    UINTn_T(dist_size_constant) before = *dist_addr;                                          \
    UINTn_T(dist_size_constant) src = (INTn_T(dist_size_constant))(INTn_T(src_size_constant))ins->immediate.dword;                 \
    *dist_addr -= src;                                                     \
    SUB_SET_FLAGS(machine->rflags, before, src, *dist_addr, dist_size_constant);           \
} while (false)

#define TEMPLATE_SUB_RMn_IMMn_DISASSEMBLE(machine, ins, dist_size_constant, src_size_constant) do { \
    char dist_buf[64];                                                                                  \
    char *register_str = get_addr_str_from_modRM_64bit(machine, ins, dist_buf);                         \
    if (register_str != NULL) {                                                                         \
        format_register_str(register_str, dist_size_constant, dist_buf);                                                    \
    }                                                                                               \
    UINTn_T(dist_size_constant) src = (INTn_T(dist_size_constant))(INTn_T(src_size_constant))ins->immediate.dword;                 \
    printf("sub %s, 0x%" PRIx ## dist_size_constant "\n", dist_buf, src);                                                   \
} while (false)

struct ErrorInfo exec_SUB_REX__80_5_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m8, imm8
    // Opcode           : REX + 80 /5 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Subtract imm8 from r/m8.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_SUB_RMn_IMMn(machine, ins, error, 8, 8);
    return error;
}

struct ErrorInfo exec_SUB_REX__80_5_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m8, imm8
    // Opcode           : REX + 80 /5 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Subtract imm8 from r/m8.

    TEMPLATE_SUB_RMn_IMMn_DISASSEMBLE(machine, ins, 8, 8);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_SUB_REXW__81_5_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m64, imm32
    // Opcode           : REX.W + 81 /5 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract imm32 sign-extended to 64-bits from r/m64.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_SUB_RMn_IMMn(machine, ins, error, 64, 32);

    return error;
}

struct ErrorInfo exec_SUB_REXW__81_5_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m64, imm32
    // Opcode           : REX.W + 81 /5 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract imm32 sign-extended to 64-bits from r/m64.

    TEMPLATE_SUB_RMn_IMMn_DISASSEMBLE(machine, ins, 64, 32);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_SUB_REXW__83_5_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m64, imm8
    // Opcode           : REX.W + 83 /5 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract sign-extended imm8 from r/m64.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_SUB_RMn_IMMn(machine, ins, error, 64, 8);

    return error;
}

struct ErrorInfo exec_SUB_REXW__83_5_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m64, imm8
    // Opcode           : REX.W + 83 /5 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract sign-extended imm8 from r/m64.

    TEMPLATE_SUB_RMn_IMMn_DISASSEMBLE(machine, ins, 64, 8);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_SUB_81_5_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m16, imm16
    // Opcode           : 81 /5 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Subtract imm16 from r/m16.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_SUB_RMn_IMMn(machine, ins, error, 16, 16);

    return error;
}

struct ErrorInfo exec_SUB_81_5_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m16, imm16
    // Opcode           : 81 /5 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Subtract imm16 from r/m16.

    TEMPLATE_SUB_RMn_IMMn_DISASSEMBLE(machine, ins, 16, 16);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_SUB_81_5_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m32, imm32
    // Opcode           : 81 /5 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Subtract imm32 from r/m32.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_SUB_RMn_IMMn(machine, ins, error, 32, 32);

    return error;
}

struct ErrorInfo exec_SUB_81_5_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m32, imm32
    // Opcode           : 81 /5 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Subtract imm32 from r/m32.

    TEMPLATE_SUB_RMn_IMMn_DISASSEMBLE(machine, ins, 32, 32);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_SUB_83_5_ib_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m16, imm8
    // Opcode           : 83 /5 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Subtract sign-extended imm8 from r/m16.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_SUB_RMn_IMMn(machine, ins, error, 16, 8);

    return error;
}

struct ErrorInfo exec_SUB_83_5_ib_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m16, imm8
    // Opcode           : 83 /5 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Subtract sign-extended imm8 from r/m16.

    TEMPLATE_SUB_RMn_IMMn_DISASSEMBLE(machine, ins, 16, 8);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_SUB_83_5_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m32, imm8
    // Opcode           : 83 /5 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Subtract sign-extended imm8 from r/m32.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_SUB_RMn_IMMn(machine, ins, error, 32, 8);

    return error;
}

struct ErrorInfo exec_SUB_83_5_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m32, imm8
    // Opcode           : 83 /5 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Subtract sign-extended imm8 from r/m32.

    TEMPLATE_SUB_RMn_IMMn_DISASSEMBLE(machine, ins, 32, 8);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

// r8, imm8 (no rex prefix)

struct ErrorInfo exec_SUB_80_5_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m8, imm8
    // Opcode           : 80 /5 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Subtract imm8 from r/m8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_80_5_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m8, imm8
    // Opcode           : 80 /5 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Subtract imm8 from r/m8.

    // TODO: implement

    puts("not implemented: SUB r/m8, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_REX__28_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m8, r8
    // Opcode           : REX + 28 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r8 from r/m8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_REX__28_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m8, r8
    // Opcode           : REX + 28 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r8 from r/m8.

    // TODO: implement

    puts("not implemented: SUB r/m8, r8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_REXW__29_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m64, r64
    // Opcode           : REX.W + 29 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r64 from r/m64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_REXW__29_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m64, r64
    // Opcode           : REX.W + 29 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r64 from r/m64.

    // TODO: implement

    puts("not implemented: SUB r/m64, r64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_REX__2A_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r8, r/m8
    // Opcode           : REX + 2A /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r/m8 from r8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_REX__2A_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r8, r/m8
    // Opcode           : REX + 2A /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r/m8 from r8.

    // TODO: implement

    puts("not implemented: SUB r8, r/m8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_REXW__2B_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r64, r/m64
    // Opcode           : REX.W + 2B /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r/m64 from r64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_REXW__2B_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r64, r/m64
    // Opcode           : REX.W + 2B /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r/m64 from r64.

    // TODO: implement

    puts("not implemented: SUB r64, r/m64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_2C_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB AL, imm8
    // Opcode           : 2C ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract imm8 from AL.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_2C_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB AL, imm8
    // Opcode           : 2C ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract imm8 from AL.

    // TODO: implement

    puts("not implemented: SUB AL, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_2D_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB AX, imm16
    // Opcode           : 2D iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract imm16 from AX.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_2D_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB AX, imm16
    // Opcode           : 2D iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract imm16 from AX.

    // TODO: implement

    puts("not implemented: SUB AX, imm16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_2D_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB EAX, imm32
    // Opcode           : 2D id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract imm32 from EAX.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_2D_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB EAX, imm32
    // Opcode           : 2D id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/26/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract imm32 from EAX.

    // TODO: implement

    puts("not implemented: SUB EAX, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_28_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m8, r8
    // Opcode           : 28 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r8 from r/m8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_28_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m8, r8
    // Opcode           : 28 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r8 from r/m8.

    // TODO: implement

    puts("not implemented: SUB r/m8, r8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_29_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m16, r16
    // Opcode           : 29 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r16 from r/m16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_29_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m16, r16
    // Opcode           : 29 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r16 from r/m16.

    // TODO: implement

    puts("not implemented: SUB r/m16, r16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_29_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m32, r32
    // Opcode           : 29 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r32 from r/m32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_29_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r/m32, r32
    // Opcode           : 29 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r32 from r/m32.

    // TODO: implement

    puts("not implemented: SUB r/m32, r32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_2A_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r8, r/m8
    // Opcode           : 2A /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r/m8 from r8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_2A_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r8, r/m8
    // Opcode           : 2A /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r/m8 from r8.

    // TODO: implement

    puts("not implemented: SUB r8, r/m8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_2B_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r16, r/m16
    // Opcode           : 2B /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r/m16 from r16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_2B_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r16, r/m16
    // Opcode           : 2B /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r/m16 from r16.

    // TODO: implement

    puts("not implemented: SUB r16, r/m16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_2B_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r32, r/m32
    // Opcode           : 2B /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r/m32 from r32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_SUB_2B_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SUB r32, r/m32
    // Opcode           : 2B /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Subtract r/m32 from r32.

    // TODO: implement

    puts("not implemented: SUB r32, r/m32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}