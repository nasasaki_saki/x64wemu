#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>

struct ErrorInfo exec_SETE_REX__0F_94(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SETE r/m8
    // Opcode           : REX + 0F 94
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Set byte if equal (ZF=1).

    struct ErrorInfo error = {NO_ERROR, 0};

    struct ModRMPointer dist_info = get_addr_from_modRM_64bit(machine, ins);
    uint8_t *dist_addr;
    if (dist_info.kind == MODRM_MEMORY) {
        dist_addr = &MEMORY_ACCESS_WRITE(uint8_t, machine, dist_info.memory_addr);
    } else if (dist_info.kind == MODRM_REGISTER) {
        dist_addr = &dist_info.register_p->l;
    } else {
        error.code = ERROR_PANIC;
        return error;
    }
    *dist_addr = machine->rflags.zero;

    return error;
}

struct ErrorInfo exec_SETE_REX__0F_94_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SETE r/m8
    // Opcode           : REX + 0F 94
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Set byte if equal (ZF=1).

    char dist_buf[64];
    char *register_str = get_addr_str_from_modRM_64bit(machine, ins, dist_buf);
    if (register_str != NULL) {
        format_register_str(register_str, 8, dist_buf);
    }
    printf("sete %s\n", dist_buf);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_SETE_0F_94(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SETE r/m8
    // Opcode           : 0F 94
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Set byte if equal (ZF=1).

    struct ErrorInfo error = {NO_ERROR, 0};

    struct ModRMPointer dist_info = get_addr_from_modRM_64bit_byte(machine, ins);
    uint8_t *dist_addr;
    if (dist_info.kind == MODRM_MEMORY) {
        dist_addr = &MEMORY_ACCESS_WRITE(uint8_t, machine, dist_info.memory_addr);
    } else if (dist_info.kind == MODRM_BYTE_REGISTER) {
        if (dist_info.byte_register.pos == LOW) {
            dist_addr = &dist_info.byte_register.register_p->l;
        } else if (dist_info.byte_register.pos == HIGH) {
            dist_addr = &dist_info.byte_register.register_p->h;
        } ELSE_ERROR_RET(error, ERROR_PANIC)
    } ELSE_ERROR_RET(error, ERROR_PANIC)

    *dist_addr = machine->rflags.zero;

    return error;
}

struct ErrorInfo exec_SETE_0F_94_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : SETE r/m8
    // Opcode           : 0F 94
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Set byte if equal (ZF=1).

    char dist_buf[64];
    get_addr_str_from_modRM_64bit_byte(machine, ins, dist_buf);
    printf("sete %s\n", dist_buf);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}
