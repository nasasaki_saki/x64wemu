#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#define TEMPLATE_LEA(machine, ins, error, size_constant) do {                               \
    union X64Register *dist_register = get_register(machine, ins->mod_rm.reg, REX_R(ins));  \
    struct ModRMPointer src_info = get_addr_from_modRM_64bit(machine, ins);                 \
    if (src_info.kind != MODRM_MEMORY) {                                                    \
        error.code = ERROR_UD;                                                              \
        return error;                                                                       \
    }                                                                                       \
    dist_register->REGISTER_SIZE_KIND(size_constant) = src_info.memory_addr;                \
} while (false)

#define TEMPLATE_LEA_DISASSEMBLE(machine, ins, error, size_constant) do {                                   \
    char dist_buf[64];                                                                                      \
    char src_buf[64];                                                                                       \
    format_register_str(get_register_str(machine, ins->mod_rm.reg, REX_R(ins)), size_constant, dist_buf);   \
    char *src_register = get_addr_str_from_modRM_64bit(machine, ins, src_buf);                              \
    if (src_register != NULL) {                                                                             \
        error.code = ERROR_UD;                                                                              \
        return error;                                                                                       \
    }                                                                                                       \
    printf("lea %s, %s", dist_buf, src_buf);                                                                \
} while (false)

struct ErrorInfo exec_LEA_REXW__8D_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : LEA r64,m
    // Opcode           : REX.W + 8D /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Store effective address for m in register r64.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_LEA(machine, ins, error, 64);
    return error;
}

struct ErrorInfo exec_LEA_REXW__8D_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : LEA r64,m
    // Opcode           : REX.W + 8D /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Store effective address for m in register r64.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_LEA_DISASSEMBLE(machine, ins, error, 64);

    return error;
}

struct ErrorInfo exec_LEA_8D_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : LEA r16,m
    // Opcode           : 8D /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Store effective address for m in register r16.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_LEA(machine, ins, error, 16);
    return error;
}

struct ErrorInfo exec_LEA_8D_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : LEA r16,m
    // Opcode           : 8D /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Store effective address for m in register r16.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_LEA_DISASSEMBLE(machine, ins, error, 16);
    return error;
}

struct ErrorInfo exec_LEA_8D_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : LEA r32,m
    // Opcode           : 8D /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Store effective address for m in register r32.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_LEA(machine, ins, error, 32);
    return error;
}

struct ErrorInfo exec_LEA_8D_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : LEA r32,m
    // Opcode           : 8D /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Store effective address for m in register r32.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_LEA_DISASSEMBLE(machine, ins, error, 32);
    return error;
}