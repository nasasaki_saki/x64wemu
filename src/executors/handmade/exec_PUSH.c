#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

// flags: None.

// rN

// TAG: implemented here
#define TEMPLATE_PUSH_Rn(machine, ins, error, size_constant) do { \
    union X64Register *src = get_register(machine, OPCODE_REG(ins->opcode[0]), REX_B(ins)); \
    MEMORY_ACCESS_WRITE(UINTn_T(size_constant), machine, machine->sp.r -= (size_constant / 8)) = src->REGISTER_SIZE_KIND(size_constant);                                \
} while (false)

#define TEMPLATE_PUSH_Rn_DISASSEMBLE(machine, ins, size_constant) do { \
    char *src = get_register_str(machine, ins->opcode[0] & 0x7, REX_B(ins)); \
    char src_buf[64];                                                    \
    format_register_str(src, size_constant, src_buf);                              \
    printf("push %s\n", src_buf);                                           \
} while (false)

struct ErrorInfo exec_PUSH_50_rw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH r16
    // Opcode           : 50 +rw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : opcode +rd (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Push r16.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_PUSH_Rn(machine, ins, error, 16);
    return error;
}

struct ErrorInfo exec_PUSH_50_rw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH r16
    // Opcode           : 50 +rw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : opcode +rd (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Push r16.

    TEMPLATE_PUSH_Rn_DISASSEMBLE(machine, ins, 16);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_PUSH_50_rd(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH r64
    // Opcode           : 50 +rd
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : opcode +rd (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Push r64.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_PUSH_Rn(machine, ins, error, 64);
    return error;
}

struct ErrorInfo exec_PUSH_50_rd_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH r64
    // Opcode           : 50 +rd
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : opcode +rd (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Push r64.

    TEMPLATE_PUSH_Rn_DISASSEMBLE(machine, ins, 64);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

// not implement

struct ErrorInfo exec_PUSH_0F_A0(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH FS
    // Opcode           : 0F A0
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push FS.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_0F_A0_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH FS
    // Opcode           : 0F A0
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push FS.

    // TODO: implement

    puts("not implemented: PUSH FS");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_0F_A8(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH GS
    // Opcode           : 0F A8
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push GS.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_0F_A8_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH GS
    // Opcode           : 0F A8
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push GS.

    // TODO: implement

    puts("not implemented: PUSH GS");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_FF_6_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH r/m16
    // Opcode           : FF /6
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Push r/m16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_FF_6_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH r/m16
    // Opcode           : FF /6
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Push r/m16.

    // TODO: implement

    puts("not implemented: PUSH r/m16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_FF_6(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH r/m64
    // Opcode           : FF /6
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Push r/m64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_FF_6_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH r/m64
    // Opcode           : FF /6
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : NA
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : Push r/m64.

    // TODO: implement

    puts("not implemented: PUSH r/m64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_6A_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH imm8
    // Opcode           : 6A ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push imm8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_6A_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH imm8
    // Opcode           : 6A ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push imm8.

    // TODO: implement

    puts("not implemented: PUSH imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_68_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH imm16
    // Opcode           : 68 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push imm16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_68_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH imm16
    // Opcode           : 68 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push imm16.

    // TODO: implement

    puts("not implemented: PUSH imm16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_68_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH imm32
    // Opcode           : 68 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push imm32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_68_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH imm32
    // Opcode           : 68 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push imm32.

    // TODO: implement

    puts("not implemented: PUSH imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_0E(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH CS
    // Opcode           : 0E
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push CS.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_0E_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH CS
    // Opcode           : 0E
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push CS.

    // TODO: implement

    puts("not implemented: PUSH CS");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_16(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH SS
    // Opcode           : 16
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push SS.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_16_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH SS
    // Opcode           : 16
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push SS.

    // TODO: implement

    puts("not implemented: PUSH SS");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_1E(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH DS
    // Opcode           : 1E
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push DS.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_1E_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH DS
    // Opcode           : 1E
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push DS.

    // TODO: implement

    puts("not implemented: PUSH DS");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_06(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH ES
    // Opcode           : 06
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push ES.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_PUSH_06_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : PUSH ES
    // Opcode           : 06
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Push ES.

    // TODO: implement

    puts("not implemented: PUSH ES");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}