#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>


// rmN, rN

// TAG: implemented here
// TODO: flags
#define TEMPLATE_XOR_RMn_Rn(machine, ins, error, size_constant) do { \
    struct ModRMPointer dist_info =  get_addr_from_modRM_64bit(machine, ins);         \
    UINTn_T(size_constant) *dist_addr;                                                              \
    if (dist_info.kind == MODRM_MEMORY) {                                             \
        dist_addr = &MEMORY_ACCESS_WRITE(UINTn_T(size_constant), machine, dist_info.memory_addr);             \
    } else if (dist_info.kind == MODRM_REGISTER) {                                    \
        dist_addr = &dist_info.register_p->REGISTER_SIZE_KIND(size_constant);                                             \
    } else {                                                                          \
        error.code = ERROR_PANIC;                                                         \
        return error;                                                                     \
    }                                                                                 \
    union X64Register *src_register = get_register(machine, ins->mod_rm.reg, REX_R(ins)); \
    *dist_addr = *dist_addr ^ src_register->REGISTER_SIZE_KIND(size_constant);                                        \
} while (false)

#define TEMPLATE_XOR_RMn_Rn_DISASSEMBLE(machine, ins, size_constant) do { \
    char dist_buf[64];                                                        \
    char *register_str =get_addr_str_from_modRM_64bit(machine, ins, dist_buf);\
    if (register_str != NULL) {                                               \
        format_register_str(register_str, size_constant, dist_buf);                          \
    }                                                                         \
    char src_buf[64];                                                         \
    format_register_str(get_register_str(machine, ins->mod_rm.reg, REX_R(ins)), size_constant, src_buf); \
    printf("xor %s, %s\n", dist_buf, src_buf);                                \
} while (false)

struct ErrorInfo exec_XOR_REX__30_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m8, r8
    // Opcode           : REX + 30 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m8 XOR r8.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_XOR_RMn_Rn(machine, ins, error, 8);

    return error;
}

struct ErrorInfo exec_XOR_REX__30_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m8, r8
    // Opcode           : REX + 30 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m8 XOR r8.

    TEMPLATE_XOR_RMn_Rn_DISASSEMBLE(machine, ins, 8);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}



struct ErrorInfo exec_XOR_REXW__31_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m64, r64
    // Opcode           : REX.W + 31 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m64 XOR r64.

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_XOR_RMn_Rn(machine, ins, error, 64);

    return error;
}

struct ErrorInfo exec_XOR_REXW__31_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m64, r64
    // Opcode           : REX.W + 31 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m64 XOR r64.

    TEMPLATE_XOR_RMn_Rn_DISASSEMBLE(machine, ins, 64);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}


struct ErrorInfo exec_XOR_31_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m16, r16
    // Opcode           : 31 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m16 XOR r16.


    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_XOR_RMn_Rn(machine, ins, error, 16);

    return error;
}

struct ErrorInfo exec_XOR_31_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m16, r16
    // Opcode           : 31 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m16 XOR r16.

    TEMPLATE_XOR_RMn_Rn_DISASSEMBLE(machine, ins, 16);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}


struct ErrorInfo exec_XOR_31_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m32, r32
    // Opcode           : 31 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m32 XOR r32.

    // TODO: flags

    struct ErrorInfo error = {NO_ERROR, 0};

    TEMPLATE_XOR_RMn_Rn(machine, ins, error, 32);

    return error;
}

struct ErrorInfo exec_XOR_31_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m32, r32
    // Opcode           : 31 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : r/m32 XOR r32.

    TEMPLATE_XOR_RMn_Rn_DISASSEMBLE(machine, ins, 32);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

// not implemented

struct ErrorInfo exec_XOR_REXW__35_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR RAX, imm32
    // Opcode           : REX.W + 35 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : RAX XOR imm32 (sign-extended).

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_REXW__35_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR RAX, imm32
    // Opcode           : REX.W + 35 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : RAX XOR imm32 (sign-extended).

    // TODO: implement

    puts("not implemented: XOR RAX, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_REX__80_6_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m8, imm8
    // Opcode           : REX + 80 /6 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m8 XOR imm8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_REX__80_6_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m8, imm8
    // Opcode           : REX + 80 /6 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m8 XOR imm8.

    // TODO: implement

    puts("not implemented: XOR r/m8, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_REXW__81_6_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m64, imm32
    // Opcode           : REX.W + 81 /6 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m64 XOR imm32 (sign-extended).

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_REXW__81_6_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m64, imm32
    // Opcode           : REX.W + 81 /6 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m64 XOR imm32 (sign-extended).

    // TODO: implement

    puts("not implemented: XOR r/m64, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_REXW__83_6_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m64, imm8
    // Opcode           : REX.W + 83 /6 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m64 XOR imm8 (sign-extended).

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_REXW__83_6_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m64, imm8
    // Opcode           : REX.W + 83 /6 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m64 XOR imm8 (sign-extended).

    // TODO: implement

    puts("not implemented: XOR r/m64, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_REX__32_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r8, r/m8
    // Opcode           : REX + 32 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r8 XOR r/m8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_REX__32_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r8, r/m8
    // Opcode           : REX + 32 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r8 XOR r/m8.

    // TODO: implement

    puts("not implemented: XOR r8, r/m8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_REXW__33_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r64, r/m64
    // Opcode           : REX.W + 33 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r64 XOR r/m64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_REXW__33_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r64, r/m64
    // Opcode           : REX.W + 33 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r64 XOR r/m64.

    // TODO: implement

    puts("not implemented: XOR r64, r/m64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_34_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR AL, imm8
    // Opcode           : 34 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AL XOR imm8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_34_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR AL, imm8
    // Opcode           : 34 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AL XOR imm8.

    // TODO: implement

    puts("not implemented: XOR AL, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_35_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR AX, imm16
    // Opcode           : 35 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AX XOR imm16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_35_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR AX, imm16
    // Opcode           : 35 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AX XOR imm16.

    // TODO: implement

    puts("not implemented: XOR AX, imm16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_35_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR EAX, imm32
    // Opcode           : 35 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : EAX XOR imm32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_35_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR EAX, imm32
    // Opcode           : 35 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : EAX XOR imm32.

    // TODO: implement

    puts("not implemented: XOR EAX, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_80_6_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m8, imm8
    // Opcode           : 80 /6 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m8 XOR imm8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_80_6_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m8, imm8
    // Opcode           : 80 /6 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m8 XOR imm8.

    // TODO: implement

    puts("not implemented: XOR r/m8, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_81_6_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m16, imm16
    // Opcode           : 81 /6 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m16 XOR imm16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_81_6_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m16, imm16
    // Opcode           : 81 /6 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m16 XOR imm16.

    // TODO: implement

    puts("not implemented: XOR r/m16, imm16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_81_6_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m32, imm32
    // Opcode           : 81 /6 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m32 XOR imm32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_81_6_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m32, imm32
    // Opcode           : 81 /6 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m32 XOR imm32.

    // TODO: implement

    puts("not implemented: XOR r/m32, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_83_6_ib_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m16, imm8
    // Opcode           : 83 /6 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m16 XOR imm8 (sign-extended).

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_83_6_ib_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m16, imm8
    // Opcode           : 83 /6 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m16 XOR imm8 (sign-extended).

    // TODO: implement

    puts("not implemented: XOR r/m16, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_83_6_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m32, imm8
    // Opcode           : 83 /6 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m32 XOR imm8 (sign-extended).

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_83_6_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m32, imm8
    // Opcode           : 83 /6 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m32 XOR imm8 (sign-extended).

    // TODO: implement

    puts("not implemented: XOR r/m32, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_30_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m8, r8
    // Opcode           : 30 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m8 XOR r8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_30_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r/m8, r8
    // Opcode           : 30 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r, w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r/m8 XOR r8.

    // TODO: implement

    puts("not implemented: XOR r/m8, r8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_32_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r8, r/m8
    // Opcode           : 32 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r8 XOR r/m8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_32_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r8, r/m8
    // Opcode           : 32 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r8 XOR r/m8.

    // TODO: implement

    puts("not implemented: XOR r8, r/m8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_33_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r16, r/m16
    // Opcode           : 33 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r16 XOR r/m16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_33_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r16, r/m16
    // Opcode           : 33 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r16 XOR r/m16.

    // TODO: implement

    puts("not implemented: XOR r16, r/m16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}



struct ErrorInfo exec_XOR_33_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r32, r/m32
    // Opcode           : 33 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r32 XOR r/m32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_XOR_33_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : XOR r32, r/m32
    // Opcode           : 33 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:reg (r, w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : r32 XOR r/m32.

    // TODO: implement

    puts("not implemented: XOR r32, r/m32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

