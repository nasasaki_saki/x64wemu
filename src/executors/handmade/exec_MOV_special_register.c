//
// Created by tia on 2021/04/24.
//

#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

// flags: None.

struct ErrorInfo exec_MOV_REXR__0F_20_0(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r64, CR8
    // Opcode           : REX.R + 0F 20 /0
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move extended CR8 to r64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXR__0F_20_0_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r64, CR8
    // Opcode           : REX.R + 0F 20 /0
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move extended CR8 to r64.

    // TODO: implement

    puts("not implemented: MOV r64, CR8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXR__0F_22_0(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV CR8, r64
    // Opcode           : REX.R + 0F 22 /0
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r64 to extended CR8.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXR__0F_22_0_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV CR8, r64
    // Opcode           : REX.R + 0F 22 /0
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r64 to extended CR8.

    // TODO: implement

    puts("not implemented: MOV CR8, r64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__8C_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m64,Sreg
    // Opcode           : REX.W + 8C /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move zero extended 16-bit segment register to r/m64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__8C_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m64,Sreg
    // Opcode           : REX.W + 8C /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move zero extended 16-bit segment register to r/m64.

    // TODO: implement

    puts("not implemented: MOV r/m64,Sreg");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__8E_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV Sreg,r/m64
    // Opcode           : REX.W + 8E /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move lower 16 bits of r/m64 to segment register.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_REXW__8E_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV Sreg,r/m64
    // Opcode           : REX.W + 8E /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move lower 16 bits of r/m64 to segment register.

    // TODO: implement

    puts("not implemented: MOV Sreg,r/m64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_20_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r32, CR0-CR7
    // Opcode           : 0F 20 /r
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move control register to r32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_20_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r32, CR0-CR7
    // Opcode           : 0F 20 /r
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move control register to r32.

    // TODO: implement

    puts("not implemented: MOV r32, CR0-CR7");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_20_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r64, CR0-CR7
    // Opcode           : 0F 20 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move extended control register to r64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_20_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r64, CR0-CR7
    // Opcode           : 0F 20 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move extended control register to r64.

    // TODO: implement

    puts("not implemented: MOV r64, CR0-CR7");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_22_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV CR0-CR7, r32
    // Opcode           : 0F 22 /r
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r32 to control register.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_22_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV CR0-CR7, r32
    // Opcode           : 0F 22 /r
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r32 to control register.

    // TODO: implement

    puts("not implemented: MOV CR0-CR7, r32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_22_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV CR0-CR7, r64
    // Opcode           : 0F 22 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r64 to extended control register.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_22_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV CR0-CR7, r64
    // Opcode           : 0F 22 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r64 to extended control register.

    // TODO: implement

    puts("not implemented: MOV CR0-CR7, r64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_21_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r32, DR0-DR7
    // Opcode           : 0F 21 /r
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move debug register to r32.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_21_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r32, DR0-DR7
    // Opcode           : 0F 21 /r
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move debug register to r32.

    // TODO: implement

    puts("not implemented: MOV r32, DR0-DR7");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_21_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r64, DR0-DR7
    // Opcode           : 0F 21 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move extended debug register to r64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_21_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r64, DR0-DR7
    // Opcode           : 0F 21 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move extended debug register to r64.

    // TODO: implement

    puts("not implemented: MOV r64, DR0-DR7");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_23_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV DR0-DR7, r32
    // Opcode           : 0F 23 /r
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r32 to debug register.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_23_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV DR0-DR7, r32
    // Opcode           : 0F 23 /r
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r32 to debug register.

    // TODO: implement

    puts("not implemented: MOV DR0-DR7, r32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_23_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV DR0-DR7, r64
    // Opcode           : 0F 23 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r64 to extended debug register.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_0F_23_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV DR0-DR7, r64
    // Opcode           : 0F 23 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r64 to extended debug register.

    // TODO: implement

    puts("not implemented: MOV DR0-DR7, r64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_8C_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m16,Sreg
    // Opcode           : 8C /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move segment register to r/m16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_8C_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV r/m16,Sreg
    // Opcode           : 8C /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (w)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move segment register to r/m16.

    // TODO: implement

    puts("not implemented: MOV r/m16,Sreg");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_8E_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV Sreg,r/m16
    // Opcode           : 8E /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r/m16 to segment register.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_MOV_8E_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : MOV Sreg,r/m16
    // Opcode           : 8E /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:reg (w)
    // Operand 2        : ModRM:r/m (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : Move r/m16 to segment register.

    // TODO: implement

    puts("not implemented: MOV Sreg,r/m16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}
