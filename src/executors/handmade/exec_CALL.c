#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>

// TODO: implement
// flags: All flags are affected if a task switch occurs; no flags are affected if a task switch does not occur.

struct ErrorInfo exec_CALL_REXW__FF_3(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL m16:64
    // Opcode           : REX.W + FF /3
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : In 64-bit mode: If selector points to a gate, then RIP = 64-bit displacement taken from gate; else RIP = 64-bit offset from far pointer referenced in the instruction.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CALL_REXW__FF_3_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL m16:64
    // Opcode           : REX.W + FF /3
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : In 64-bit mode: If selector points to a gate, then RIP = 64-bit displacement taken from gate; else RIP = 64-bit offset from far pointer referenced in the instruction.

    // TODO: implement

    puts("not implemented: CALL m16:64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CALL_E8_cw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL rel16
    // Opcode           : E8 cw
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Call near, relative, displacement relative to next instruction.

    // TAG: 64bit invalid

    struct ErrorInfo error = {ERROR_UD, 0};
    return error;
}

struct ErrorInfo exec_CALL_E8_cw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL rel16
    // Opcode           : E8 cw
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Call near, relative, displacement relative to next instruction.

    struct ErrorInfo error = {ERROR_UD, 0};
    return error;
}

struct ErrorInfo exec_CALL_E8_cd(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL rel32
    // Opcode           : E8 cd
    // Valid in         : Valid, Valid, Invalid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Call near, relative, displacement relative to next instruction. 32-bit displacement sign extended to 64-bits in 64-bit mode.

    struct ErrorInfo error = {NO_ERROR, 0};

    // TAG: implemented here

    uint64_t ip_tmp = machine->ip.r;
    uint64_t rel = (int64_t)(int32_t)ins->code_offset.dword;
    MEMORY_ACCESS_WRITE(uint64_t, machine, machine->sp.r -= 8) = ip_tmp;
    machine->ip.r += rel;

    return error;
}

struct ErrorInfo exec_CALL_E8_cd_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL rel32
    // Opcode           : E8 cd
    // Valid in         : Valid, Valid, Invalid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Call near, relative, displacement relative to next instruction. 32-bit displacement sign extended to 64-bits in 64-bit mode.

    uint64_t rel = (int64_t)(int32_t)ins->code_offset.dword;
    printf("call [rip + %" PRIx64 "]\n", rel);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_CALL_FF_2_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL r/m16
    // Opcode           : FF /2
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Call near, absolute indirect, address given in r/m16.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CALL_FF_2_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL r/m16
    // Opcode           : FF /2
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Call near, absolute indirect, address given in r/m16.

    // TODO: implement

    puts("not implemented: CALL r/m16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CALL_FF_2(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL r/m64
    // Opcode           : FF /2
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Call near, absolute indirect, address given in r/m64.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CALL_FF_2_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL r/m64
    // Opcode           : FF /2
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Call near, absolute indirect, address given in r/m64.

    // TODO: implement

    puts("not implemented: CALL r/m64");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CALL_9A_cd(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL ptr16:16
    // Opcode           : 9A cd
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Call far, absolute, address given in operand.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CALL_9A_cd_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL ptr16:16
    // Opcode           : 9A cd
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Call far, absolute, address given in operand.

    // TODO: implement

    puts("not implemented: CALL ptr16:16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CALL_FF_3_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL m16:16
    // Opcode           : FF /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Call far, absolute indirect address given in m16:16. In 32-bit mode: if selector points to a gate, then RIP = 32-bit zero extended displacement taken from gate; else RIP = zero extended 16-bit offset from far pointer referenced in the instruction.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CALL_FF_3_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL m16:16
    // Opcode           : FF /3
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Call far, absolute indirect address given in m16:16. In 32-bit mode: if selector points to a gate, then RIP = 32-bit zero extended displacement taken from gate; else RIP = zero extended 16-bit offset from far pointer referenced in the instruction.

    // TODO: implement

    puts("not implemented: CALL m16:16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CALL_FF_3(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL m16:32
    // Opcode           : FF /3
    // Valid in         : Valid, Valid, Invalid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : In 64-bit mode: If selector points to a gate, then RIP = 64-bit displacement taken from gate; else RIP = zero extended 32-bit offset from far pointer referenced in the instruction.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_CALL_FF_3_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : CALL m16:32
    // Opcode           : FF /3
    // Valid in         : Valid, Valid, Invalid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : In 64-bit mode: If selector points to a gate, then RIP = 64-bit displacement taken from gate; else RIP = zero extended 32-bit offset from far pointer referenced in the instruction.

    // TODO: implement

    puts("not implemented: CALL m16:32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}