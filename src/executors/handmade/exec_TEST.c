#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>

// flags: The OF and CF flags are cleared;
// the SF, ZF, and PF flags are set according to the result.
// The state of the AF flag is undefined.
#define TEST_SET_FLAGS(rflags, result, size_constant) do { \
    (rflags).overflow = 0;                                \
    (rflags).carry = 0;                                   \
    (rflags).sign = SIGN((result));                       \
    (rflags).zero = ZERO((result));                       \
    (rflags).parity = parity((result));                   \
} while(false)

// r/mN, immN

// TAG: implemented here
#define TEMPLATE_TEST_RMn_IMMn(machine, ins, error, dist_size_constant, src_size_constant) do { \
    struct ModRMPointer dist_info = get_addr_from_modRM_64bit(machine, ins); \
    UINTn_T(dist_size_constant) *dist_addr;                                                   \
    if (dist_info.kind == MODRM_MEMORY) {                                  \
        dist_addr = &MEMORY_ACCESS_WRITE(UINTn_T(dist_size_constant), machine, dist_info.memory_addr); \
    } else if (dist_info.kind == MODRM_REGISTER) {                         \
        dist_addr = &dist_info.register_p->REGISTER_SIZE_KIND(dist_size_constant);                                                   \
    } else {                                                               \
        error.code = ERROR_PANIC;                                              \
        return error;                                                          \
    }                                                                      \
    UINTn_T(dist_size_constant) before = *dist_addr;                                          \
    UINTn_T(dist_size_constant) src = (INTn_T(dist_size_constant))(INTn_T(src_size_constant))ins->immediate.dword;                 \
    UINTn_T(dist_size_constant) result = *dist_addr & src;                                                     \
    TEST_SET_FLAGS(machine->rflags, result, dist_size_constant);           \
} while (false)

#define TEMPLATE_TEST_RMn_IMMn_DISASSEMBLE(machine, ins, dist_size_constant, src_size_constant) do { \
    char dist_buf[64];                                                                                  \
    char *register_str = get_addr_str_from_modRM_64bit(machine, ins, dist_buf);                         \
    if (register_str != NULL) {                                                                         \
        format_register_str(register_str, dist_size_constant, dist_buf);                                                    \
    }                                                                                               \
    UINTn_T(dist_size_constant) src = (INTn_T(dist_size_constant))(INTn_T(src_size_constant))ins->immediate.dword;                 \
    printf("sub %s, 0x%" PRIx ## dist_size_constant "\n", dist_buf, src);                                                   \
} while (false)

struct ErrorInfo exec_TEST_REX__F6_0_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m8, imm8
    // Opcode           : REX + F6 /0 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AND imm8 with r/m8; set SF, ZF, PF according to result.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_TEST_RMn_IMMn(machine, ins, error, 8, 8);
    return error;
}

struct ErrorInfo exec_TEST_REX__F6_0_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m8, imm8
    // Opcode           : REX + F6 /0 ib
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AND imm8 with r/m8; set SF, ZF, PF according to result.

    TEMPLATE_TEST_RMn_IMMn_DISASSEMBLE(machine, ins, 8, 8);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_TEST_REXW__F7_0_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m64, imm32
    // Opcode           : REX.W + F7 /0 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AND imm32 sign-extended to 64-bits with r/m64; set SF, ZF, PF according to result.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_TEST_RMn_IMMn(machine, ins, error, 64, 32);
    return error;
}

struct ErrorInfo exec_TEST_REXW__F7_0_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m64, imm32
    // Opcode           : REX.W + F7 /0 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AND imm32 sign-extended to 64-bits with r/m64; set SF, ZF, PF according to result.

    TEMPLATE_TEST_RMn_IMMn_DISASSEMBLE(machine, ins, 64, 32);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_TEST_F7_0_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m16, imm16
    // Opcode           : F7 /0 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm16 with r/m16; set SF, ZF, PF according to result.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_TEST_RMn_IMMn(machine, ins, error, 16, 16);
    return error;
}

struct ErrorInfo exec_TEST_F7_0_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m16, imm16
    // Opcode           : F7 /0 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm16 with r/m16; set SF, ZF, PF according to result.

    TEMPLATE_TEST_RMn_IMMn_DISASSEMBLE(machine, ins, 16, 16);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_TEST_F7_0_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m32, imm32
    // Opcode           : F7 /0 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm32 with r/m32; set SF, ZF, PF according to result.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_TEST_RMn_IMMn(machine, ins, error, 32, 32);
    return error;
}

struct ErrorInfo exec_TEST_F7_0_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m32, imm32
    // Opcode           : F7 /0 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm32 with r/m32; set SF, ZF, PF according to result.

    TEMPLATE_TEST_RMn_IMMn_DISASSEMBLE(machine, ins, 32, 32);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

// r/m8, imm8 (no rex prefix)

struct ErrorInfo exec_TEST_F6_0_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m8, imm8
    // Opcode           : F6 /0 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm8 with r/m8; set SF, ZF, PF according to result.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_TEST_F6_0_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m8, imm8
    // Opcode           : F6 /0 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm8 with r/m8; set SF, ZF, PF according to result.

    // TODO: implement

    puts("not implemented: TEST r/m8, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

// *a*, immN

struct ErrorInfo exec_TEST_REXW__A9_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST RAX, imm32
    // Opcode           : REX.W + A9 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm32 sign-extended to 64-bits with RAX; set SF, ZF, PF according to result.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_TEST_REXW__A9_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST RAX, imm32
    // Opcode           : REX.W + A9 id
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    :
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm32 sign-extended to 64-bits with RAX; set SF, ZF, PF according to result.

    // TODO: implement

    puts("not implemented: TEST RAX, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_TEST_A8_ib(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST AL, imm8
    // Opcode           : A8 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm8 with AL; set SF, ZF, PF according to result.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_TEST_A8_ib_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST AL, imm8
    // Opcode           : A8 ib
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm8 with AL; set SF, ZF, PF according to result.

    // TODO: implement

    puts("not implemented: TEST AL, imm8");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_TEST_A9_iw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST AX, imm16
    // Opcode           : A9 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm16 with AX; set SF, ZF, PF according to result.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_TEST_A9_iw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST AX, imm16
    // Opcode           : A9 iw
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm16 with AX; set SF, ZF, PF according to result.

    // TODO: implement

    puts("not implemented: TEST AX, imm16");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_TEST_A9_id(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST EAX, imm32
    // Opcode           : A9 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm32 with EAX; set SF, ZF, PF according to result.

    // TODO: implement

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

struct ErrorInfo exec_TEST_A9_id_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST EAX, imm32
    // Opcode           : A9 id
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : AL/AX/EAX/RAX
    // Operand 2        : imm8/16/32
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND imm32 with EAX; set SF, ZF, PF according to result.

    // TODO: implement

    puts("not implemented: TEST EAX, imm32");

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}

// r/mN, rN

// TAG: implemented here
#define TEMPLATE_TEST_RMn_Rn(machine, ins, error, size_constant) do { \
    struct ModRMPointer dist_info =  get_addr_from_modRM_64bit(machine, ins);       \
    UINTn_T(size_constant) *dist_addr;                                                            \
    if (dist_info.kind == MODRM_MEMORY) {                                           \
        dist_addr = &MEMORY_ACCESS_WRITE(UINTn_T(size_constant), machine, dist_info.memory_addr);   \
    } else if (dist_info.kind == MODRM_REGISTER) {                                  \
        dist_addr = &dist_info.register_p->REGISTER_SIZE_KIND(size_constant);                                           \
    } else {                                                                        \
        dist_addr = NULL;                                                               \
        error.code = ERROR_PANIC;                                                       \
        return error;                                                                   \
    }                                                                               \
    union X64Register *src_register = get_register(machine, ins->mod_rm.reg, REX_R(ins)); \
    UINTn_T(size_constant) result = *dist_addr & src_register->REGISTER_SIZE_KIND(size_constant);                                               \
    TEST_SET_FLAGS(machine->rflags, result, size_constant);                                                                      \
} while(false)

#define TEMPLATE_TEST_RMn_Rn_DISASSEMBLE(machine, ins, reg_size) do { \
    char dist_buf[64];                           \
    char *register_str =get_addr_str_from_modRM_64bit(machine, ins, dist_buf); \
    if (register_str != NULL) {                  \
        format_register_str(register_str, reg_size, dist_buf);       \
    }                                            \
    char src_buf[64];                            \
    format_register_str(get_register_str(machine, ins->mod_rm.reg, REX_R(ins)), reg_size, src_buf); \
    printf("test %s, %s\n", dist_buf, src_buf);   \
} while(false)

struct ErrorInfo exec_TEST_REX__84_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m8, r8
    // Opcode           : REX + 84 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AND r8 with r/m8; set SF, ZF, PF according to result.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_TEST_RMn_Rn(machine, ins, error, 8);
    return error;
}

struct ErrorInfo exec_TEST_REX__84_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m8, r8
    // Opcode           : REX + 84 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AND r8 with r/m8; set SF, ZF, PF according to result.

    TEMPLATE_TEST_RMn_Rn_DISASSEMBLE(machine, ins, 8);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_TEST_REXW__85_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m64, r64
    // Opcode           : REX.W + 85 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AND r64 with r/m64; set SF, ZF, PF according to result.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_TEST_RMn_Rn(machine, ins, error, 64);
    return error;
}

struct ErrorInfo exec_TEST_REXW__85_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m64, r64
    // Opcode           : REX.W + 85 /r
    // Valid in         : Valid, Invalid, Invalid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AND r64 with r/m64; set SF, ZF, PF according to result.

    TEMPLATE_TEST_RMn_Rn_DISASSEMBLE(machine, ins, 64);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_TEST_85_r_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m16, r16
    // Opcode           : 85 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AND r16 with r/m16; set SF, ZF, PF according to result.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_TEST_RMn_Rn(machine, ins, error, 16);
    return error;
}

struct ErrorInfo exec_TEST_85_r_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m16, r16
    // Opcode           : 85 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AND r16 with r/m16; set SF, ZF, PF according to result.

    TEMPLATE_TEST_RMn_Rn_DISASSEMBLE(machine, ins, 16);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_TEST_85_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m32, r32
    // Opcode           : 85 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AND r32 with r/m32; set SF, ZF, PF according to result.

    struct ErrorInfo error = {NO_ERROR, 0};
    TEMPLATE_TEST_RMn_Rn(machine, ins, error, 32);
    return error;
}

struct ErrorInfo exec_TEST_85_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m32, r32
    // Opcode           : 85 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       : 
    // Description      : AND r32 with r/m32; set SF, ZF, PF according to result.

    TEMPLATE_TEST_RMn_Rn_DISASSEMBLE(machine, ins, 32);

    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

// r/m8, r8 (no rex prefix)

struct ErrorInfo exec_TEST_84_r(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m8, r8
    // Opcode           : 84 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND r8 with r/m8; set SF, ZF, PF according to result.

    struct ErrorInfo error = {NO_ERROR, 0};

    struct ModRMPointer dist_info = get_addr_from_modRM_64bit_byte(machine, ins);
    NORMALIZE_MODRM_POINTER_BYTE_READ(machine, dist_info, dist_addr, error)
    struct RegisterWithBytePosition src_register = get_byte_register(machine, ins->mod_rm.reg);
    NORMALIZE_BYTE_REGISTER_WITH_POSITION_WITH_DECL(machine, src_addr, src_register);
    uint8_t result = *dist_addr & *src_addr;
    TEST_SET_FLAGS(machine->rflags, result, 8);

    return error;
}

struct ErrorInfo exec_TEST_84_r_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : TEST r/m8, r8
    // Opcode           : 84 /r
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    :
    // Operand 1        : ModRM:r/m (r)
    // Operand 2        : ModRM:reg (r)
    // Operand 3        : NA
    // Operand 4        : NA
    // Tuple Type       :
    // Description      : AND r8 with r/m8; set SF, ZF, PF according to result.

    char dist_buf[64];
    get_addr_str_from_modRM_64bit_byte(machine, ins, dist_buf);
    char *src_register = get_byte_register_str(machine, ins->mod_rm.reg);
    printf("test %s, %s", dist_buf, src_register);

    struct ErrorInfo error = {ERROR_NOT_IMPLEMENTED, 0};
    return error;
}
