#include "../../x64machine.h"
#include "../../x64instruction.h"
#include "../x64executortool.h"
#include "../x64disassembletool.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>

// TAG: implemented here
#define TEMPLATE_JA_REL(machine, ins, error, size_constant) do { \
    if (!machine->rflags.carry && !machine->rflags.zero) {                                  \
        uint64_t rel = (int64_t)(INTn_T(size_constant))ins->code_offset.IMM_SIZE_KIND(size_constant); \
        machine->ip.r += rel;                                    \
    }                                                            \
} while (false)

#define TEMPLATE_JA_REL_DISASSEMBLE(machine, ins, size_constant) do { \
    INTn_T(size_constant) rel = ins->code_offset.IMM_SIZE_KIND(size_constant);              \
    printf("je [rip + 0x%" PRIx ## size_constant "]  (%" PRIx64 ")\n", rel, machine->ip.r + rel);                              \
} while (false)

// cat: not categorized

struct ErrorInfo exec_JA_0F_87_cw_P66(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : JA rel16
    // Opcode           : 0F 87 cw
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Jump near if above (CF=0 and ZF=0). Not supported in 64-bit mode.

    struct ErrorInfo error = {ERROR_UD, 0};
    return error;
}

struct ErrorInfo exec_JA_0F_87_cw_P66_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : JA rel16
    // Opcode           : 0F 87 cw
    // Valid in         : Invalid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Jump near if above (CF=0 and ZF=0). Not supported in 64-bit mode.

    TEMPLATE_JA_REL_DISASSEMBLE(machine, ins, 16);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_JA_0F_87_cd(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : JA rel32
    // Opcode           : 0F 87 cd
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Jump near if above (CF=0 and ZF=0).

    TEMPLATE_JA_REL(machine, ins, error, 32);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_JA_0F_87_cd_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : JA rel32
    // Opcode           : 0F 87 cd
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Jump near if above (CF=0 and ZF=0).

    TEMPLATE_JA_REL_DISASSEMBLE(machine, ins, 32);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_JA_77_cb(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : JA rel8
    // Opcode           : 77 cb
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Jump short if above (CF=0 and ZF=0).

    TEMPLATE_JA_REL(machine, ins, error, 8);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}

struct ErrorInfo exec_JA_77_cb_disassemble(struct X64Machine *machine, struct Instruction *ins) {
    // Instruction      : JA rel8
    // Opcode           : 77 cb
    // Valid in         : Valid, Valid, Valid
    // Feature Flags    : 
    // Operand 1        : 
    // Operand 2        : 
    // Operand 3        : 
    // Operand 4        : 
    // Tuple Type       : 
    // Description      : Jump short if above (CF=0 and ZF=0).

    TEMPLATE_JA_REL_DISASSEMBLE(machine, ins, 8);
    struct ErrorInfo error = {NO_ERROR, 0};
    return error;
}