//
// Created by tia on 2021/03/26.
//

#ifndef X64WEMU_SRC_EXECUTORS_X64DISASSEMBLETOOL_H
#define X64WEMU_SRC_EXECUTORS_X64DISASSEMBLETOOL_H

#include "../x64machine.h"
#include "../x64instruction.h"

char *get_byte_register_str(struct X64Machine *machine, uint8_t reg);
char *get_register_str(struct X64Machine *machine, uint8_t reg, _Bool rex_b);
char *get_addr_str_from_modRM_64bit(struct X64Machine *machine, struct Instruction *ins, char *buf);
void format_register_str(char *kind, uint8_t size, char buf[5]);
void get_addr_str_from_modRM_64bit_byte(struct X64Machine *machine, struct Instruction *ins, char *buf);

#endif //X64WEMU_SRC_EXECUTORS_X64DISASSEMBLETOOL_H
