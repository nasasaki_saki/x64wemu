//
// Created by tia on 2021/03/27.
//

#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include "x64executortool.h"
#include "x64disassembletool.h"
#include "../tools.h"

/**
 * get register.8 string from register indicator (no rex prefix)
 * @param machine
 * @param reg
 * @return char *
 */
char *get_byte_register_str(struct X64Machine *machine, uint8_t reg) {
    switch (reg) {
    case 0:
        return "al";
    case 1:
        return "cl";
    case 2:
        return "dl";
    case 3:
        return "bl";
    case 4:
        return "ah";
    case 5:
        return "ch";
    case 6:
        return "dh";
    case 7:
        return "bh";
    default:
        return NULL;  // not reachable
    }
}

/**
 * get register string from register indicator.
 * @param machine
 * @param reg register indicator (3 bit)
 * @param rex_b        REX.B bit
 * @return char *
 */
char *get_register_str(struct X64Machine *machine, uint8_t reg, _Bool rex_b) {
    switch (reg) {
    case 0:
        if (rex_b) return "r8";
        return "a";
    case 1:
        if (rex_b) return "r9";
        return "c";
    case 2:
        if (rex_b) return "r10";
        return "d";
    case 3:
        if (rex_b) return "r11";
        return "b";
    case 4:
        if (rex_b) return "r12";
        return "sp";
    case 5:
        if (rex_b) return "r13";
        return "bp";
    case 6:
        if (rex_b) return "r14";
        return "si";
    case 7:
        if (rex_b) return "r15";
        return "di";
    default:
        return NULL;
    }
}

void format_register_str(char *kind, uint8_t size, char buf[5]) {
    switch (size) {
    case 8:
        if (kind[0] == 'r')
            sprintf(buf, "%sb", kind);
        else
            sprintf(buf, "%sl", kind);
        break;
    case 16:
        if (kind[0] == 'r')
            sprintf(buf, "%sw", kind);
        else if (kind[1] == 'i' || kind[1] == 'p' || (kind[0] == 'i' && kind[1] == 'z'))
            sprintf(buf, "%s", kind);
        else
            sprintf(buf, "%sx", kind);
        break;
    case 32:
        if (kind[0] == 'r')
            sprintf(buf, "%sd", kind);
        else if (kind[1] == 'i' || kind[1] == 'p' || (kind[0] == 'i' && kind[1] == 'z'))
            sprintf(buf, "e%s", kind);
        else
            sprintf(buf, "e%sx", kind);
        break;
    case 64:
        if (kind[0] == 'r')
            sprintf(buf, "%s", kind);
        else if (kind[1] == 'i' || kind[1] == 'p' || (kind[0] == 'i' && kind[1] == 'z'))
            sprintf(buf, "r%s", kind);
        else
            sprintf(buf, "r%sx", kind);
        break;
    default:
        ;
    }
}

void get_memory_addr_str_from_sib_64bit(struct X64Machine *machine, struct Instruction *ins, int32_t displacement, char *buf) {
    struct OpeSib sib = ins->sib;

    char base_buf[32];
    char index_buf[32];

    if (ins->mod_rm.mod == 0u && sib.base == 5u) {
        displacement = ins->displacement.dword;
        sprintf(base_buf, "");
    } else {
        int size;
        if (ADDRESS_OVERRIDE(ins)) {
            size = 32;
        } else {
            size = 64;
        }
        format_register_str(get_register_str(machine, ins->sib.base, REX_B(ins)), size, base_buf);
    }

    int size;
    if (ADDRESS_OVERRIDE(ins)) {
        size = 32;
    } else {
        size = 64;
    }

    char *r;
    if (ins->sib.index == 4u) {
        r = "iz";
    } else {
        r = get_register_str(machine, ins->sib.index, REX_X(ins));
    }

    format_register_str(r, size, index_buf);

    char *scale = "err";

    if (ins->sib.scale == 0u) {
        scale = "1";
    } else if (ins->sib.scale == 1u) {
        scale = "2";
    } else if (ins->sib.scale == 2u) {
        scale = "4";
    } else if (ins->sib.scale == 3u) {
        scale = "8";
    }

    sprintf(buf, "[%s + %s * %s + 0x%" PRIx32 "]" , base_buf, index_buf, scale, displacement);
}

char *get_addr_str_from_modRM_64bit(struct X64Machine *machine, struct Instruction *ins, char *buf) {
    struct OpeModRM mod_rm = ins->mod_rm;

    int32_t displacement = 0;
    if (mod_rm.mod == 0u && mod_rm.rm == 5u) {
        displacement = (int32_t)ins->displacement.dword;
        sprintf(buf, "[rip + %" PRIx32 "]", displacement);
        return NULL;
    } else if (mod_rm.mod == 1u) {
        displacement = (int32_t)(int8_t)ins->displacement.byte;
    } else if (mod_rm.mod == 2u) {
        displacement = (int32_t)ins->displacement.dword;
    }

    if (mod_rm.rm == 4u && mod_rm.mod != 3u) {
        get_memory_addr_str_from_sib_64bit(machine, ins, displacement, buf);
        return NULL;
    }

    char *r = get_register_str(machine, mod_rm.rm, REX_B(ins));
    if (mod_rm.mod == 3u) {  // register
        return r;
    }


    int size;
    if (ADDRESS_OVERRIDE(ins)) {
        size = 32;
    } else {
        size = 64;
    }
    char reg_buf[32];
    format_register_str(r, size, reg_buf);

    sprintf(buf, "[%s + 0x%" PRIx32 "]", reg_buf, displacement);
    return NULL;
}

void get_addr_str_from_modRM_64bit_byte(struct X64Machine *machine, struct Instruction *ins, char *buf) {
    struct OpeModRM mod_rm = ins->mod_rm;

    int32_t displacement = 0;
    if (mod_rm.mod == 0u && mod_rm.rm == 5u) {
        displacement = (int32_t)ins->displacement.dword;
        sprintf(buf, "[%" PRIx32 "]", displacement);
        return;
    } else if (mod_rm.mod == 1u) {
        displacement = (int32_t)(int8_t)ins->displacement.byte;
    } else if (mod_rm.mod == 2u) {
        displacement = (int32_t)ins->displacement.dword;
    }

    if (mod_rm.rm == 4u && mod_rm.mod != 3u) {
        get_memory_addr_str_from_sib_64bit(machine, ins, displacement, buf);
        return;
    }

    if (mod_rm.mod == 3u) {  // register
        strcpy(buf, get_byte_register_str(machine, mod_rm.rm));
        return;
    }

    char *r = get_register_str(machine, mod_rm.rm, REX_B(ins));

    int size;
    if (ADDRESS_OVERRIDE(ins)) {
        size = 32;
    } else {
        size = 64;
    }
    char reg_buf[32];
    format_register_str(r, size, reg_buf);

    sprintf(buf, "[%s + 0x%" PRIx32 "]", reg_buf, displacement);
}
