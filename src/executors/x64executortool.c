//
// Created by tia on 2021/03/27.
//

#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>
#include "x64executortool.h"
#include "../tools.h"


struct RegisterWithBytePosition get_byte_register(struct X64Machine *machine, uint8_t reg) {
    struct RegisterWithBytePosition result;
    switch (reg) {
    case 0:
        result.register_p = &machine->a;
        result.pos = LOW;
        break;
    case 1:
        result.register_p = &machine->c;
        result.pos = LOW;
        break;
    case 2:
        result.register_p = &machine->d;
        result.pos = LOW;
        break;
    case 3:
        result.register_p = &machine->b;
        result.pos = LOW;
        break;
    case 4:
        result.register_p = &machine->a;
        result.pos = HIGH;
        break;
    case 5:
        result.register_p = &machine->c;
        result.pos = HIGH;
        break;
    case 6:
        result.register_p = &machine->d;
        result.pos = HIGH;
        break;
    case 7:
        result.register_p = &machine->b;
        result.pos = HIGH;
        break;
    default:
        result.register_p = NULL;  // not reachable
    }
    return result;
}

/**
 * get register from register indicator.
 * @param machine
 * @param reg register indicator (3 bit)
 * @param rex_b        REX.B bit
 * @return X64Register*
 */
union X64Register *get_register(struct X64Machine *machine, uint8_t reg, _Bool rex_b) {
    switch (reg) {
    case 0:
        if (rex_b) return &machine->r8;
        return &machine->a;
    case 1:
        if (rex_b) return &machine->r9;
        return &machine->c;
    case 2:
        if (rex_b) return &machine->r10;
        return &machine->d;
    case 3:
        if (rex_b) return &machine->r11;
        return &machine->b;
    case 4:
        if (rex_b) return &machine->r12;
        return &machine->sp;
    case 5:
        if (rex_b) return &machine->r13;
        return &machine->bp;
    case 6:
        if (rex_b) return &machine->r14;
        return &machine->si;
    case 7:
        if (rex_b) return &machine->r15;
        return &machine->di;
    default:
        return NULL;
    }
}

uint64_t get_memory_addr_from_sib_64bit(struct X64Machine *machine, struct Instruction *ins, int32_t displacement) {
    struct OpeSib sib = ins->sib;

    int64_t base = 0;

    if (ins->mod_rm.mod == 0u && sib.base == 5u) {
        displacement = ins->displacement.dword;
    } else {
        union X64Register *r = get_register(machine, ins->sib.base, REX_B(ins));
        if (ADDRESS_OVERRIDE(ins)) {
            base = (int32_t)r->e;
        } else {
            base = r->r;
        }
    }

    const union X64Register *i;
    if (ins->sib.index == 4u) {
        i = &riz_r;
    } else {
        i = get_register(machine, ins->sib.index, REX_X(ins));
    }
    int64_t index;
    if (ADDRESS_OVERRIDE(ins)) {
        index = (int32_t)i->e;
    } else {
        index = i->r;
    }

    uint8_t scale = 0;

    if (ins->sib.scale == 0u) {
        scale = 1;
    } else if (ins->sib.scale == 1u) {
        scale = 2;
    } else if (ins->sib.scale == 2u) {
        scale = 4;
    } else if (ins->sib.scale == 3u) {
        scale = 8;
    }

    return base + index * scale + displacement;
}

struct ModRMPointer get_addr_from_modRM_64bit(struct X64Machine *machine, struct Instruction *ins) {
    struct OpeModRM mod_rm = ins->mod_rm;
    struct ModRMPointer result;

    int32_t displacement = 0;
    if (mod_rm.mod == 0u && mod_rm.rm == 5u) {
        displacement = (int32_t)ins->displacement.dword;
        result.kind = MODRM_MEMORY;
        result.memory_addr = displacement + machine->ip.r;
        return result;
    } else if (mod_rm.mod == 1u) {
        displacement = (int32_t)(int8_t)ins->displacement.byte;
    } else if (mod_rm.mod == 2u) {
        displacement = (int32_t)ins->displacement.dword;
    }

    if (mod_rm.rm == 4u && mod_rm.mod != 3u) {
        uint64_t addr = get_memory_addr_from_sib_64bit(machine, ins, displacement);
        result.kind = MODRM_MEMORY;
        result.memory_addr = addr;
        return result;
    }

    union X64Register *r = get_register(machine, mod_rm.rm, REX_B(ins));
    if (mod_rm.mod == 3u) {  // register
        result.kind = MODRM_REGISTER;
        result.register_p = r;
        return result;
    }

    uint64_t addr;
    if (ADDRESS_OVERRIDE(ins)) {
        addr = (int64_t)((int32_t)r->e + displacement);
    } else {
        addr = ((int64_t)r->r + displacement);
    }

    result.kind = MODRM_MEMORY;
    result.memory_addr = addr;
    return result;
}

struct ModRMPointer get_addr_from_modRM_64bit_byte(struct X64Machine *machine, struct Instruction *ins) {
    struct OpeModRM mod_rm = ins->mod_rm;
    struct ModRMPointer result;

    int32_t displacement = 0;
    if (mod_rm.mod == 0u && mod_rm.rm == 5u) {
        displacement = (int32_t)ins->displacement.dword;
        result.kind = MODRM_MEMORY;
        result.memory_addr = displacement + machine->ip.r;
        return result;
    } else if (mod_rm.mod == 1u) {
        displacement = (int32_t)(int8_t)ins->displacement.byte;
    } else if (mod_rm.mod == 2u) {
        displacement = (int32_t)ins->displacement.dword;
    }

    if (mod_rm.rm == 4u && mod_rm.mod != 3u) {
        uint64_t addr = get_memory_addr_from_sib_64bit(machine, ins, displacement);
        result.kind = MODRM_MEMORY;
        result.memory_addr = addr;
        return result;
    }

    if (mod_rm.mod == 3u) {  // register
        struct RegisterWithBytePosition br = get_byte_register(machine, mod_rm.rm);
        result.kind = MODRM_BYTE_REGISTER;
        result.byte_register = br;
        return result;
    }

    union X64Register *r = get_register(machine, mod_rm.rm, REX_B(ins));

    uint64_t addr;
    if (ADDRESS_OVERRIDE(ins)) {
        addr = (int64_t)((int32_t)r->e + displacement);
    } else {
        addr = ((int64_t)r->r + displacement);
    }

    result.kind = MODRM_MEMORY;
    result.memory_addr = addr;
    return result;
}

_Bool parity(uint64_t x) {
    x ^= x >> 32;
    x ^= x >> 16;
    x ^= x >> 8;
    x ^= x >> 4;
    x ^= x >> 2;
    x ^= x >> 1;
    return (~x) & 1;
}
