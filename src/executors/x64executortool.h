//
// Created by tia on 2021/03/26.
//

#ifndef X64WEMU_SRC_EXECUTORS_X64EXECUTORTOOL_H
#define X64WEMU_SRC_EXECUTORS_X64EXECUTORTOOL_H

#include "../x64machine.h"
#include "../x64instruction.h"
#include <stdint.h>

/* extension of the ModR/M r/m, SIB base, Opcode reg field */
#define REX_B(op) (op->has_rex_prefix && op->rex_prefix.b)
/* extension of the SIB index field */
#define REX_X(op) (op->has_rex_prefix && op->rex_prefix.x)
/* extension of the ModR/M reg field */
#define REX_R(op) (op->has_rex_prefix && op->rex_prefix.r)
/* 0 = Operand size determined by CS.D
 * 1 = 64 Bit Operand Size */
#define REX_W(op) (op->has_rex_prefix && op->rex_prefix.w)

#define OPCODE_REG(opcode) ((opcode) & 0x07)

#define OPERAND_OVERRIDE(op) (op->has_prefix && op->prefix.grp3 == 0x66)
#define ADDRESS_OVERRIDE(op) (op->has_prefix && op->prefix.grp4 == 0x67)

#define UINTn_T(size_constant) uint ## size_constant ## _t
#define INTn_T(size_constant) int ## size_constant ## _t

#define REGISTER_SIZE_KIND(size_constant) REGISTER_SIZE_KIND_ ## size_constant
#define REGISTER_SIZE_KIND_8 l
#define REGISTER_SIZE_KIND_8H l
#define REGISTER_SIZE_KIND_16 x
#define REGISTER_SIZE_KIND_32 e
#define REGISTER_SIZE_KIND_64 r

#define IMM_SIZE_KIND(size_constant) IMM_SIZE_KIND_ ## size_constant
#define IMM_SIZE_KIND_8 byte
#define IMM_SIZE_KIND_16 word
#define IMM_SIZE_KIND_32 dword
#define IMM_SIZE_KIND_64 qword

#define BIT_TO_BYTE(bits_constant) BIT_TO_BYTE_ ## bits_constant
#define BIT_TO_BYTE_8 1
#define BIT_TO_BYTE_16 2
#define BIT_TO_BYTE_32 4
#define BIT_TO_BYTE_64 8
#define BIT_TO_BYTE_128 16

#define BIT_TO_BYTE_STR(bits_constant) BIT_TO_BYTE_STR_ ## bits_constant
#define BIT_TO_BYTE_STR_8 "1"
#define BIT_TO_BYTE_STR_16 "2"
#define BIT_TO_BYTE_STR_32 "4"
#define BIT_TO_BYTE_STR_64 "8"
#define BIT_TO_BYTE_STR_128 "16"

#define ELSE_ERROR_RET(error, kind) else { error.code = kind; return error; }

#define NORMALIZE_BYTE_REGISTER_WITH_POSITION(machine, dist, reg) \
    if (reg.pos == LOW) {                                         \
        dist = &reg.register_p->l;                                 \
    } else if (reg.pos == HIGH) {                                 \
        dist = &reg.register_p->h;                                 \
    } ELSE_ERROR_RET(error, ERROR_PANIC)

#define NORMALIZE_BYTE_REGISTER_WITH_POSITION_WITH_DECL(machine, dist, reg) \
    uint8_t *dist;                                                          \
    if (reg.pos == LOW) {                                         \
        dist = &reg.register_p->l;                                 \
    } else if (reg.pos == HIGH) {                                 \
        dist = &reg.register_p->h;                                 \
    } ELSE_ERROR_RET(error, ERROR_PANIC)

#define NORMALIZE_MODRM_POINTER_READ(machine, info, dist, error, size_constant)              \
    UINTn_T(size_constant) *dist;                                                   \
    if (info.kind == MODRM_MEMORY) {                                                \
        dist = &MEMORY_ACCESS(UINTn_T(size_constant), machine, info.memory_addr);   \
    } else if (info.kind == MODRM_REGISTER) {                                       \
        dist = &info.register_p->REGISTER_SIZE_KIND(size_constant);                 \
    } else {                                                                        \
        error.code = ERROR_PANIC;                                                   \
        return error;                                                               \
    }
#define NORMALIZE_MODRM_POINTER_WRITE(machine, info, dist, error, size_constant)                 \
    UINTn_T(size_constant) *dist;                                                       \
    if (info.kind == MODRM_MEMORY) {                                                    \
        dist = &MEMORY_ACCESS_WRITE(UINTn_T(size_constant), machine, info.memory_addr); \
    } else if (info.kind == MODRM_REGISTER) {                                           \
        dist = &info.register_p->REGISTER_SIZE_KIND(size_constant);                     \
    } else {                                                                            \
        error.code = ERROR_PANIC;                                                       \
        return error;                                                                   \
    }
#define NORMALIZE_MODRM_POINTER_BYTE_READ(machine, info, dist, error)             \
    uint8_t *dist;                                                                \
    if (info.kind == MODRM_MEMORY) {                                              \
        dist = &MEMORY_ACCESS(uint8_t, machine, dist_info.memory_addr);           \
    } else if (info.kind == MODRM_BYTE_REGISTER) {                                \
        NORMALIZE_BYTE_REGISTER_WITH_POSITION(machine, dist, info.byte_register)  \
    } ELSE_ERROR_RET(error, ERROR_PANIC)
#define NORMALIZE_MODRM_POINTER_BYTE_WRITE(machine, info, dist, error) \
    uint8_t *dist;                                                         \
    if (info.kind == MODRM_MEMORY) {                                           \
        dist = &MEMORY_ACCESS_WRITE(uint8_t, machine, dist_info.memory_addr);      \
    } else if (info.kind == MODRM_BYTE_REGISTER) {                             \
        NORMALIZE_BYTE_REGISTER_WITH_POSITION(machine, dist, info.byte_register)  \
    } ELSE_ERROR_RET(error, ERROR_PANIC)

enum ModRMPointKind {
    MODRM_MEMORY,
    MODRM_REGISTER,
    MODRM_BYTE_REGISTER,
};

enum LowOrHigh {
    LOW,
    HIGH,
};

struct RegisterWithBytePosition {
    enum LowOrHigh pos;
    union X64Register *register_p;
};

struct ModRMPointer {
    enum ModRMPointKind kind;
    union {
        uint64_t memory_addr;
        union X64Register *register_p;
        struct RegisterWithBytePosition byte_register;
    };
};

struct RegisterWithBytePosition get_byte_register(struct X64Machine *machine, uint8_t reg);
union X64Register *get_register(struct X64Machine *machine, uint8_t reg, _Bool rex_b);
struct ModRMPointer get_addr_from_modRM_64bit(struct X64Machine *machine, struct Instruction *ins);
struct ModRMPointer get_addr_from_modRM_64bit_byte(struct X64Machine *machine, struct Instruction *ins);
_Bool parity(uint64_t x);
#define ZERO(x) ((!(x)) ? 1 : 0)
#define SIGN(x) (_Generic((x),                  \
    uint64_t: ((uint64_t)(x)) >> (64 - 1),      \
    uint32_t: (x) >> (32 - 1),                  \
    uint16_t: (x) >> (16 - 1),                  \
    uint8_t: (x) >> (8 - 1),                    \
    default: 0                                  \
))

#endif //X64WEMU_SRC_EXECUTORS_X64EXECUTORTOOL_H
