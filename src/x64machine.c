//
// Created by tia on 2021/03/19.
//

#include "x64machine.h"
#include "tools.h"
#include "elfdecoder.h"
#include "x64step.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#define DUMP(x) do {printf(#x " : %16llx\n", (uint64_t)(x));} while(0)

const union X64Register riz_r = {0};

void set_stack(struct X64Machine *machine, struct MemoryInfo *memory_info) {
    machine->sp.r = memory_info->size - 8;
    MEMORY_ACCESS_WRITE(uint64_t, machine, machine->sp.r) = 0;
    strcpy(&MEMORY_ACCESS_WRITE(char, machine, machine->sp.r -= 10), "ENV3=env3");
    uint64_t env3_p = machine->sp.r;
    strcpy(&MEMORY_ACCESS_WRITE(char, machine, machine->sp.r -= 10), "ENV2=env2");
    uint64_t env2_p = machine->sp.r;
    strcpy(&MEMORY_ACCESS_WRITE(char, machine, machine->sp.r -= 10), "ENV1=env1");
    uint64_t env1_p = machine->sp.r;
    strcpy(&MEMORY_ACCESS_WRITE(char, machine, machine->sp.r -= 11), "/argument1");
    uint64_t arg1_p = machine->sp.r;
    machine->sp.r &= 0xfffffffffffffff8u;

    MEMORY_ACCESS_WRITE(uint64_t, machine, machine->sp.r -= 8) = 0;
    MEMORY_ACCESS_WRITE(uint64_t, machine, machine->sp.r -= 8) = env3_p;
    MEMORY_ACCESS_WRITE(uint64_t, machine, machine->sp.r -= 8) = env2_p;
    MEMORY_ACCESS_WRITE(uint64_t, machine, machine->sp.r -= 8) = env1_p;
    MEMORY_ACCESS_WRITE(uint64_t, machine, machine->sp.r -= 8) = 0;
    MEMORY_ACCESS_WRITE(uint64_t, machine, machine->sp.r -= 8) = arg1_p;
    MEMORY_ACCESS_WRITE(uint64_t, machine, machine->sp.r -= 8) = 1;
}

void initialize_rflags(struct X64Machine *machine) {
    machine->rflags._1_reserved_0 = 1;
    machine->rflags._0_reserved_1 = 0;
    machine->rflags._0_reserved_2 = 0;
    machine->rflags._0_reserved_3 = 0;
    machine->rflags._0_reserved_4 = 0;
}

void run(uint8_t *memory, uint64_t rip, struct MemoryInfo *memory_info) {
    struct X64Machine machine = {0};
    machine.memory = memory;
    set_stack(&machine, memory_info);
    initialize_rflags(&machine);
    machine.ip.r = rip;
    struct ErrorInfo result;

    while (true) {
        result = step_(&machine, false, false, false);
        if (result.code != NO_ERROR) {
            puts("error exited");
            printf("at %" PRIx64 "\n", machine.ip.r);
            break;
        }
        if (machine.exited) {
            puts("exited");
            break;
        }
    }
}

void run_disassemble(uint8_t *memory, uint64_t rip, uint64_t size, struct MemoryInfo *memory_info, _Bool stop_on_error) {
    struct X64Machine machine = {0};
    machine.memory = memory;
    set_stack(&machine, memory_info);
    machine.ip.r = rip;
    struct ErrorInfo result;

    while (true) {
        printf("%" PRIx64 " : ", machine.ip.r);
        result = step_(&machine, true, true, false);
        if (stop_on_error && result.code != NO_ERROR) {
            printf("error exited, %d, ip=0x%016" PRIx64 "\n", result.code, machine.ip.r);
            break;
        }
        if (machine.ip.r >= rip + size) {
            break;
        }
    }
}

void run_both(uint8_t *memory, uint64_t rip, struct MemoryInfo *memory_info) {
    struct X64Machine machine = {0};
    machine.memory = memory;
    set_stack(&machine, memory_info);
    machine.ip.r = rip;
    struct ErrorInfo result;

    while (true) {
        printf("%" PRIx64 " : ", machine.ip.r);
        result = step_(&machine, true, false, false);
        if (result.code != NO_ERROR) {
            printf("error exited, %d, ip=0x%016" PRIx64 "\n", result.code, machine.ip.r);
            break;
        } else if (machine.exited) {
            puts("exited");
            break;
        }
    }
}

void run_fetch(uint8_t *memory, uint64_t rip, uint64_t size, struct MemoryInfo *memory_info) {
    struct X64Machine machine = {0};
    machine.memory = memory;
    set_stack(&machine, memory_info);
    machine.ip.r = rip;
    struct ErrorInfo result;

    while (true) {
        result = step_(&machine, false, true, true);
        if (result.code != NO_ERROR) {
            printf("error exited, %d, ip=0x%016" PRIx64 "\n", result.code, machine.ip.r);
            break;
        }
        if (machine.ip.r >= rip + size) {
            break;
        }
    }
}
