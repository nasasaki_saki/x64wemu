//
// Created by tia on 2021/03/18.
//

#ifndef X64EMU_SRC_ELFDECODER_H
#define X64EMU_SRC_ELFDECODER_H

#include <stdint.h>
#include "x64machine.h"

uint64_t elf_get_entry_point(const uint8_t *filearray);
uint64_t elf_get_text_section_size(const uint8_t *filearray);
_Bool elf_memory_load(const uint8_t *filearray, uint8_t *memory, struct MemoryInfo *memory_info);

#endif //X64EMU_SRC_ELFDECODER_H
