//
// Created by tia on 2021/03/18.
//

#include "elfdecoder.h"
#include "x64machine.h"

#include <elf.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define DUMP(x)                                                                \
    do {                                                                       \
        printf("%20s: 0x%8llx (%10lld)\n", #x, (uint64_t)(x), (uint64_t)(x));  \
    } while (0)

#define MAX(x, y) ((x > y) ? (x) : (y))
#define MIN(x, y) ((x < y) ? (x) : (y))

uint64_t align(const uint64_t value, const uint64_t a) {
    volatile uint64_t value_v = value;
    value_v += (a - 1);
    value_v /= a;
    value_v *= a;
    return value_v;
}

void elf_read_magic_number(const uint8_t *filearray) {
    Elf64_Ehdr *ehdr = (Elf64_Ehdr *)filearray;
    char *ident = (char *)ehdr->e_ident;
    printf("magic: ");
    for (int i = 0; i < EI_NIDENT; i++) {
        printf("%02x ", (int)(ident[i]));
    }
    printf("\n");

    DUMP(ehdr->e_type);
    DUMP(ehdr->e_machine);
    DUMP(ehdr->e_version);
    DUMP(ehdr->e_entry);
    DUMP(ehdr->e_phoff);
    DUMP(ehdr->e_shoff);
    DUMP(ehdr->e_flags);
    DUMP(ehdr->e_ehsize);
    DUMP(ehdr->e_phentsize);
    DUMP(ehdr->e_phnum);
    DUMP(ehdr->e_shentsize);
    DUMP(ehdr->e_shnum);
    DUMP(ehdr->e_shstrndx);
}

uint64_t elf_get_exec_env_size(const uint8_t *filearray) {
    Elf64_Ehdr *ehdr = (Elf64_Ehdr *)filearray;
    Elf64_Phdr *phdrtbl = (Elf64_Phdr *)((uint64_t)filearray + ehdr->e_phoff);

    uint64_t env_size = 0;

    for (int i = 0; i < ehdr->e_phnum; i++) {
        Elf64_Phdr phdr = phdrtbl[i];
        if (phdr.p_type == PT_LOAD) {
            uint64_t last_offset =
                align(phdr.p_vaddr + phdr.p_memsz, phdr.p_align);
            env_size = MAX(env_size, last_offset);
        }
    }

    return env_size;
}

uint64_t elf_get_entry_point(const uint8_t *filearray) {
    return ((Elf64_Ehdr *)filearray)->e_entry;
}

uint64_t elf_get_text_section_size(const uint8_t *filearray) {
    Elf64_Ehdr *ehdr = (Elf64_Ehdr *)filearray;
    Elf64_Shdr *shdrtbl = (Elf64_Shdr *)((uint64_t)filearray + ehdr->e_shoff);
    char *shstrtbl = (char *)((uint64_t)filearray + shdrtbl[ehdr->e_shstrndx].sh_offset);

    for (int i = 0; i < ehdr->e_shnum; i++) {
        Elf64_Shdr shdr = shdrtbl[i];
        if (strcmp(shstrtbl + shdr.sh_name, ".text") == 0) {
            return shdr.sh_size;
        }
    }

    return 0;
}

_Bool elf_set_memory_info_or_false(struct MemoryInfo *memory_info, uint64_t start, uint64_t size) {
    struct MemoryMapEntry *map_entry = &memory_info->memory_map[memory_info->map_len ++];
    map_entry->size = size;
    map_entry->start = start;
    map_entry->type = MEMORY_ELF_LOAD;
    return true;  // TODO: impl error case
}

_Bool elf_memory_load(const uint8_t *filearray, uint8_t *memory, struct MemoryInfo *memory_info) {
    if (memory == NULL)
        return NULL;
    Elf64_Ehdr *ehdr = (Elf64_Ehdr *)filearray;
//    Elf64_Shdr *shdrtbl = (Elf64_Shdr *)((uint64_t)filearray + ehdr->e_shoff);
    Elf64_Phdr *phdrtbl = (Elf64_Phdr *)((uint64_t)filearray + ehdr->e_phoff);
//    char *shstrtbl = (char *)((uint64_t)filearray + shdrtbl[ehdr->e_shstrndx].sh_offset);

    for (int i = 0; i < ehdr->e_phnum; i++) {
        Elf64_Phdr phdr = phdrtbl[i];
        if (phdr.p_type == PT_INTERP) {
            // TODO: Implement
        } else if (phdr.p_type == PT_LOAD) {
            if (!elf_set_memory_info_or_false(memory_info, phdr.p_vaddr, phdr.p_memsz))
                return false;
            memcpy(memory + phdr.p_vaddr, filearray + phdr.p_offset, phdr.p_filesz);
            memset(memory + phdr.p_vaddr + phdr.p_filesz, 0, phdr.p_memsz - phdr.p_filesz);
        }
    }

    return true;
}
