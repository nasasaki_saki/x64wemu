//
// Created by tia on 2021/03/26.
//

#include "tools.h"
#include <stdint.h>
#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>

uint8_t *file_read(const char *filename) {
    FILE *const file = fopen(filename, "rb");
    if (file == NULL) {
        return NULL;
    }

    struct stat filestat;
    stat(filename, &filestat);
    const off_t filesize = filestat.st_size;

    uint8_t *const filebuf = calloc(filesize, 1);
    if (filebuf == NULL) {
        return NULL;
    }
    if (fread(filebuf, 1, filesize, file) != filesize) {
        return NULL;
    }

    return filebuf;
}
