//
// Created by tia on 2021/04/15.
//

#ifndef X64WEMU_SRC_X64INSTRUCTION_H
#define X64WEMU_SRC_X64INSTRUCTION_H

#include <stdint.h>

struct OpePrefix {
    uint8_t grp1;
    uint8_t grp2;
    uint8_t grp3;
    uint8_t grp4;
};

struct __attribute((packed)) OpeRexPrefix {
    unsigned b: 1;
    unsigned x: 1;
    unsigned r: 1;
    unsigned w: 1;
    unsigned _rex: 4;
};

struct __attribute((packed)) Ope2byteVexPrefix {
    uint8_t _2byte_vex;
    struct __attribute((packed)) {
        unsigned pp: 2;
        unsigned l: 1;
        unsigned vvvv: 4;
        unsigned r: 1;
    };
};

struct __attribute((packed)) Ope3byteVexPrefix {
    uint8_t _3byte_vex;
    struct __attribute((packed)) {
        unsigned m_mmmm:5;
        unsigned b: 1;
        unsigned x: 1;
        unsigned r: 1;
    };
    struct __attribute((packed)) {
        unsigned pp: 2;
        unsigned l: 1;
        unsigned vvvv: 4;
        unsigned w: 1;
    };
};

struct __attribute((packed)) OpeEvexPrefix {
    uint8_t _evex;
    struct __attribute((packed)) {
        unsigned mm: 2;
        unsigned _0: 2;
        unsigned r2: 1;
        unsigned rxb: 3;
    };
    struct __attribute((packed)) {
        unsigned pp: 2;
        unsigned _1: 1;
        unsigned vvvv: 4;
        unsigned w: 1;
    };
    struct __attribute((packed)) {
        unsigned aaa: 3;
        unsigned v: 1;
        unsigned b: 1;
        unsigned l: 1;
        unsigned l2: 1;
        unsigned z: 1;
    };
};

union OpeVexPrefix {
    struct Ope2byteVexPrefix byte_2;
    struct Ope3byteVexPrefix byte_3;
};

struct __attribute((packed)) OpeModRM {
    unsigned rm: 3;
    unsigned reg: 3;
    unsigned mod: 2;
};

struct __attribute((packed)) OpeSib {
    unsigned base: 3;
    unsigned index: 3;
    unsigned scale: 2;
};

struct Instruction {
    _Bool has_prefix;
    struct OpePrefix prefix;
    _Bool has_rex_prefix;
    struct OpeRexPrefix rex_prefix;
    uint8_t vex_size; // 0 is NULL
    union OpeVexPrefix vex_prefix;
    _Bool has_evex_prefix;
    struct OpeEvexPrefix evex_prefix;
    uint8_t opcode_size;
    uint8_t opcode[3];
    struct OpeModRM mod_rm;
    struct OpeSib sib;
    union OpeDisplacement {
        uint8_t byte;
        uint16_t word;
        uint32_t dword;
    } displacement;
    union {
        uint8_t byte;
        uint16_t word;
        uint32_t dword;
        uint64_t sword;
        uint64_t qword;
        struct {
            uint64_t low;
            uint16_t high;
        } tword;
    } code_offset;
    union {
        uint8_t byte;
        uint16_t word;
        uint32_t dword;
        uint64_t qword;
    } immediate;
    uint8_t operation_size;
};

#endif //X64WEMU_SRC_X64INSTRUCTION_H
