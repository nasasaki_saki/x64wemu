//
// Created by tia on 2021/03/19.
//

#ifndef X64EMU_SRC_X64MACHINE_H
#define X64EMU_SRC_X64MACHINE_H

#include "IA32e-64/register.h"
#include <stdint.h>

#define BYTE 1
#define KBYTE 1024
#define MBYTE 0x100000
#define GBYTE 0x40000000

#define MEMORY_ACCESS(T, machine, addr) (*(T*)(&((machine)->memory[(addr)])))
#define MEMORY_ACCESS_WRITE(T, machine, addr) (*(T*)(&((machine)->memory[(addr)])))

enum ErrorCode {
    ERROR_UD,  // TODO: set vector code
    ERROR_NULL_POINTER,
    NO_ERROR = 256,
    ERROR_NOT_IMPLEMENTED,
    ERROR_PANIC,
};

struct ErrorInfo {
    enum ErrorCode code;
    uint32_t info;
};

union X64Register {
    uint64_t r;
    uint32_t e;  // at mov to e, high 32bit must be 0
    uint16_t x;
    __attribute((packed)) struct {
        uint8_t l;
        uint8_t h;
    };
};

enum MemoryType {
    MEMORY_ELF_LOAD,
    MEMORY_STACK
};

struct MemoryMapEntry {
    uint64_t start;
    uint64_t size;
    enum MemoryType type;
};

struct MemoryInfo {
    uint64_t size;
    uint64_t map_len_max;
    uint64_t map_len;
    struct MemoryMapEntry *memory_map;
};

struct X64Machine {
    union X64Register a;
    union X64Register b;
    union X64Register c;
    union X64Register d;
    union X64Register di;
    union X64Register si;
    union X64Register sp;
    union X64Register bp;
    union X64Register ip;
    union X64Register r8;
    union X64Register r9;
    union X64Register r10;
    union X64Register r11;
    union X64Register r12;
    union X64Register r13;
    union X64Register r14;
    union X64Register r15;

    struct RFLAGS rflags;

    _Bool exited;
    uint64_t exit_code;

    struct MemoryInfo memory_info;
    uint8_t *memory;
};

#define PREFIX_LOCK 0xf0
#define PREFIX_REP_NE_NZ 0xf2
#define PREFIX_REP_E_Z 0xf3
#define PREFIX_CS_OVERRIDE 0x2e
#define PREFIX_SS_OVERRIDE 0x36
#define PREFIX_DS_OVERRIDE 0x3e
#define PREFIX_ES_OVERRIDE 0x26
#define PREFIX_FS_OVERRIDE 0x64
#define PREFIX_GS_OVERRIDE 0x65
#define PREFIX_OPERAND_SIZE_OVERRIDE 0x66
#define PREFIX_ADDR_SIZE_OVERRIDE 0x67

extern const union X64Register riz_r;

void run(uint8_t *memory, uint64_t rip, struct MemoryInfo *memory_info);
void run_disassemble(uint8_t *memory, uint64_t rip, uint64_t size, struct MemoryInfo *memory_info, _Bool stop_on_error);
void run_both(uint8_t *memory, uint64_t rip, struct MemoryInfo *memory_info);
void run_fetch(uint8_t *memory, uint64_t rip, uint64_t size, struct MemoryInfo *memory_info);

#endif //X64EMU_SRC_X64MACHINE_H
