//
// Created by tia on 2021/03/26.
//

#ifndef X64WEMU_SRC_TOOLS_H
#define X64WEMU_SRC_TOOLS_H

#include <stdint.h>

uint8_t *file_read(const char *filename);

#endif //X64WEMU_SRC_TOOLS_H
