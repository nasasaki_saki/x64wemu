//
// Created by tia on 2021/03/20.
//

#ifndef X64EMU_SRC_X64FETCH_H
#define X64EMU_SRC_X64FETCH_H

#include "x64machine.h"
#include "x64instruction.h"

uint8_t fetch_all_prefix(struct X64Machine * machine, struct Instruction * operation);
uint8_t fetch_mod_rm(struct X64Machine *machine, uint8_t offset, struct Instruction *dist);

#endif //X64EMU_SRC_X64FETCH_H
