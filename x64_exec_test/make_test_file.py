from itertools import permutations, product
import argparse
import os

rx = ['rax', 'rbx', 'rcx', 'rdx', 'rsi', 'rdi', 'rsp', 'rbp',
      'r8', 'r9', 'r10', 'r11', 'r12', 'r13', 'r14', 'r15']
ex = ['eax', 'ebx', 'ecx', 'edx', 'esi', 'edi', 'esp', 'ebp',
      'r8d', 'r9d', 'r10d', 'r11d', 'r12d', 'r13d', 'r14d', 'r15d']
x = ['ax', 'bx', 'cx', 'dx', 'si', 'di', 'sp', 'bp',
     'r8w', 'r9w', 'r10w', 'r11w', 'r12w', 'r13w', 'r14w', 'r15w']
l = ['al', 'bl', 'cl', 'dl', 'sil', 'dil', 'spl', 'bpl',
     'r8b', 'r9b', 'r10b', 'r11b', 'r12b', 'r13b', 'r14b', 'r15b']
h = ['ah', 'bh', 'ch', 'dh']
m = ['[{0} + 0x77]', '[{0} - 0x11]', '[{0} + 0x77eeddcc]', '[{0} - 0x11223344]']
imm8 = ['0x7f', '0xfffffff7']  #
imm32 = ['0x77777777', '0xfffffff7']

parser = argparse.ArgumentParser()
parser.add_argument("mnemonic", help="test mnimonic")
parser.add_argument("register_set", help="use register set")
args = parser.parse_args()

dir_path = os.path.dirname(os.path.realpath(__file__))
print(open(dir_path + os.path.sep + '_base.s.txt').read())

for regset in args.register_set.split(','):

    if regset == 'none':
        print(args.mnemonic)

    if regset == 'r64':
        for i in rx:
            print(f'{args.mnemonic} {i}')

    if regset == 'r64-r64':
        for i in product(rx, repeat=2):
            print(f'{args.mnemonic} {i[0]}, {i[1]}')

    if regset == 'r64-imm8':
        for i in product(rx, imm8):
            print(f'{args.mnemonic} {i[0]}, {i[1]}')

    if regset == 'r64-imm32':
        for i in product(rx, imm32):
            print(f'{args.mnemonic} {i[0]}, {i[1]}')

    if regset == 'm64-imm32':
        for i in product(m, rx+ex, imm32):
            print(f'{args.mnemonic} qword {i[0].format(i[1])}, {i[2]}')

    if regset == 'r32':
        for i in ex:
            print(f'{args.mnemonic} {i}')

    if regset == 'r32-r32':
        for i in product(ex, repeat=2):
            print(f'{args.mnemonic} {i[0]}, {i[1]}')

    if regset == 'r16':
        for i in x:
            print(f'{args.mnemonic} {i}')

    if regset == 'r16-r16':
        for i in product(x, repeat=2):
            print(f'{args.mnemonic} {i[0]}, {i[1]}')

    if regset == 'r8-r8':
        for i in product(l, repeat=2):
            print(f'{args.mnemonic} {i[0]}, {i[1]}')
        for i in product(l[:4] + h, repeat=2):
            print(f'{args.mnemonic} {i[0]}, {i[1]}')

    if regset == 'r8':
        for i in l + h:
            print(f'{args.mnemonic} {i}')

    if regset == 'm8-r8':
        for i in product(m, rx + ex):
            print(f'{args.mnemonic} byte {i[0].format(i[1])}, al')
        print(f'{args.mnemonic} byte [0x7e], al')
        print(f'{args.mnemonic} byte [-0x22], al')
        print(f'{args.mnemonic} byte [0x7feeddcc], al')
        print(f'{args.mnemonic} byte [-0x22334455], al')

    if regset == 'near':
        print(f'{args.mnemonic} _start + 0xff')
