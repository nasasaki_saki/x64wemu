#!/bin/bash

dir=$(dirname $0)

cat ${dir}/tests.txt | while read line; do
  if [ ${line:0:1} == '#' ]; then echo "skipped ${line}"
  else
    arr=(${line})
    python ${dir}/make_test_file.py ${arr[1]} ${arr[2]} > ${dir}/${arr[0]}.s
  fi
done

for i in ${dir}/*.s; do

  name=$(basename $i | awk '{split($0, r, "."); print r[1]}')
  src=${dir}/${name}.s
  work_name=${dir}/work/${name}

  echo "--- ${name} test ---"

  nasm -f elf64 ${src} -o ${work_name}.o -w-number-overflow
  ld ${work_name}.o -o ${work_name}.elf
  objdump -d -j .text -M intel ${work_name}.elf > ${work_name}-full-lst.txt
  objdump -d -j .text -M intel ${work_name}.elf --no-show-raw-insn --no-addresses \
    | awk '{if (NR>7) {gsub(/^\s*/, "", $0); gsub(/\s+/, " ", $0); gsub(",", ", ", $0); \
    gsub("PTR ", "", $0); $0 = tolower($0); print $0;}}' > ${work_name}-lst.txt
  $1 disasm ${work_name}.elf > ${work_name}-lst-result.txt

  diff ${work_name}-lst.txt ${work_name}-lst-result.txt

done