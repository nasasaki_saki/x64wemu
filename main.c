#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "src/elfdecoder.h"
#include "src/x64machine.h"
#include "src/tools.h"

int main(int argc, char **argv) {
//    uint8_t *filebuf = file_read(TESTFILE_PATH "/test");
    if (argc < 3) {
        fprintf(stderr, "must 2 parameter; mode, file\n");
        exit(1);
    }

    _Bool fetch_debug = false;
    if (argc >= 4) {
        fetch_debug = strcmp(argv[3], "fetch-debug") == 0;
    }

    char *mode = argv[1];
    char *filename = argv[2];

    uint8_t *filebuf = file_read(filename);

    if (filebuf == NULL) {
        perror("file_read");
        return 1;
    }

    uint64_t memory_size = 8 * MBYTE;
    uint64_t memory_map_size = 20;
    uint8_t *const memory = malloc(memory_size);
    if (memory == NULL) {
        perror("malloc");
        return 1;
    }
    struct MemoryMapEntry *memory_map = malloc(memory_map_size * sizeof(struct MemoryMapEntry));
    if (memory_map == NULL) {
        perror("malloc");
        return 1;
    }
    memset(memory_map, 0, memory_map_size * sizeof(struct MemoryMapEntry));
    struct MemoryInfo memory_info = {memory_size,  memory_map_size, 0, memory_map};

    if (!elf_memory_load(filebuf, memory, &memory_info)) {
        return 1;
    }
    const uint64_t entry_point = elf_get_entry_point(filebuf);

    if (strcmp(mode, "run") == 0) {
        run(memory, entry_point, &memory_info);
    } else if (strcmp(mode, "disasm") == 0) {
        uint64_t text_size = elf_get_text_section_size(filebuf);
        run_disassemble(memory, entry_point, text_size, &memory_info, true);
    } else if (strcmp(mode, "both") == 0) {
        run_both(memory, entry_point, &memory_info);
    } else if (strcmp(mode, "fetch") == 0) {
        uint64_t text_size = elf_get_text_section_size(filebuf);
        run_fetch(memory, entry_point, text_size, &memory_info);
    } else if (strcmp(mode, "disasm-test") == 0) {
        uint64_t text_size = elf_get_text_section_size(filebuf);
        run_disassemble(memory, entry_point, text_size, &memory_info, false);
    } else {
        printf("mode must be 'run', 'disasm', 'both', 'fetch', 'disasm-test'\n");
        exit(1);
    }

    return 0;
}
