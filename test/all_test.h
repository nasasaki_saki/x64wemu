//
// Created by tia on 2021/04/22.
//

#ifndef X64WEMU_TEST_TEST_ALL_TEST_H
#define X64WEMU_TEST_TEST_ALL_TEST_H

void test_type_x64_register();
void test_get_byte_register();
void test_get_register();
void test_get_addr_from_modRM_64bit();

#endif //X64WEMU_TEST_TEST_ALL_TEST_H
