//
// Created by tia on 2021/04/24.
//

#include "../../src/x64machine.h"
#include "../no_stop_assert.h"
#include <stdio.h>

void test_type_x64_register() {
    union X64Register r;
    r.r = 0x0123456789abcdef;
    NO_STOP_ASSERT(r.r == 0x0123456789abcdef);
    NO_STOP_ASSERT(r.e ==         0x89abcdef);
    NO_STOP_ASSERT(r.x ==             0xcdef);
    NO_STOP_ASSERT(r.l ==               0xef);
    NO_STOP_ASSERT(r.h ==             0xcd);
    puts("test_type_x64_register done.");
}
