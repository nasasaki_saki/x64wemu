//
// Created by tia on 2021/04/22.
//

#include "../../src/x64machine.h"
#include "../../src/executors/x64executortool.h"
#include "../no_stop_assert.h"
#include <stdio.h>

_Bool cmp_byte_register(struct RegisterWithBytePosition a, struct RegisterWithBytePosition b) {
    return a.pos == b.pos && a.register_p == b.register_p;
}

void test_get_byte_register() {
    // intel sdm vol 2a Table 3-1. Register Codes Associated With +rb, +rw, +rd, +ro
    struct X64Machine machine = {0};
    struct RegisterWithBytePosition expect = {0};
    expect.pos = LOW;
    expect.register_p = &machine.a;
    NO_STOP_ASSERT(cmp_byte_register(get_byte_register(&machine, 0), expect));
    expect.register_p = &machine.c;
    NO_STOP_ASSERT(cmp_byte_register(get_byte_register(&machine, 1), expect));
    expect.register_p = &machine.d;
    NO_STOP_ASSERT(cmp_byte_register(get_byte_register(&machine, 2), expect));
    expect.register_p = &machine.b;
    NO_STOP_ASSERT(cmp_byte_register(get_byte_register(&machine, 3), expect));
    expect.pos = HIGH;
    expect.register_p = &machine.a;
    NO_STOP_ASSERT(cmp_byte_register(get_byte_register(&machine, 4), expect));
    expect.register_p = &machine.c;
    NO_STOP_ASSERT(cmp_byte_register(get_byte_register(&machine, 5), expect));
    expect.register_p = &machine.d;
    NO_STOP_ASSERT(cmp_byte_register(get_byte_register(&machine, 6), expect));
    expect.register_p = &machine.b;
    NO_STOP_ASSERT(cmp_byte_register(get_byte_register(&machine, 7), expect));

    puts("test_get_byte_register done");
}
