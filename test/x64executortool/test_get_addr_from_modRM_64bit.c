//
// Created by tia on 2021/04/22.
//

#include "../../src/x64machine.h"
#include "../../src/executors/x64executortool.h"
#include "../no_stop_assert.h"
#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>

#define RAX 0x1234567890abcdef
#define RCX 0x234567890abcdef1
#define RDX 0x34567890abcdef12
#define RBX 0x4567890abcdef123
#define RSP 0x567890abcdef1234
#define RBP 0x67890abcdef12345
#define RSI 0x7890abcdef123456
#define RDI 0x890abcdef1234567

#define EAX_EXTENDED 0xffffffff90abcdef

#define R8  0x90abcdef12345678
#define R9  0x0abcdef123456789
#define R10 0xabcdef1234567890
#define R11 0xbcdef1234567890a
#define R12 0xcdef1234567890ab
#define R13 0xdef1234567890abc
#define R14 0xef1234567890abcd
#define R15 0xf1234567890abcde

#define DISP_BYTE 0x12
#define DISP_DWORD 0x3456789a

#define SET_MACHINE_REGISTER(machine) do { \
    machine.a.r = RAX;                     \
    machine.c.r = RCX;                     \
    machine.d.r = RDX;                     \
    machine.b.r = RBX;                     \
    machine.sp.r = RSP;                    \
    machine.bp.r = RBP;                    \
    machine.si.r = RSI;                    \
    machine.di.r = RDI;                     \
    machine.r8.r = R8;                     \
    machine.r9.r = R9;                     \
    machine.r10.r = R10;                   \
    machine.r11.r = R11;                   \
    machine.r12.r = R12;                   \
    machine.r13.r = R13;                   \
    machine.r14.r = R14;                   \
    machine.r15.r = R15;                   \
} while (false)

#define SET_MOD_RM_DATA(ins, mod_v, rm_v, disp_v, disp_size) do { \
    ins.mod_rm.mod = mod_v;                                   \
    ins.mod_rm.rm = rm_v;                                     \
    ins.displacement.disp_size = disp_v;                      \
} while (false)

#define SET_SIB_DATA(ins, scale_v, index_v, base_v) do { \
    ins.sib.scale = scale_v;                         \
    ins.sib.index = index_v;                         \
    ins.sib.base = base_v;                           \
} while (false)

#define SET_REX_DATA(ins, has_rex, bb, xx) do { \
    ins.has_rex_prefix = has_rex;           \
    ins.rex_prefix.b = bb;                  \
    ins.rex_prefix.x = xx;                  \
} while (false)

#define SET_REX_B(ins, bb) SET_REX_DATA(ins, true, bb, 0)

#define EQUAL_MOD_RM_POINTER(a, b) (                                    \
    a.kind == b.kind                                                    \
    && (                                                                \
        (a.kind == MODRM_MEMORY && a.memory_addr == b.memory_addr)      \
        || (a.kind == MODRM_REGISTER && a.register_p == b.register_p)   \
    )                                                                   \
)

void test_get_addr_from_modRM_64bit() {
    // intel sdm 2a Table 2-2. 32-Bit Addressing Forms with the ModR/M Byte
    //              Table 2-5. Special Cases of REX Encodings
    //              Table 2-7. RIP-Relative Addressing
    struct X64Machine machine = {0};
    machine.ip.r = 0x1234;
    struct Instruction instruction = {0};
    struct ModRMPointer expect;
    struct ModRMPointer actual;

    // mod: 00
#define DO_TEST_MOD_00(rm, expect_addr, bit_4) do {\
        SET_MACHINE_REGISTER(machine);                      \
        SET_MOD_RM_DATA(instruction, 0, rm, DISP_DWORD, dword); \
        SET_REX_B(instruction, bit_4);                      \
        expect.kind = MODRM_MEMORY;                         \
        expect.memory_addr = expect_addr + 0;               \
        actual = get_addr_from_modRM_64bit(&machine, &instruction); \
        NO_STOP_ASSERT(EQUAL_MOD_RM_POINTER(actual, expect));   \
    } while (false)

    DO_TEST_MOD_00(0, RAX, 0);
    DO_TEST_MOD_00(1, RCX, 0);
    DO_TEST_MOD_00(2, RDX, 0);
    DO_TEST_MOD_00(3, RBX, 0);
    // rm=4 is sib.
    DO_TEST_MOD_00(5, DISP_DWORD + machine.ip.r, 0);
    DO_TEST_MOD_00(6, RSI, 0);
    DO_TEST_MOD_00(7, RDI, 0);

    DO_TEST_MOD_00(0, R8, 1);
    DO_TEST_MOD_00(1, R9, 1);
    DO_TEST_MOD_00(2, R10, 1);
    DO_TEST_MOD_00(3, R11, 1);
    // rm=4 is sib.
    DO_TEST_MOD_00(5, DISP_DWORD + machine.ip.r, 1);
    DO_TEST_MOD_00(6, R14, 1);
    DO_TEST_MOD_00(7, R15, 1);

    instruction.prefix.grp4 = PREFIX_ADDR_SIZE_OVERRIDE;
    DO_TEST_MOD_00(0, RAX, 0);
    instruction.prefix.grp4 = 0;
#undef DO_TEST_MOD_00

    // mod: 01
#define DO_TEST_MOD_01(rm, expect_base, bit_4) do {\
        SET_MACHINE_REGISTER(machine);                      \
        SET_MOD_RM_DATA(instruction, 1, rm, DISP_BYTE, byte); \
        SET_REX_B(instruction, bit_4);          \
        expect.kind = MODRM_MEMORY;                         \
        expect.memory_addr = expect_base + DISP_BYTE;               \
        actual = get_addr_from_modRM_64bit(&machine, &instruction); \
        NO_STOP_ASSERT(EQUAL_MOD_RM_POINTER(actual, expect));   \
    } while (false)

    DO_TEST_MOD_01(0, RAX, 0);
    DO_TEST_MOD_01(1, RCX, 0);
    DO_TEST_MOD_01(2, RDX, 0);
    DO_TEST_MOD_01(3, RBX, 0);
    // rm=4 is sib.
    DO_TEST_MOD_01(5, RBP, 0);
    DO_TEST_MOD_01(6, RSI, 0);
    DO_TEST_MOD_01(7, RDI, 0);

    DO_TEST_MOD_01(0, R8, 1);
    DO_TEST_MOD_01(1, R9, 1);
    DO_TEST_MOD_01(2, R10, 1);
    DO_TEST_MOD_01(3, R11, 1);
    // rm=4 is sib.
    DO_TEST_MOD_01(5, R13, 1);
    DO_TEST_MOD_01(6, R14, 1);
    DO_TEST_MOD_01(7, R15, 1);
#undef DO_TEST_MOD_01

    // mod: 10
#define DO_TEST_MOD_10(rm, expect_base, bit_4) do {\
        SET_MACHINE_REGISTER(machine);                      \
        SET_MOD_RM_DATA(instruction, 2, rm, DISP_DWORD, dword); \
        SET_REX_B(instruction, bit_4);            \
        expect.kind = MODRM_MEMORY;                         \
        expect.memory_addr = expect_base + DISP_DWORD;               \
        actual = get_addr_from_modRM_64bit(&machine, &instruction); \
        NO_STOP_ASSERT(EQUAL_MOD_RM_POINTER(actual, expect));   \
    } while (false)
    
    DO_TEST_MOD_10(0, RAX, 0);
    DO_TEST_MOD_10(1, RCX, 0);
    DO_TEST_MOD_10(2, RDX, 0);
    DO_TEST_MOD_10(3, RBX, 0);
    // rm=4 is sib.
    DO_TEST_MOD_10(5, RBP, 0);
    DO_TEST_MOD_10(6, RSI, 0);
    DO_TEST_MOD_10(7, RDI, 0);

    DO_TEST_MOD_10(0, R8, 1);
    DO_TEST_MOD_10(1, R9, 1);
    DO_TEST_MOD_10(2, R10, 1);
    DO_TEST_MOD_10(3, R11, 1);
    // rm=4 is sib.
    DO_TEST_MOD_10(5, R13, 1);
    DO_TEST_MOD_10(6, R14, 1);
    DO_TEST_MOD_10(7, R15, 1);
#undef DO_TEST_MOD_10

    // mod: 11
#define DO_TEST_MOD_11(rm, expect_register, bit_4) do {\
        SET_MACHINE_REGISTER(machine);                          \
        SET_MOD_RM_DATA(instruction, 3, rm, DISP_DWORD, dword); \
        SET_REX_B(instruction, bit_4);              \
        expect.kind = MODRM_REGISTER;                           \
        expect.register_p = &machine.expect_register;           \
        actual = get_addr_from_modRM_64bit(&machine, &instruction); \
        NO_STOP_ASSERT(EQUAL_MOD_RM_POINTER(actual, expect));   \
    } while (false)

    // TODO: byte-register (ah, bh, ch, dh)

    DO_TEST_MOD_11(0, a, 0);
    DO_TEST_MOD_11(1, c, 0);
    DO_TEST_MOD_11(2, d, 0);
    DO_TEST_MOD_11(3, b, 0);
    DO_TEST_MOD_11(4, sp, 0);
    DO_TEST_MOD_11(5, bp, 0);
    DO_TEST_MOD_11(6, si, 0);
    DO_TEST_MOD_11(7, di, 0);

    DO_TEST_MOD_11(0, r8, 1);
    DO_TEST_MOD_11(1, r9, 1);
    DO_TEST_MOD_11(2, r10, 1);
    DO_TEST_MOD_11(3, r11, 1);
    DO_TEST_MOD_11(4, r12, 1);
    DO_TEST_MOD_11(5, r13, 1);
    DO_TEST_MOD_11(6, r14, 1);
    DO_TEST_MOD_11(7, r15, 1);
#undef DO_TEST_MOD_11

    // has sib.

#define DO_TEST_SIB(mod, disp, disp_size, scale, index, base, bb, xx, expect_addr) do {\
    SET_MACHINE_REGISTER(machine);                          \
    SET_MOD_RM_DATA(instruction, mod, 4, disp, disp_size); \
    SET_SIB_DATA(instruction, scale, index, base);         \
    SET_REX_DATA(instruction, true, bb, xx);              \
    expect.kind = MODRM_MEMORY;                           \
    expect.memory_addr = (expect_addr);           \
    actual = get_addr_from_modRM_64bit(&machine, &instruction); \
    NO_STOP_ASSERT(EQUAL_MOD_RM_POINTER(actual, expect));   \
    } while (false)

    DO_TEST_SIB(0, 0, byte, 0, 0, 0, 0, 0, machine.a.r + 0 + machine.a.r * 1);
    DO_TEST_SIB(0, 0, byte, 1, 0, 0, 0, 0, machine.a.r + 0 + machine.a.r * 2);
    DO_TEST_SIB(0, 0, byte, 2, 0, 0, 0, 0, machine.a.r + 0 + machine.a.r * 4);
    DO_TEST_SIB(0, 0, byte, 3, 0, 0, 0, 0, machine.a.r + 0 + machine.a.r * 8);

    DO_TEST_SIB(0, 0, byte, 0, 0, 0, 0, 0, machine.a.r + 0 + machine.a.r * 1);
    DO_TEST_SIB(0, 0, byte, 0, 1, 0, 0, 0, machine.a.r + 0 + machine.c.r * 1);
    DO_TEST_SIB(0, 0, byte, 0, 2, 0, 0, 0, machine.a.r + 0 + machine.d.r * 1);
    DO_TEST_SIB(0, 0, byte, 0, 3, 0, 0, 0, machine.a.r + 0 + machine.b.r * 1);
    DO_TEST_SIB(0, 0, byte, 0, 4, 0, 0, 0, machine.a.r + 0 + riz_r.r * 1);
    DO_TEST_SIB(0, 0, byte, 0, 5, 0, 0, 0, machine.a.r + 0 + machine.bp.r * 1);
    DO_TEST_SIB(0, 0, byte, 0, 6, 0, 0, 0, machine.a.r + 0 + machine.si.r * 1);
    DO_TEST_SIB(0, 0, byte, 0, 7, 0, 0, 0, machine.a.r + 0 + machine.di.r * 1);

    // TODO: more...

    puts("test_get_addr_from_modRM_64bit done");
}
