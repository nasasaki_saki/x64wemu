//
// Created by tia on 2021/04/22.
//

#include "../../src/x64machine.h"
#include "../../src/executors/x64executortool.h"
#include "../no_stop_assert.h"
#include <stdio.h>
#include <stdbool.h>

void test_get_register() {
    // intel sdm vol 2a Table 3-1. Register Codes Associated With +rb, +rw, +rd, +ro
    struct X64Machine machine = {0};
    NO_STOP_ASSERT(get_register(&machine, 0, false) == &machine.a);
    NO_STOP_ASSERT(get_register(&machine, 1, false) == &machine.c);
    NO_STOP_ASSERT(get_register(&machine, 2, false) == &machine.d);
    NO_STOP_ASSERT(get_register(&machine, 3, false) == &machine.b);
    NO_STOP_ASSERT(get_register(&machine, 4, false) == &machine.sp);
    NO_STOP_ASSERT(get_register(&machine, 5, false) == &machine.bp);
    NO_STOP_ASSERT(get_register(&machine, 6, false) == &machine.si);
    NO_STOP_ASSERT(get_register(&machine, 7, false) == &machine.di);

    NO_STOP_ASSERT(get_register(&machine, 0, true) == &machine.r8);
    NO_STOP_ASSERT(get_register(&machine, 1, true) == &machine.r9);
    NO_STOP_ASSERT(get_register(&machine, 2, true) == &machine.r10);
    NO_STOP_ASSERT(get_register(&machine, 3, true) == &machine.r11);
    NO_STOP_ASSERT(get_register(&machine, 4, true) == &machine.r12);
    NO_STOP_ASSERT(get_register(&machine, 5, true) == &machine.r13);
    NO_STOP_ASSERT(get_register(&machine, 6, true) == &machine.r14);
    NO_STOP_ASSERT(get_register(&machine, 7, true) == &machine.r15);

    NO_STOP_ASSERT(get_register(&machine, 8, false) == NULL);
    NO_STOP_ASSERT(get_register(&machine, 8, true) == NULL);

    puts("test_get_register done");
}
