//
// Created by tia on 2021/04/23.
//

#ifndef X64WEMU_TEST_TEST_NO_STOP_ASSERT_H
#define X64WEMU_TEST_TEST_NO_STOP_ASSERT_H

#include <stdio.h>
#include <stdbool.h>

#define NO_STOP_ASSERT(condition) do { \
    if (!(condition))                     \
        fprintf(stderr, "%s:%d : %s : Assertion `" #condition "` failed.\n", __FILE__, __LINE__, __func__); \
} while (false)

#endif //X64WEMU_TEST_TEST_NO_STOP_ASSERT_H
