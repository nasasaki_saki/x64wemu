//
// Created by tia on 2021/04/22.
//

#include "all_test.h"

int main() {
    test_type_x64_register();
    test_get_byte_register();
    test_get_register();
    test_get_addr_from_modRM_64bit();
    return 0;
}
