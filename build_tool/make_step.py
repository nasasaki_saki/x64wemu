from pprint import pprint
from typing import *
from dataclasses import dataclass
import re
from jinja2 import Template, Environment, FileSystemLoader
import pathlib
from collections import Counter, defaultdict

from parse_x86_csv import parse_x86, ParsedInstruction as Instruction

explicit_operand_opcode = ['CMPS', 'INS', 'LODS', 'MOVS', 'OUTS', 'SCAS', 'STOS', 'XLAT']
trim_instructions = {  # TODO: xmm
    'CMOVNBE r16, r/m16',  # same as CMOVA
    'CMOVNBE r32, r/m32',
    'CMOVNBE r64, r/m64',
    'CMOVNB r16, r/m16',
    'CMOVNB r32, r/m32',
    'CMOVNB r64, r/m64',
    'CMOVNC r16, r/m16',
    'CMOVNC r32, r/m32',
    'CMOVNC r64, r/m64',
    'CMOVC r16, r/m16',  # same as CMOVAE
    'CMOVC r32, r/m32',
    'CMOVC r64, r/m64',
    'CMOVNAE r16, r/m16',  # same as CMOVB
    'CMOVNAE r32, r/m32',
    'CMOVNAE r64, r/m64',
    'CMOVNA r16, r/m16',  # same as CMOVBE
    'CMOVNA r32, r/m32',
    'CMOVNA r64, r/m64',
    'CMOVNLE r16, r/m16',  # same as CMOVG
    'CMOVNLE r32, r/m32',
    'CMOVNLE r64, r/m64',
    'CMOVNL r16, r/m16',  # same as CMOVGE
    'CMOVNL r32, r/m32',
    'CMOVNL r64, r/m64',
    'CMOVNGE r16, r/m16',  # same as CMOVL
    'CMOVNGE r32, r/m32',
    'CMOVNGE r64, r/m64',
    'CMOVNG r16, r/m16',  # same as CMOVLE
    'CMOVNG r32, r/m32',
    'CMOVNG r64, r/m64',
    'CMOVNZ r16, r/m16',  # same as CMOVNE
    'CMOVNZ r32, r/m32',
    'CMOVNZ r64, r/m64',
    'CMOVPE r16, r/m16',  # same as CMOVP
    'CMOVPE r32, r/m32',
    'CMOVPE r64, r/m64',
    'CMOVPO r16, r/m16',
    'CMOVPO r32, r/m32',
    'CMOVPO r64, r/m64',
    'CMOVZ r16, r/m16',
    'CMOVZ r32, r/m32',
    'CMOVZ r64, r/m64',

    'MOVLPS xmm1, m64',  # same as MOVHLPS xmm1, xmm2
    'MOVHPS xmm1, m64',  # same as MOVLHPS xmm1, xmm2
    'MOVSD xmm1, m64',  # same as MOVSD xmm1, xmm2
    'MOVSS xmm1, m32',  # same as MOVSS xmm1, xmm2

    'VMOVLPS xmm2, xmm1, m64',  # same as VMOVHLPS xmm1, xmm2, xmm3
    'VMOVHPS xmm2, xmm1, m64',  # same as VMOVLHPS xmm1, xmm2, xmm3

    'VBROADCASTSS ymm1, m32',  # same as VBROADCASTSS ymm1, xmm2
    'VBROADCASTSD ymm1, m64',  # same as VBROADCASTSD ymm1, xmm2

    'JNBE rel8',  # same as JA
    'JNBE rel16',
    'JNBE rel32',
    'JNB rel8',
    'JNB rel16',
    'JNB rel32',
    'JNC rel8',  # same as JAE
    'JNC rel16',
    'JNC rel32',
    'JC rel8',
    'JC rel16',
    'JC rel32',
    'JNAE rel8',  # same as JB
    'JNAE rel16',
    'JNAE rel32',
    'JNA rel8',  # same as JBE
    'JNA rel16',
    'JNA rel32',
    'JRCXZ rel8',  # same as JRCXZ
    'JRCXZ rel16',
    'JRCXZ rel32',
    'JZ rel8',  # same as JE
    'JZ rel16',
    'JZ rel32',
    'JNLE rel8',  # same as JG
    'JNLE rel16',
    'JNLE rel32',
    'JNL rel8',  # same as JGE
    'JNL rel16',
    'JNL rel32',
    'JNGE rel8',  # same as JL
    'JNGE rel16',
    'JNGE rel32',
    'JNG rel8',  # same as JLE
    'JNG rel16',
    'JNG rel32',
    'JNZ rel8',  # same as JNE
    'JNZ rel16',
    'JNZ rel32',
    'JPO rel8',  # same as JNP
    'JPO rel16',
    'JPO rel32',
    'JPE rel8',  # same as JP
    'JPE rel16',
    'JPE rel32',

    'SETNBE r/m8',  # same as SETA
    'SETNB r/m8',
    'SETNC r/m8',  # same as SETAE
    'SETC r/m8',
    'SETNAE r/m8',  # same as SETB
    'SETNA r/m8',  # same as SETBE
    'SETNLE r/m8',  # same as SETG
    'SETNL r/m8',  # same as SETGE
    'SETNGE r/m8',  # same as SETL
    'SETNG r/m8',  # same as SETLE
    'SETZ r/m8',  # same as SETE
    'SETNZ r/m8',  # same as SETNE
    'SETNP r/m8',  # same as SETNE

    'FWAIT',  # same as WAIT

    'SHL r/m8, 1',
    'SHL r/m16,1',
    'SHL r/m32,1',
    'SHL r/m8, CL',
    'SHL r/m16, CL',
    'SHL r/m32, CL',
    'SHL r/m8, imm8',
    'SHL r/m16, imm8',
    'SHL r/m32, imm8',
    'SHL r/m64,1',
    'SHL r/m64, CL',
    'SHL r/m64, imm8',  # same as SAL

    'XCHG r/m8, r8',
    'XCHG r/m16, r16',
    'XCHG r/m32, r32',
    'XCHG r/m64, r64',  # same as XCHG r, r/m
    'XCHG r16, AX',
    'XCHG r32, EAX',
    'XCHG r64, RAX',  # same as *AX, r

    'ENTER imm16, 0',
    'ENTER imm16,1',  # same as ENTER imm16, imm8

    'VBROADCASTSS xmm1, m32',  # same as xmm1, xmm2
}


@dataclass
class Conditions:
    conditions: List[str]

    def get_all_condition(self):
        if len(self.conditions) == 0:
            return 'true'
        elif len(self.conditions) == 1:
            return self.conditions[0]
        else:
            return '(' + ') && ('.join(self.conditions) + ')'


class InstructionVar:
    name: str

    def __init__(self, name):
        self.name = name
        self.has_prefix = f'{name}.has_prefix'
        self.prefix = f'{name}.prefix'
        self.has_rex_prefix = f'{name}.has_rex_prefix'
        self.rex_prefix = f'{name}.rex_prefix'
        self.vex_size = f'{name}.vex_size'
        self.vex_prefix = f'{name}.vex_prefix'
        self.has_evex_prefix = f'{name}.has_evex_prefix'
        self.evex_prefix = f'{name}.evex_prefix'
        self.opcode_size = f'{name}.opcode_size'
        self.opcode = lambda i: f'{name}.opcode[{i}]'
        self.code_offset = f'{name}.code_offset'
        self.immediate = f'{name}.immediate'
        self.operation_size = f'{name}.operation_size'


# class InstructionVar:
#     name: str
#
#     def __init__(self, name):
#         self.name = name
#         self.raw = RawOperationVar(f'{name}.raw')
#         self.mnemonic = f'{name}.mnemonic'
#         self.opsize_overrode = f'{name}.opsize_overrode'
#         self.operand_1 = f'{name}.operand_1'
#         self.operand_2 = f'{name}.operand_2'
#         self.operand_3 = f'{name}.operand_3'
#         self.operand_4 = f'{name}.operand_4'
#

class MakeFetchDecode:
    instructions: List[Instruction]
    grouped: List[List[Instruction]]

    def __init__(self):
        self.instructions = parse_x86()
        self.trim_same_instructions(self.instructions)
        self.instructions.sort(key=lambda x: (-15 if x.rex or x.vex or x.evex else 0) + (- len(x.code)))
        self.grouped = self.grouping(self.instructions)
        self.group_normalize(self.grouped)
        self.v_ins = InstructionVar('i')
        self.v_machine = 'm'
        self.v_cptr = 'cptr'
        self.v_csize = 'csize'
        self.v_result = 'r'

    @staticmethod
    def trim_same_instructions(instructions: List[Instruction]):
        i = 0
        while i < len(instructions):
            ins = instructions[i]
            if ins.src.instruction in trim_instructions \
                    or ins.src.instruction.split()[0] in explicit_operand_opcode:
                instructions.pop(i)
            else:
                i += 1

    @staticmethod
    def grouping(inss, debug=False):
        result = []
        while inss:
            current = inss.pop(0)
            current_group = [current]
            j = 0
            while j < len(inss):
                other = inss[j]
                if current.is_same_opcode_for_fetch(other):
                    current_group.append(inss.pop(j))
                else:
                    j += 1
                if not debug and j > 2:  # 1行挟んであるところがある
                    break  # groupは長さが1 or 2
            if debug:
                if len(current_group) > 2:
                    pprint('over length 2')
                    pprint(current_group)
            result.append(current_group)
        return result

    @staticmethod
    def group_normalize(grouped: List[List[Instruction]], debug: bool = False):
        for group in grouped:
            if len(group) == 1:  # 64bit-invalidでもフェッチ・デコードはする
                continue
            i = 1
            if debug and len(group) > 2:
                pprint('over length 2')
                pprint(group)
            while i < len(group):
                # ただし同じコードで別命令が割り当てられていたら弾く
                if not group[i].src.is_valid_in_64bit:
                    group.pop(i)
                else:
                    i += 1

    def get_code_part_np(self, ins: Instruction):
        if ins.np:
            if ins.src.instruction.split()[0] == 'NOP':
                return [], []
            else:
                return [
                    f'{self.v_ins.prefix}.grp1 != PREFIX_REP_E_Z',
                    f'{self.v_ins.prefix}.grp1 != PREFIX_REP_NE_NZ',
                    f'{self.v_ins.prefix}.grp3 != PREFIX_OPERAND_SIZE_OVERRIDE'
                ], []
        else:
            return [], []

    def get_code_part_prefix(self, ins: Instruction):
        conditions = []
        if ins.prefix:
            conditions.append(f'{self.v_ins.has_prefix}')
            if ins.prefix == '66':
                conditions.append(f'{self.v_ins.prefix}.grp3 == PREFIX_OPERAND_SIZE_OVERRIDE')
            elif ins.prefix == 'F2':
                conditions.append(f'{self.v_ins.prefix}.grp1 == PREFIX_REP_NE_NZ')
            elif ins.prefix == 'F3':
                conditions.append(f'{self.v_ins.prefix}.grp1 == PREFIX_REP_E_Z')
        return conditions, []

    def get_code_part_rex(self, ins: Instruction):
        conditions = []
        if ins.rex:
            conditions.append(f'{self.v_ins.has_rex_prefix}')
            if ins.rex.ide:
                conditions.append(f'(_Bool){self.v_ins.rex_prefix}.{ins.rex.ide.lower()}')
        return conditions, []

    def get_code_part_vex(self, ins: Instruction):
        if ins.vex:
            vex_3 = []
            vex_2 = []
            vex_3.append(f'{self.v_ins.vex_size} == 3')
            if ins.vex.l == '128' or ins.vex.l == 'LZ' or ins.vex.l == 'L0':
                vex_3.append(f'{self.v_ins.vex_prefix}.byte_3.l == 0')
            elif ins.vex.l == '256':
                vex_3.append(f'{self.v_ins.vex_prefix}.byte_3.l == 1')

            if ins.vex.pp == '66':
                vex_3.append(f'{self.v_ins.vex_prefix}.byte_3.pp == 0x1')
            if ins.vex.pp == 'F3':
                vex_3.append(f'{self.v_ins.vex_prefix}.byte_3.pp == 0x2')
            if ins.vex.pp == 'F2':
                vex_3.append(f'{self.v_ins.vex_prefix}.byte_3.pp == 0x3')

            if ins.vex.mmmmm == '0F':
                vex_3.append(f'{self.v_ins.vex_prefix}.byte_3.m_mmmm == 1')
            elif ins.vex.mmmmm == '0F38':
                vex_3.append(f'{self.v_ins.vex_prefix}.byte_3.m_mmmm == 2')
            elif ins.vex.mmmmm == '0F3A':
                vex_3.append(f'{self.v_ins.vex_prefix}.byte_3.m_mmmm == 3')

            if ins.vex.w == 'W1':
                vex_3.append(f'(_Bool){self.v_ins.vex_prefix}.byte_3.w')
            elif ins.vex.w == 'W0':
                vex_3.append(f'!(_Bool){self.v_ins.vex_prefix}.byte_3.w')

            if not ins.vex.is_only_3byte():
                vex_2.append(f'{self.v_ins.vex_size} == 2')
                if ins.vex.l == '128' or ins.vex.l == 'LZ' or ins.vex.l == 'L0':
                    vex_2.append(f'{self.v_ins.vex_prefix}.byte_2.l == 0')
                elif ins.vex.l == '256':
                    vex_2.append(f'{self.v_ins.vex_prefix}.byte_2.l == 1')

                if ins.vex.pp == '66':
                    vex_2.append(f'{self.v_ins.vex_prefix}.byte_2.pp == 0x1')
                if ins.vex.pp == 'F3':
                    vex_2.append(f'{self.v_ins.vex_prefix}.byte_2.pp == 0x2')
                if ins.vex.pp == 'F2':
                    vex_2.append(f'{self.v_ins.vex_prefix}.byte_2.pp == 0x3')

                if ins.vex.mmmmm != '0F':
                    raise Exception('vex.mmmmm')

                c_2 = Conditions(vex_2)
                c_3 = Conditions(vex_3)
                return [f'({c_2.get_all_condition()}) || {c_3.get_all_condition()}'], []
            else:
                return vex_3, []
        else:
            return [], []

    def get_code_part_evex(self, ins: Instruction):
        conditions = []
        if ins.evex:
            conditions.append(f'{self.v_ins.has_evex_prefix}')
            if ins.evex.l == '128':
                conditions.append(f'{self.v_ins.evex_prefix}.l == 0')
                conditions.append(f'{self.v_ins.evex_prefix}.l2 == 0')
            if ins.evex.l == '256':
                conditions.append(f'{self.v_ins.evex_prefix}.l == 1')
                conditions.append(f'{self.v_ins.evex_prefix}.l2 == 0')
            if ins.evex.l == '512':
                # conditions.append(f'{self.var_instruction.evex_prefix}.l == 1')
                conditions.append(f'{self.v_ins.evex_prefix}.l2 == 1')

            if ins.evex.pp == '66':
                conditions.append(f'{self.v_ins.evex_prefix}.pp == 0x1')
            if ins.evex.pp == 'F3':
                conditions.append(f'{self.v_ins.evex_prefix}.pp == 0x2')
            if ins.evex.pp == 'F2':
                conditions.append(f'{self.v_ins.evex_prefix}.pp == 0x3')

            if ins.evex.mm == '0F':
                conditions.append(f'{self.v_ins.evex_prefix}.mm == 1')
            elif ins.evex.mm == '0F38':
                conditions.append(f'{self.v_ins.evex_prefix}.mm == 2')
            elif ins.evex.mm == '0F3A':
                conditions.append(f'{self.v_ins.evex_prefix}.mm == 3')

            if ins.evex.w == 'W1':
                conditions.append(f'(_Bool){self.v_ins.evex_prefix}.w')
            elif ins.evex.w == 'W0':
                conditions.append(f'!(_Bool){self.v_ins.evex_prefix}.w')
        return conditions, []

    def get_code_part_opcode_modrm(self, ins: Instruction):
        offset = 0
        conditions = []
        operations = []
        for i, c in enumerate(ins.code):
            if i == len(ins.code) - 1 and ins.register_indicator:
                conditions.append(f'({self.v_cptr}[{self.v_csize} + {i}] & 0xf8) == 0x{c}')
            else:
                conditions.append(f'{self.v_cptr}[{self.v_csize} + {i}] == 0x{c}')
            operations.append(f'{self.v_ins.opcode(i)} = {self.v_cptr}[{self.v_csize} ++];')
            operations.append(f'{self.v_ins.opcode_size} ++;')
            offset += 1

        if ins.mod_rm:
            if re.fullmatch(r'/[0-7]', ins.mod_rm):
                digit = ins.mod_rm[1]
                conditions.append(f'({self.v_cptr}[{self.v_csize} + {offset}] >> 3 & 7) == {digit}')
            operations.append(f'{self.v_csize} += fetch_mod_rm({self.v_machine}, {self.v_csize}, &{self.v_ins.name});')
            offset += 1
        
        return conditions, operations

    def get_code_part_immediate(self, ins: Instruction):
        operations = []
        for imm in ins.immediate:
            if re.match(r'c[bwdpot]', imm):
                if imm[1] == 'b':
                    operations.append(f'{self.v_ins.code_offset}.byte = *(uint8_t *)({self.v_cptr} + {self.v_csize});')
                    op_size = 1
                if imm[1] == 'w':
                    operations.append(f'{self.v_ins.code_offset}.word = *(uint16_t *)({self.v_cptr} + {self.v_csize});')
                    op_size = 2
                if imm[1] == 'd':
                    operations.append(f'{self.v_ins.code_offset}.dword = *(uint32_t *)({self.v_cptr} + {self.v_csize});')
                    op_size = 4
                if imm[1] == 'p':
                    operations.append(f'{self.v_ins.code_offset}.sword = (*(uint64_t *)({self.v_cptr} + {self.v_csize}) & 0x0000fffffffffffful);')
                    op_size = 6
                if imm[1] == 'o':
                    operations.append(f'{self.v_ins.code_offset}.qword = *(uint64_t *)({self.v_cptr} + {self.v_csize});')
                    op_size = 8
                if imm[1] == 't':
                    operations.append(f'{self.v_ins.code_offset}.tword.low = *(uint64_t *)({self.v_cptr} + {self.v_csize});')
                    operations.append(f'{self.v_ins.code_offset}.tword.high = *(uint16_t *)({self.v_cptr} + {self.v_csize} + 8);')
                    op_size = 10
                operations.append(f'{self.v_csize} += {op_size};')

            elif re.match(r'i[bwdo]', imm):
                if imm[1] == 'b':
                    operations.append(f'{self.v_ins.immediate}.byte = *(uint8_t *)({self.v_cptr} + {self.v_csize});')
                    op_size = 1
                elif imm[1] == 'w':
                    operations.append(f'{self.v_ins.immediate}.word = *(uint16_t *)({self.v_cptr} + {self.v_csize});')
                    op_size = 2
                elif imm[1] == 'd':
                    operations.append(f'{self.v_ins.immediate}.dword = *(uint32_t *)({self.v_cptr} + {self.v_csize});')
                    op_size = 4
                elif imm[1] == 'o':
                    operations.append(f'{self.v_ins.immediate}.qword = *(uint64_t *)({self.v_cptr} + {self.v_csize});')
                    op_size = 8
                operations.append(f'{self.v_csize} += {op_size};')
                
        return [], operations

    def get_code_part_all(self, ins: Instruction):
        np_c, np_o = self.get_code_part_np(ins)
        pre_c, pre_o = self.get_code_part_prefix(ins)
        rex_c, rex_o = self.get_code_part_rex(ins)
        vex_c, vex_o = self.get_code_part_vex(ins)
        evex_c, evex_o = self.get_code_part_evex(ins)
        op_c, op_o = self.get_code_part_opcode_modrm(ins)
        imm_c, imm_o = self.get_code_part_immediate(ins)
        return [*np_c, *pre_c, *rex_c, *vex_c, *evex_c, *op_c, *imm_c], \
               [
                   *np_o, *pre_o, *rex_o, *vex_o, *evex_o, *op_o, *imm_o,
                    f'{self.v_ins.operation_size} = {self.v_csize};',
                    f'{self.v_machine}->ip.r += {self.v_ins.operation_size};',
               ]

    def get_code_grouped(self) -> List[List[Tuple[str, List, Instruction, List]]]:
        result = []
        for g in self.grouped:
            if len(g) == 1:
                c, o = self.get_code_part_all(g[0])
                e_o = self.get_code_execute(g[0])
                e_disasm_o = self.get_code_disassemble(g[0])
                c = Conditions(c)
                result.append([(c.get_all_condition(), o + e_o, g[0], o + e_disasm_o)])
            elif len(g) == 2:
                c0, o0 = self.get_code_part_all(g[0])
                c1, o1 = self.get_code_part_all(g[1])
                e_o0 = self.get_code_execute(g[0], True)
                e_o1 = self.get_code_execute(g[1])
                e_disasm_o0 = self.get_code_disassemble(g[0], True)
                e_disasm_o1 = self.get_code_disassemble(g[1])

                c0 = Conditions(c0)
                c1 = Conditions(c1)
                result.append([
                    (c0.get_all_condition(), o0 + e_o0, g[0], o0 + e_disasm_o0),
                    (c1.get_all_condition(), o1 + e_o1, g[1], o1 + e_disasm_o1),
                ])
            else:
                raise Exception('len')
        return result

    def get_code_all_grouped(self) -> List[List[Dict[str, Union[str, Instruction]]]]:
        result = []
        for g in self.grouped:
            if len(g) == 1:
                c, o = self.get_code_part_all(g[0])
                e_o = self.get_code_execute(g[0])
                e_disasm_o = self.get_code_disassemble(g[0])

                c = Conditions(c)

                result.append([{
                    'condition': c.get_all_condition(),
                    'fetch_code': o,
                    'execute_code': e_o,
                    'disassemble_code': e_disasm_o,
                    'instruction': g[0]
                }])
            elif len(g) == 2:
                c0, o0 = self.get_code_part_all(g[0])
                c1, o1 = self.get_code_part_all(g[1])
                e_o0 = self.get_code_execute(g[0], True)
                e_o1 = self.get_code_execute(g[1])
                e_disasm_o0 = self.get_code_disassemble(g[0], True)
                e_disasm_o1 = self.get_code_disassemble(g[1])

                c0 = Conditions(c0)
                c1 = Conditions(c1)

                result.append([{
                    'condition': c0.get_all_condition(),
                    'fetch_code': o0,
                    'execute_code': e_o0,
                    'disassemble_code': e_disasm_o0,
                    'instruction': g[0]
                }, {
                    'condition': c1.get_all_condition(),
                    'fetch_code': o1,
                    'execute_code': e_o1,
                    'disassemble_code': e_disasm_o1,
                    'instruction': g[1]
                }])
            else:
                raise Exception('len')
        return result

    @staticmethod
    def get_operand_enum(o: str):
        if o is None:
            return 'NA'
        elif o == 'ModRM:r/m (r)':
            return 'MOD_RM_R'
        elif o == 'ModRM:r/m (r, w)':
            return 'MOD_RM_RW'
        elif o == 'ModRM:r/m (w)':
            return 'MOD_RM_W'
        elif o == 'ModRM:reg (r)':
            return 'MOD_REG_R'
        elif o == 'ModRM:reg (r, w)':
            return 'MOD_REG_RW'
        elif o == 'ModRM:reg (w)':
            return 'MOD_REG_W'
        else:
            return 'NOT_IMPLEMENTED'

    # def get_code_decode(self, ins: Instruction, overrode: bool=False):
    #     return [
    #         f'{self.v_ins.mnemonic} = {ins.src.instruction.split()[0].upper()};',
    #         f'{self.v_ins.opsize_overrode} = {str(overrode).lower()};',
    #         f'{self.v_ins.operand_1} = {self.get_operand_enum(ins.src.op_1)};',
    #         f'{self.v_ins.operand_2} = {self.get_operand_enum(ins.src.op_2)};',
    #         f'{self.v_ins.operand_3} = {self.get_operand_enum(ins.src.op_3)};',
    #         f'{self.v_ins.operand_4} = {self.get_operand_enum(ins.src.op_4)};',
    #     ]

    def get_code_execute(self, ins: Instruction, overrode: bool = False):
        return [
            f'{self.v_result} = exec_{self.get_name(ins, overrode)}({self.v_machine}, &{self.v_ins.name});'
        ]

    def get_code_disassemble(self, ins: Instruction, overrode: bool = False):
        return [
            f'{self.v_result} = exec_{self.get_name(ins, overrode)}_disassemble({self.v_machine}, &{self.v_ins.name});'
        ]

    @staticmethod
    def get_name(ins: Instruction, overrode: bool=False):
        overrode = '_P66' if overrode else ''
        return ins.src.instruction.split()[0] + '_' + ins.src.opcode\
                    .replace(' ', '_') \
                    .replace('+r', 'r') \
                    .replace('+', '') \
                    .replace('.', '') \
                    .replace('/', '') \
                    .replace('(', '') \
                    .replace(')', '') \
                    .replace('=', '_') \
                    .replace('!', '_') \
               + overrode

    def get_code_executor(self, ins: Instruction, overrode: bool=False):
        return self.get_name(ins, overrode)

    @staticmethod
    def make():
        c_file_name = 'x64step'
        maker = MakeFetchDecode()
        grouped_code = maker.get_code_all_grouped()
        file_dir = pathlib.Path(__file__).parent
        c_file_path_c = pathlib.Path(__file__).parent.parent / 'src' / (c_file_name + '.c')

        env = Environment(loader=FileSystemLoader(file_dir))
        template = env.get_template('x64step.c.j2')
        c_file_c = c_file_path_c.open('w')
        c_file_c.write(template.render(
            file_name=c_file_name,
            func_name='step_',
            var_machine=maker.v_machine,
            ins=maker.v_ins.name,
            var_ope_size=maker.v_csize,
            var_code_ptr=maker.v_cptr,
            contents=grouped_code,
            result=maker.v_result,
            debug=True,
        ))
        c_file_c.close()

    @staticmethod
    def make_exec():
        result = []
        maker = MakeFetchDecode()
        for g in maker.grouped:
            if len(g) == 1:
                result.append((MakeFetchDecode.get_name(g[0]), g[0]))
            if len(g) == 2:
                result.append((MakeFetchDecode.get_name(g[0], True), g[0]))
                result.append((MakeFetchDecode.get_name(g[1]), g[1]))

        mnemonic_categorized:Dict[str, list[Tuple[str, Dict[str, str]]]] = defaultdict(list)
        for i in result:
            mnemonic_categorized[i[1].instruction.split()[0]].append(('exec_' + i[0], i[1].src.src))

        categorized = dict()
        for mnemonic, m_l in mnemonic_categorized.items():
            operand_categorized = defaultdict(list)
            for m_v in m_l:
                if re.match(r'[A-Z]+ +r/m[0-9][0-9]? ?, ?imm[0-9][0-9]?', m_v[1]['Instruction']):
                    operand_categorized['r/mN, immN'].append(m_v)
                elif re.match(r'[A-Z]+ +imm[0-9][0-9]? ?, ?r/m[0-9][0-9]?', m_v[1]['Instruction']):
                    operand_categorized['immN, r/mN'].append(m_v)
                elif re.match(r'[A-Z]+ +r/m[0-9][0-9]? ?, ?r[0-9][0-9]?', m_v[1]['Instruction']):
                    operand_categorized['r/mN, rN'].append(m_v)
                elif re.match(r'[A-Z]+ +r[0-9][0-9]? ?, ?r/m[0-9][0-9]?', m_v[1]['Instruction']):
                    operand_categorized['rN, r/mN'].append(m_v)
                elif re.match(r'[A-Z]+ +((RAX)|(EAX)|(AX)|(AL)) ?, ?imm[0-9][0-9]?', m_v[1]['Instruction']):
                    operand_categorized['*a*, immN'].append(m_v)
                else:
                    operand_categorized['not categorized'].append(m_v)
            categorized[mnemonic] = operand_categorized

        # output

        file_dir = pathlib.Path(__file__).parent
        c_executors_dir = file_dir.parent / 'src' / 'executors'
        env = Environment(loader=FileSystemLoader(file_dir))

        for key, value in categorized.items():
            template = env.get_template('executor.c.j2')
            file_name = 'exec_' + key
            c_gen_file = c_executors_dir / 'generate' / (file_name + '.c')
            c_handmade_file = c_executors_dir / 'handmade' / (file_name + '.c')
            if not c_handmade_file.is_file():
                with c_gen_file.open('w') as f:
                    f.write(template.render(contents=value))

        c_head_file = c_executors_dir / 'executor_all.h'
        template = env.get_template('executor_all.h.j2')
        with c_head_file.open('w') as f:
            # pprint(['exec_' + i[0] for i in result])
            f.write(template.render(name_list=['exec_' + i[0] for i in result]))


if __name__ == '__main__':
    MakeFetchDecode.make()
    MakeFetchDecode.make_exec()
