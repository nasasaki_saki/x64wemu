import csv
import pathlib
import re
from pprint import pprint
from dataclasses import dataclass
from typing import *


csv_file_path = pathlib.Path(__file__).parent.parent / 'x86-csv' / 'x86.csv'
# csv_file_path = pathlib.Path(__file__).parent / 'x86.csv'

PREFIX_REP_NE = 'F2'
PREFIX_REP_E = 'F3'
PREFIX_OPERAND_SIZE = '66'


@dataclass
class RawInstruction:
    instruction: str
    opcode: str
    is_valid_in_64bit: bool
    is_valid_in_32bit: bool
    is_valid_in_16bit: bool
    feature_flags: Optional[str]
    op_1: Optional[str]
    op_2: Optional[str]
    op_3: Optional[str]
    op_4: Optional[str]
    tuple_type: Optional[str]
    description: str
    src: Dict[str, str]

    def __str__(self):
        return f'''RawIns(ins=" {self.instruction:63}", code=" {self.opcode:35}")  # {self.description}'''

    @staticmethod
    def format_str(g: List['RawInstruction']):
        result = [''] * len(g)
        for key in g[0].src.keys():
            max_len = max([len(i.src[key]) for i in g])
            for i in range(len(g)):
                v = g[i].src[key]
                p = max_len - len(v)
                a = f', {key}: '
                a += ' ' * p
                a += '"' + v + '"'
                result[i] += a
        for i, s in enumerate(result):
            result[i] = s.lstrip(', ')
        return result


@dataclass
class RexPrefix:
    ide: Optional[str]


@dataclass
class VexPrefix:
    nd: Optional[str]
    l: str  # 128, 256, LIG, LZ
    pp: Optional[str]  # 66, F2, F3
    mmmmm: str  # 0F 0F3A 0F38
    w: Optional[str]  # W1, W0, WIG

    def is_only_3byte(self):
        return (
            self.mmmmm == '0F3A' or self.mmmmm == '0F38'
            or self.w == 'W1'
        )


@dataclass
class EvexPrefix:
    nd: Optional[str]
    l: str  # 128, 256, 512, LIG
    pp: Optional[str]  # 66, F2, F3
    mm: str  # 0F, 0F3A, 0F38
    w: Optional[str]  # W0, W1, WIG


@dataclass
class ParsedInstruction:
    instruction: str
    np: bool
    prefix: Optional[str]  # 66, F2, F3
    rex: Optional[RexPrefix]
    vex: Optional[VexPrefix]  # TODO: impl
    evex: Optional[EvexPrefix]
    code: List[str]
    register_indicator: Optional[str]
    mod_rm: Optional[str]
    is4: Optional[str]  # TODO: impl
    immediate: List[str]
    operands: List[str]
    src: RawInstruction

    def is_same_opcode_for_fetch(self, other: 'ParsedInstruction'):
        if self.mod_rm is None or other.mod_rm is None:
            m = True
        else:
            m = self.mod_rm[1] == 'r' or other.mod_rm[1] == 'r' or self.mod_rm == other.mod_rm

        return (
            self.prefix == other.prefix
            and self.rex == other.rex
            and self.vex == other.vex
            and self.evex == other.evex
            and self.code == other.code
            and m
        )

    def is_same_opcode_for_exec(self, other: 'instruction'):
        if self.mod_rm is None or other.mod_rm is None:
            m = True
        else:
            m = self.mod_rm[1] == 'r' or other.mod_rm[1] == 'r' or self.mod_rm == other.mod_rm

        s = ''
        if self.vex:
            s = self.vex.mmmmm
        elif self.evex:
            s = self.evex.mm
        o = ''
        if other.vex:
            o = other.vex.mmmmm
        elif other.evex:
            o = other.evex.mm

        if (self.vex or self.evex) and (other.vex or other.evex):
            v = s == o
        elif (self.vex or self.evex) or (other.vex or other.evex):
            v = False
        else:
            v = True

        return (
            v
            and self.code == other.code
            and m
        )


def parse_x86():
    reader = csv.DictReader(csv_file_path.open(encoding="utf-8-sig"))
    raw_instructions: List[RawInstruction] = []
    for row in reader:
        raw_instructions.append(RawInstruction(
            row['Instruction'],
            row['Opcode'],
            row['Valid 64-bit'] == 'Valid',
            row['Valid 32-bit'] == 'Valid',
            row['Valid 16-bit'] == 'Valid',
            row['Feature Flags'] if row['Feature Flags'] else None,
            row['Operand 1'] if row['Operand 1'] != 'NA' else None,
            row['Operand 2'] if row['Operand 2'] != 'NA' else None,
            row['Operand 3'] if row['Operand 3'] != 'NA' else None,
            row['Operand 4'] if row['Operand 4'] != 'NA' else None,
            row['Tuple Type'] if row['Tuple Type'] else None,
            row['Description'],
            row,
        ))

    parsed = []
    for raw in raw_instructions:
        splited = raw.opcode.replace(' + ', ' ').split()
        splited_iter = iter(splited)
        current = next(splited_iter)

        np = False
        prefix = None
        rex = None
        vex = None
        evex = None
        code = []
        register_indicator = None
        mod_rm = None
        is4 = None
        immediate = []
        operands = []

        if raw.op_1:
            operands.append(raw.op_1)
        if raw.op_2:
            operands.append(raw.op_2)
        if raw.op_3:
            operands.append(raw.op_3)
        if raw.op_4:
            operands.append(raw.op_4)

        # ------------ special case

        if raw.instruction[:3] == 'SET':
            mod_rm = '/r'

        if raw.instruction == 'NOP':
            continue

        raw.opcode.rstrip(' (mod=11)')
        raw.opcode.rstrip(' (mod!=11, /5, memory only)')

        # ------------

        if current == 'NP':
            np = True
            try:
                current = next(splited_iter)
            except StopIteration:
                parsed.append(ParsedInstruction(raw.instruction, np, prefix, rex, vex, evex, code, register_indicator, mod_rm, is4, immediate, operands, raw))
                continue

        if current in ('66', 'F2', 'F3'):
            prefix = current
            try:
                current = next(splited_iter)
            except StopIteration:
                parsed.append(ParsedInstruction(raw.instruction, np, prefix, rex, vex, evex, code, register_indicator, mod_rm, is4, immediate, operands, raw))
                continue

        if m := re.fullmatch(r'REX(\.(?P<ide>[WXBR]))?', current):
            rex = RexPrefix(m.group('ide'))
            try:
                current = next(splited_iter)
            except StopIteration:
                parsed.append(ParsedInstruction(raw.instruction, np, prefix, rex, vex, evex, code, register_indicator, mod_rm, is4, immediate, operands, raw))
                print('end in rex', len(parsed))
                continue

        if m := re.fullmatch(r'VEX(\.(?P<nd>(NDS)|(NDD)|(DDS)))?'
                         r'(\.(?P<l>(128)|(256)|(LIG)|(LZ)|(L1)|(L0)))'
                         r'(\.(?P<pp>(66)|(F2)|(F3)))?'
                         r'(\.(?P<mmmmm>(0F)|(0F3A)|(0F38)))'
                         r'(\.(?P<w>(W0)|(W1)|(WIG)))?', current):
            g = m.groupdict()
            vex = VexPrefix(g['nd'], g['l'], g['pp'], g['mmmmm'], g['w'])
            try:
                current = next(splited_iter)
            except StopIteration:
                parsed.append(ParsedInstruction(raw.instruction, np, prefix, rex, vex, evex, code, register_indicator, mod_rm, is4, immediate, operands, raw))
                continue

        if m := re.fullmatch(r'EVEX(\.(?P<nd>(NDS)|(NDD)|(DDS)))?'
                         r'(\.(?P<l>128|256|512|LIG))'
                         r'(\.(?P<pp>(66)|(F2)|(F3)))?'
                         r'(\.(?P<mmmmm>(0F)|(0F3A)|(0F38)))'
                         r'(\.(?P<w>(W0)|(W1)|(WIG)))?', current):
            g = m.groupdict()
            evex = EvexPrefix(g['nd'], g['l'], g['pp'], g['mmmmm'], g['w'])
            try:
                current = next(splited_iter)
            except StopIteration:
                parsed.append(ParsedInstruction(raw.instruction, np, prefix, rex, vex, evex, code, register_indicator, mod_rm, is4, immediate, operands, raw))
                continue

        f = False
        while True:
            if m := re.fullmatch(r'[0-9A-F]{2}', current):
                code.append(current)
            else:
                break
            try:
                current = next(splited_iter)
            except StopIteration:
                f = True
                break
        if f:
            parsed.append(ParsedInstruction(raw.instruction, np, prefix, rex, vex, evex, code, register_indicator, mod_rm, is4, immediate, operands, raw))
            continue

        if current[0] == '+':
            register_indicator = current
            try:
                current = next(splited_iter)
            except StopIteration:
                parsed.append(ParsedInstruction(raw.instruction, np, prefix, rex, vex, evex, code, register_indicator, mod_rm, is4, immediate, operands, raw))
                continue

        if current[0] == '/':
            mod_rm = current
            try:
                current = next(splited_iter)
            except StopIteration:
                parsed.append(ParsedInstruction(raw.instruction, np, prefix, rex, vex, evex, code, register_indicator, mod_rm, is4, immediate, operands, raw))
                continue

        if current == '/is4':
            is4 = current
            try:
                current = next(splited_iter)
            except StopIteration:
                parsed.append(ParsedInstruction(raw.instruction, np, prefix, rex, vex, evex, code, register_indicator, mod_rm, is4, immediate, operands, raw))
                continue

        f = False
        while True:
            if current[0] == 'i' or current[0] == 'c':
                immediate.append(current)
            else:
                break

            try:
                current = next(splited_iter)
            except StopIteration:
                f = True
                break
        if f:
            parsed.append(ParsedInstruction(raw.instruction, np, prefix, rex, vex, evex, code, register_indicator, mod_rm, is4, immediate, operands, raw))
            continue

        try:
            remain = next(splited_iter)
        except StopIteration:
            parsed.append(ParsedInstruction(raw.instruction, np, prefix, rex, vex, evex, code, register_indicator, mod_rm, is4, immediate, operands, raw))
        else:
            print('remain...', len(parsed), current, remain)
            pprint(raw)
            raise Exception('parse error')

    return parsed


if __name__ == '__main__':
    pprint(parse_x86())
